// modules are defined as an array
// [ module function, map of requires ]
//
// map of requires is short require name -> numeric require
//
// anything defined in a previous bundle is accessed via the
// orig method which is the require for previous bundles
parcelRequire = (function (modules, cache, entry, globalName) {
  // Save the require from previous bundle to this closure if any
  var previousRequire = typeof parcelRequire === 'function' && parcelRequire;
  var nodeRequire = typeof require === 'function' && require;

  function newRequire(name, jumped) {
    if (!cache[name]) {
      if (!modules[name]) {
        // if we cannot find the module within our internal map or
        // cache jump to the current global require ie. the last bundle
        // that was added to the page.
        var currentRequire = typeof parcelRequire === 'function' && parcelRequire;
        if (!jumped && currentRequire) {
          return currentRequire(name, true);
        }

        // If there are other bundles on this page the require from the
        // previous one is saved to 'previousRequire'. Repeat this as
        // many times as there are bundles until the module is found or
        // we exhaust the require chain.
        if (previousRequire) {
          return previousRequire(name, true);
        }

        // Try the node require function if it exists.
        if (nodeRequire && typeof name === 'string') {
          return nodeRequire(name);
        }

        var err = new Error('Cannot find module \'' + name + '\'');
        err.code = 'MODULE_NOT_FOUND';
        throw err;
      }

      localRequire.resolve = resolve;
      localRequire.cache = {};

      var module = cache[name] = new newRequire.Module(name);

      modules[name][0].call(module.exports, localRequire, module, module.exports, this);
    }

    return cache[name].exports;

    function localRequire(x){
      return newRequire(localRequire.resolve(x));
    }

    function resolve(x){
      return modules[name][1][x] || x;
    }
  }

  function Module(moduleName) {
    this.id = moduleName;
    this.bundle = newRequire;
    this.exports = {};
  }

  newRequire.isParcelRequire = true;
  newRequire.Module = Module;
  newRequire.modules = modules;
  newRequire.cache = cache;
  newRequire.parent = previousRequire;
  newRequire.register = function (id, exports) {
    modules[id] = [function (require, module) {
      module.exports = exports;
    }, {}];
  };

  var error;
  for (var i = 0; i < entry.length; i++) {
    try {
      newRequire(entry[i]);
    } catch (e) {
      // Save first error but execute all entries
      if (!error) {
        error = e;
      }
    }
  }

  if (entry.length) {
    // Expose entry point to Node, AMD or browser globals
    // Based on https://github.com/ForbesLindesay/umd/blob/master/template.js
    var mainExports = newRequire(entry[entry.length - 1]);

    // CommonJS
    if (typeof exports === "object" && typeof module !== "undefined") {
      module.exports = mainExports;

    // RequireJS
    } else if (typeof define === "function" && define.amd) {
     define(function () {
       return mainExports;
     });

    // <script>
    } else if (globalName) {
      this[globalName] = mainExports;
    }
  }

  // Override the current require with this new one
  parcelRequire = newRequire;

  if (error) {
    // throw error from earlier, _after updating parcelRequire_
    throw error;
  }

  return newRequire;
})({"scripts/processImage.js":[function(require,module,exports) {
/**
 * @license
 * Copyright Dan Munro
 * Released under Expat license <https://directory.fsf.org/wiki/License:Expat>
 */
'use strict';

importScripts('lodash.js');

var getVicinVals = function getVicinVals(mat, x, y, range) {
  // range is how many pixels on each side to get
  var width = mat[0].length;
  var height = mat.length;
  var vicinVals = [];

  for (var xx = x - range; xx <= x + range; xx++) {
    for (var yy = y - range; yy <= y + range; yy++) {
      if (xx >= 0 && xx < width && yy >= 0 && yy < height) {
        vicinVals.push(mat[yy][xx]);
      }
    }
  }

  return vicinVals;
};

var smooth = function smooth(mat) {
  var width = mat[0].length;
  var height = mat.length;
  var simp = [];

  for (var i = 0; i < height; i++) {
    simp[i] = new Array(width);
  }

  for (var y = 0; y < height; y++) {
    for (var x = 0; x < width; x++) {
      var vicinVals = getVicinVals(mat, x, y, 4);
      simp[y][x] = Number(_.chain(vicinVals).countBy().toPairs().maxBy(_.last).head().value());
    }
  }

  return simp;
};

var neighborsSame = function neighborsSame(mat, x, y) {
  var width = mat[0].length;
  var height = mat.length;
  var val = mat[y][x]; // var xRel = [-1, 0, 1, -1, 0, 1, -1, 0, 1];
  // var yRel = [-1, -1, -1, 0, 0, 0, 1, 1, 1];
  // var xRel = [0, -1, 1, 0];
  // var yRel = [-1, 0, 0, 1];

  var xRel = [1, 0];
  var yRel = [0, 1];

  for (var i = 0; i < xRel.length; i++) {
    var xx = x + xRel[i];
    var yy = y + yRel[i];

    if (xx >= 0 && xx < width && yy >= 0 && yy < height) {
      if (mat[yy][xx] != val) {
        return false;
      }
    }
  }

  return true;
};

var outline = function outline(mat) {
  var width = mat[0].length;
  var height = mat.length;
  var line = [];

  for (var i = 0; i < height; i++) {
    line[i] = new Array(width);
  }

  for (var y = 0; y < height; y++) {
    for (var x = 0; x < width; x++) {
      line[y][x] = neighborsSame(mat, x, y) ? 0 : 1;
    }
  }

  return line;
};

var getRegion = function getRegion(mat, cov, x, y) {
  var covered = _.cloneDeep(cov);

  var region = {
    value: mat[y][x],
    x: [],
    y: []
  }; // var covered = [];
  //	 for (var i = 0; i < height; i++) { // where does height come from?
  //	 	covered[i] = new Array(width);
  //	 	_.fill(covered[i], false);
  //	 }

  var value = mat[y][x]; // var check = function(x, y) {
  // 	if (mat[y][x] == value && covered[y][x] == false) {
  // 		covered[y][x] = true;
  // 		region.x.push(x);
  // 		region.y.push(y);
  // 		if (x > 0) { check(x - 1, y); }
  // 		if (x < mat[0].length - 1) { check(x + 1, y); }
  // 		if (y > 0) { check(x, y - 1); }
  // 		if (y < mat.length - 1) { check(x, y + 1); }
  // 	}
  // };
  // check(x, y);

  var queue = [[x, y]];

  while (queue.length > 0) {
    var coord = queue.shift();

    if (covered[coord[1]][coord[0]] == false && mat[coord[1]][coord[0]] == value) {
      region.x.push(coord[0]);
      region.y.push(coord[1]);
      covered[coord[1]][coord[0]] = true;

      if (coord[0] > 0) {
        queue.push([coord[0] - 1, coord[1]]);
      }

      if (coord[0] < mat[0].length - 1) {
        queue.push([coord[0] + 1, coord[1]]);
      }

      if (coord[1] > 0) {
        queue.push([coord[0], coord[1] - 1]);
      }

      if (coord[1] < mat.length - 1) {
        queue.push([coord[0], coord[1] + 1]);
      }
    }
  }

  return region;
};

var coverRegion = function coverRegion(covered, region) {
  for (var i = 0; i < region.x.length; i++) {
    covered[region.y[i]][region.x[i]] = true;
  }
};

var sameCount = function sameCount(mat, x, y, incX, incY) {
  var value = mat[y][x];
  var count = -1;

  while (x >= 0 && x < mat[0].length && y >= 0 && y < mat.length && mat[y][x] == value) {
    count++;
    x += incX;
    y += incY;
  }

  return count;
};

var getLabelLoc = function getLabelLoc(mat, region) {
  var bestI = 0;
  var best = 0;

  for (var i = 0; i < region.x.length; i++) {
    var goodness = sameCount(mat, region.x[i], region.y[i], -1, 0) * sameCount(mat, region.x[i], region.y[i], 1, 0) * sameCount(mat, region.x[i], region.y[i], 0, -1) * sameCount(mat, region.x[i], region.y[i], 0, 1);

    if (goodness > best) {
      best = goodness;
      bestI = i;
    }
  }

  return {
    value: region.value,
    x: region.x[bestI],
    y: region.y[bestI]
  };
};

var getBelowValue = function getBelowValue(mat, region) {
  var x = region.x[0];
  var y = region.y[0];

  while (mat[y][x] == region.value) {
    y++;
  }

  return mat[y][x];
};

var removeRegion = function removeRegion(mat, region) {
  if (region.y[0] > 0) {
    var newValue = mat[region.y[0] - 1][region.x[0]]; // assumes first pixel in list is topmost then leftmost of region.
  } else {
    var newValue = getBelowValue(mat, region);
  }

  for (var i = 0; i < region.x.length; i++) {
    mat[region.y[i]][region.x[i]] = newValue;
  }
};

var getLabelLocs = function getLabelLocs(mat) {
  var width = mat[0].length;
  var height = mat.length;
  var covered = [];

  for (var i = 0; i < height; i++) {
    covered[i] = new Array(width);

    _.fill(covered[i], false);
  }

  var labelLocs = [];

  for (var y = 0; y < height; y++) {
    for (var x = 0; x < width; x++) {
      if (covered[y][x] == false) {
        var region = getRegion(mat, covered, x, y);
        coverRegion(covered, region);

        if (region.x.length > 100) {
          labelLocs.push(getLabelLoc(mat, region));
        } else {
          removeRegion(mat, region);
        }
      }
    }
  }

  return labelLocs;
};

self.addEventListener('message', function (e) {
  self.postMessage({
    cmd: "status",
    status: "smoothing edges..."
  });
  var matSmooth = smooth(e.data.mat);
  self.postMessage({
    cmd: "status",
    status: "identifying color regions..."
  });
  var labelLocs = getLabelLocs(matSmooth);
  self.postMessage({
    cmd: "status",
    status: "drawing outline..."
  });
  var matLine = outline(matSmooth);
  self.postMessage({
    cmd: "result",
    matSmooth: matSmooth,
    labelLocs: labelLocs,
    matLine: matLine
  });
}, false);
},{}],"../../../../../../usr/local/lib/node_modules/parcel/src/builtins/hmr-runtime.js":[function(require,module,exports) {
var global = arguments[3];
var OVERLAY_ID = '__parcel__error__overlay__';
var OldModule = module.bundle.Module;

function Module(moduleName) {
  OldModule.call(this, moduleName);
  this.hot = {
    data: module.bundle.hotData,
    _acceptCallbacks: [],
    _disposeCallbacks: [],
    accept: function (fn) {
      this._acceptCallbacks.push(fn || function () {});
    },
    dispose: function (fn) {
      this._disposeCallbacks.push(fn);
    }
  };
  module.bundle.hotData = null;
}

module.bundle.Module = Module;
var checkedAssets, assetsToAccept;
var parent = module.bundle.parent;

if ((!parent || !parent.isParcelRequire) && typeof WebSocket !== 'undefined') {
  var hostname = "" || location.hostname;
  var protocol = location.protocol === 'https:' ? 'wss' : 'ws';
  var ws = new WebSocket(protocol + '://' + hostname + ':' + "61976" + '/');

  ws.onmessage = function (event) {
    checkedAssets = {};
    assetsToAccept = [];
    var data = JSON.parse(event.data);

    if (data.type === 'update') {
      var handled = false;
      data.assets.forEach(function (asset) {
        if (!asset.isNew) {
          var didAccept = hmrAcceptCheck(global.parcelRequire, asset.id);

          if (didAccept) {
            handled = true;
          }
        }
      }); // Enable HMR for CSS by default.

      handled = handled || data.assets.every(function (asset) {
        return asset.type === 'css' && asset.generated.js;
      });

      if (handled) {
        console.clear();
        data.assets.forEach(function (asset) {
          hmrApply(global.parcelRequire, asset);
        });
        assetsToAccept.forEach(function (v) {
          hmrAcceptRun(v[0], v[1]);
        });
      } else if (location.reload) {
        // `location` global exists in a web worker context but lacks `.reload()` function.
        location.reload();
      }
    }

    if (data.type === 'reload') {
      ws.close();

      ws.onclose = function () {
        location.reload();
      };
    }

    if (data.type === 'error-resolved') {
      console.log('[parcel] ✨ Error resolved');
      removeErrorOverlay();
    }

    if (data.type === 'error') {
      console.error('[parcel] 🚨  ' + data.error.message + '\n' + data.error.stack);
      removeErrorOverlay();
      var overlay = createErrorOverlay(data);
      document.body.appendChild(overlay);
    }
  };
}

function removeErrorOverlay() {
  var overlay = document.getElementById(OVERLAY_ID);

  if (overlay) {
    overlay.remove();
  }
}

function createErrorOverlay(data) {
  var overlay = document.createElement('div');
  overlay.id = OVERLAY_ID; // html encode message and stack trace

  var message = document.createElement('div');
  var stackTrace = document.createElement('pre');
  message.innerText = data.error.message;
  stackTrace.innerText = data.error.stack;
  overlay.innerHTML = '<div style="background: black; font-size: 16px; color: white; position: fixed; height: 100%; width: 100%; top: 0px; left: 0px; padding: 30px; opacity: 0.85; font-family: Menlo, Consolas, monospace; z-index: 9999;">' + '<span style="background: red; padding: 2px 4px; border-radius: 2px;">ERROR</span>' + '<span style="top: 2px; margin-left: 5px; position: relative;">🚨</span>' + '<div style="font-size: 18px; font-weight: bold; margin-top: 20px;">' + message.innerHTML + '</div>' + '<pre>' + stackTrace.innerHTML + '</pre>' + '</div>';
  return overlay;
}

function getParents(bundle, id) {
  var modules = bundle.modules;

  if (!modules) {
    return [];
  }

  var parents = [];
  var k, d, dep;

  for (k in modules) {
    for (d in modules[k][1]) {
      dep = modules[k][1][d];

      if (dep === id || Array.isArray(dep) && dep[dep.length - 1] === id) {
        parents.push(k);
      }
    }
  }

  if (bundle.parent) {
    parents = parents.concat(getParents(bundle.parent, id));
  }

  return parents;
}

function hmrApply(bundle, asset) {
  var modules = bundle.modules;

  if (!modules) {
    return;
  }

  if (modules[asset.id] || !bundle.parent) {
    var fn = new Function('require', 'module', 'exports', asset.generated.js);
    asset.isNew = !modules[asset.id];
    modules[asset.id] = [fn, asset.deps];
  } else if (bundle.parent) {
    hmrApply(bundle.parent, asset);
  }
}

function hmrAcceptCheck(bundle, id) {
  var modules = bundle.modules;

  if (!modules) {
    return;
  }

  if (!modules[id] && bundle.parent) {
    return hmrAcceptCheck(bundle.parent, id);
  }

  if (checkedAssets[id]) {
    return;
  }

  checkedAssets[id] = true;
  var cached = bundle.cache[id];
  assetsToAccept.push([bundle, id]);

  if (cached && cached.hot && cached.hot._acceptCallbacks.length) {
    return true;
  }

  return getParents(global.parcelRequire, id).some(function (id) {
    return hmrAcceptCheck(global.parcelRequire, id);
  });
}

function hmrAcceptRun(bundle, id) {
  var cached = bundle.cache[id];
  bundle.hotData = {};

  if (cached) {
    cached.hot.data = bundle.hotData;
  }

  if (cached && cached.hot && cached.hot._disposeCallbacks.length) {
    cached.hot._disposeCallbacks.forEach(function (cb) {
      cb(bundle.hotData);
    });
  }

  delete bundle.cache[id];
  bundle(id);
  cached = bundle.cache[id];

  if (cached && cached.hot && cached.hot._acceptCallbacks.length) {
    cached.hot._acceptCallbacks.forEach(function (cb) {
      cb();
    });

    return true;
  }
}
},{}]},{},["../../../../../../usr/local/lib/node_modules/parcel/src/builtins/hmr-runtime.js","scripts/processImage.js"], null)
//# sourceMappingURL=/processImage.7a7b5f05.js.map
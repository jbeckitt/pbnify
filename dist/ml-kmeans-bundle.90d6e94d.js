// modules are defined as an array
// [ module function, map of requires ]
//
// map of requires is short require name -> numeric require
//
// anything defined in a previous bundle is accessed via the
// orig method which is the require for previous bundles
parcelRequire = (function (modules, cache, entry, globalName) {
  // Save the require from previous bundle to this closure if any
  var previousRequire = typeof parcelRequire === 'function' && parcelRequire;
  var nodeRequire = typeof require === 'function' && require;

  function newRequire(name, jumped) {
    if (!cache[name]) {
      if (!modules[name]) {
        // if we cannot find the module within our internal map or
        // cache jump to the current global require ie. the last bundle
        // that was added to the page.
        var currentRequire = typeof parcelRequire === 'function' && parcelRequire;
        if (!jumped && currentRequire) {
          return currentRequire(name, true);
        }

        // If there are other bundles on this page the require from the
        // previous one is saved to 'previousRequire'. Repeat this as
        // many times as there are bundles until the module is found or
        // we exhaust the require chain.
        if (previousRequire) {
          return previousRequire(name, true);
        }

        // Try the node require function if it exists.
        if (nodeRequire && typeof name === 'string') {
          return nodeRequire(name);
        }

        var err = new Error('Cannot find module \'' + name + '\'');
        err.code = 'MODULE_NOT_FOUND';
        throw err;
      }

      localRequire.resolve = resolve;
      localRequire.cache = {};

      var module = cache[name] = new newRequire.Module(name);

      modules[name][0].call(module.exports, localRequire, module, module.exports, this);
    }

    return cache[name].exports;

    function localRequire(x){
      return newRequire(localRequire.resolve(x));
    }

    function resolve(x){
      return modules[name][1][x] || x;
    }
  }

  function Module(moduleName) {
    this.id = moduleName;
    this.bundle = newRequire;
    this.exports = {};
  }

  newRequire.isParcelRequire = true;
  newRequire.Module = Module;
  newRequire.modules = modules;
  newRequire.cache = cache;
  newRequire.parent = previousRequire;
  newRequire.register = function (id, exports) {
    modules[id] = [function (require, module) {
      module.exports = exports;
    }, {}];
  };

  var error;
  for (var i = 0; i < entry.length; i++) {
    try {
      newRequire(entry[i]);
    } catch (e) {
      // Save first error but execute all entries
      if (!error) {
        error = e;
      }
    }
  }

  if (entry.length) {
    // Expose entry point to Node, AMD or browser globals
    // Based on https://github.com/ForbesLindesay/umd/blob/master/template.js
    var mainExports = newRequire(entry[entry.length - 1]);

    // CommonJS
    if (typeof exports === "object" && typeof module !== "undefined") {
      module.exports = mainExports;

    // RequireJS
    } else if (typeof define === "function" && define.amd) {
     define(function () {
       return mainExports;
     });

    // <script>
    } else if (globalName) {
      this[globalName] = mainExports;
    }
  }

  // Override the current require with this new one
  parcelRequire = newRequire;

  if (error) {
    // throw error from earlier, _after updating parcelRequire_
    throw error;
  }

  return newRequire;
})({"scripts/ml-kmeans-bundle.js":[function(require,module,exports) {
function _inherits(subClass, superClass) { if (typeof superClass !== "function" && superClass !== null) { throw new TypeError("Super expression must either be null or a function"); } subClass.prototype = Object.create(superClass && superClass.prototype, { constructor: { value: subClass, writable: true, configurable: true } }); if (superClass) _setPrototypeOf(subClass, superClass); }

function _setPrototypeOf(o, p) { _setPrototypeOf = Object.setPrototypeOf || function _setPrototypeOf(o, p) { o.__proto__ = p; return o; }; return _setPrototypeOf(o, p); }

function _createSuper(Derived) { var hasNativeReflectConstruct = _isNativeReflectConstruct(); return function () { var Super = _getPrototypeOf(Derived), result; if (hasNativeReflectConstruct) { var NewTarget = _getPrototypeOf(this).constructor; result = Reflect.construct(Super, arguments, NewTarget); } else { result = Super.apply(this, arguments); } return _possibleConstructorReturn(this, result); }; }

function _possibleConstructorReturn(self, call) { if (call && (_typeof(call) === "object" || typeof call === "function")) { return call; } return _assertThisInitialized(self); }

function _assertThisInitialized(self) { if (self === void 0) { throw new ReferenceError("this hasn't been initialised - super() hasn't been called"); } return self; }

function _isNativeReflectConstruct() { if (typeof Reflect === "undefined" || !Reflect.construct) return false; if (Reflect.construct.sham) return false; if (typeof Proxy === "function") return true; try { Date.prototype.toString.call(Reflect.construct(Date, [], function () {})); return true; } catch (e) { return false; } }

function _getPrototypeOf(o) { _getPrototypeOf = Object.setPrototypeOf ? Object.getPrototypeOf : function _getPrototypeOf(o) { return o.__proto__ || Object.getPrototypeOf(o); }; return _getPrototypeOf(o); }

function _classCallCheck(instance, Constructor) { if (!(instance instanceof Constructor)) { throw new TypeError("Cannot call a class as a function"); } }

function _defineProperties(target, props) { for (var i = 0; i < props.length; i++) { var descriptor = props[i]; descriptor.enumerable = descriptor.enumerable || false; descriptor.configurable = true; if ("value" in descriptor) descriptor.writable = true; Object.defineProperty(target, descriptor.key, descriptor); } }

function _createClass(Constructor, protoProps, staticProps) { if (protoProps) _defineProperties(Constructor.prototype, protoProps); if (staticProps) _defineProperties(Constructor, staticProps); return Constructor; }

function _typeof(obj) { "@babel/helpers - typeof"; if (typeof Symbol === "function" && typeof Symbol.iterator === "symbol") { _typeof = function _typeof(obj) { return typeof obj; }; } else { _typeof = function _typeof(obj) { return obj && typeof Symbol === "function" && obj.constructor === Symbol && obj !== Symbol.prototype ? "symbol" : typeof obj; }; } return _typeof(obj); }

(function () {
  function r(e, n, t) {
    function o(i, f) {
      if (!n[i]) {
        if (!e[i]) {
          var c = "function" == typeof require && require;
          if (!f && c) return c(i, !0);
          if (u) return u(i, !0);
          var a = new Error("Cannot find module '" + i + "'");
          throw a.code = "MODULE_NOT_FOUND", a;
        }

        var p = n[i] = {
          exports: {}
        };
        e[i][0].call(p.exports, function (r) {
          var n = e[i][1][r];
          return o(n || r);
        }, p, p.exports, r, e, n, t);
      }

      return n[i].exports;
    }

    for (var u = "function" == typeof require && require, i = 0; i < t.length; i++) {
      o(t[i]);
    }

    return o;
  }

  return r;
})()({
  1: [function (require, module, exports) {
    var kmeans = require('ml-kmeans');
  }, {
    "ml-kmeans": 7
  }],
  2: [function (require, module, exports) {
    'use strict';

    var toString = Object.prototype.toString;

    function isAnyArray(object) {
      return toString.call(object).endsWith('Array]');
    }

    module.exports = isAnyArray;
  }, {}],
  3: [function (require, module, exports) {
    'use strict';

    function _interopDefault(ex) {
      return ex && _typeof(ex) === 'object' && 'default' in ex ? ex['default'] : ex;
    }

    var isArray = _interopDefault(require('is-any-array'));
    /**
     * Computes the maximum of the given values
     * @param {Array<number>} input
     * @return {number}
     */


    function max(input) {
      if (!isArray(input)) {
        throw new TypeError('input must be an array');
      }

      if (input.length === 0) {
        throw new TypeError('input must not be empty');
      }

      var maxValue = input[0];

      for (var i = 1; i < input.length; i++) {
        if (input[i] > maxValue) maxValue = input[i];
      }

      return maxValue;
    }

    module.exports = max;
  }, {
    "is-any-array": 2
  }],
  4: [function (require, module, exports) {
    'use strict';

    function _interopDefault(ex) {
      return ex && _typeof(ex) === 'object' && 'default' in ex ? ex['default'] : ex;
    }

    var isArray = _interopDefault(require('is-any-array'));
    /**
     * Computes the minimum of the given values
     * @param {Array<number>} input
     * @return {number}
     */


    function min(input) {
      if (!isArray(input)) {
        throw new TypeError('input must be an array');
      }

      if (input.length === 0) {
        throw new TypeError('input must not be empty');
      }

      var minValue = input[0];

      for (var i = 1; i < input.length; i++) {
        if (input[i] < minValue) minValue = input[i];
      }

      return minValue;
    }

    module.exports = min;
  }, {
    "is-any-array": 2
  }],
  5: [function (require, module, exports) {
    'use strict';

    function _interopDefault(ex) {
      return ex && _typeof(ex) === 'object' && 'default' in ex ? ex['default'] : ex;
    }

    var isArray = _interopDefault(require('is-any-array'));

    var max = _interopDefault(require('ml-array-max'));

    var min = _interopDefault(require('ml-array-min'));
    /**
     *
     * @param {Array} input
     * @param {object} [options={}]
     * @param {Array} [options.output=[]] specify the output array, can be the input array for in place modification
     */


    function rescale(input) {
      var options = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};

      if (!isArray(input)) {
        throw new TypeError('input must be an array');
      } else if (input.length === 0) {
        throw new TypeError('input must not be empty');
      }

      var output;

      if (options.output !== undefined) {
        if (!isArray(options.output)) {
          throw new TypeError('output option must be an array if specified');
        }

        output = options.output;
      } else {
        output = new Array(input.length);
      }

      var currentMin = min(input);
      var currentMax = max(input);

      if (currentMin === currentMax) {
        throw new RangeError('minimum and maximum input values are equal. Cannot rescale a constant array');
      }

      var _options$min = options.min,
          minValue = _options$min === void 0 ? options.autoMinMax ? currentMin : 0 : _options$min,
          _options$max = options.max,
          maxValue = _options$max === void 0 ? options.autoMinMax ? currentMax : 1 : _options$max;

      if (minValue >= maxValue) {
        throw new RangeError('min option must be smaller than max option');
      }

      var factor = (maxValue - minValue) / (currentMax - currentMin);

      for (var i = 0; i < input.length; i++) {
        output[i] = (input[i] - currentMin) * factor + minValue;
      }

      return output;
    }

    module.exports = rescale;
  }, {
    "is-any-array": 2,
    "ml-array-max": 3,
    "ml-array-min": 4
  }],
  6: [function (require, module, exports) {
    "use strict";

    Object.defineProperty(exports, "__esModule", {
      value: true
    });

    function squaredEuclidean(p, q) {
      var d = 0;

      for (var i = 0; i < p.length; i++) {
        d += (p[i] - q[i]) * (p[i] - q[i]);
      }

      return d;
    }

    exports.squaredEuclidean = squaredEuclidean;

    function euclidean(p, q) {
      return Math.sqrt(squaredEuclidean(p, q));
    }

    exports.euclidean = euclidean;
  }, {}],
  7: [function (require, module, exports) {
    'use strict';

    var _marked = /*#__PURE__*/regeneratorRuntime.mark(kmeansGenerator);

    function _interopDefault(ex) {
      return ex && _typeof(ex) === 'object' && 'default' in ex ? ex['default'] : ex;
    }

    var mlDistanceEuclidean = require('ml-distance-euclidean');

    var nearestVector = _interopDefault(require('ml-nearest-vector'));

    var Random = _interopDefault(require('ml-random'));

    var mlMatrix = require('ml-matrix');
    /**
     * Calculates the distance matrix for a given array of points
     * @ignore
     * @param {Array<Array<number>>} data - the [x,y,z,...] points to cluster
     * @param {function} distance - Distance function to use between the points
     * @return {Array<Array<number>>} - matrix with the distance values
     */


    function calculateDistanceMatrix(data, distance) {
      var distanceMatrix = new Array(data.length);

      for (var i = 0; i < data.length; ++i) {
        for (var j = i; j < data.length; ++j) {
          if (!distanceMatrix[i]) {
            distanceMatrix[i] = new Array(data.length);
          }

          if (!distanceMatrix[j]) {
            distanceMatrix[j] = new Array(data.length);
          }

          var dist = distance(data[i], data[j]);
          distanceMatrix[i][j] = dist;
          distanceMatrix[j][i] = dist;
        }
      }

      return distanceMatrix;
    }
    /**
     * Updates the cluster identifier based in the new data
     * @ignore
     * @param {Array<Array<number>>} data - the [x,y,z,...] points to cluster
     * @param {Array<Array<number>>} centers - the K centers in format [x,y,z,...]
     * @param {Array <number>} clusterID - the cluster identifier for each data dot
     * @param {function} distance - Distance function to use between the points
     * @return {Array} the cluster identifier for each data dot
     */


    function updateClusterID(data, centers, clusterID, distance) {
      for (var i = 0; i < data.length; i++) {
        clusterID[i] = nearestVector(centers, data[i], {
          distanceFunction: distance
        });
      }

      return clusterID;
    }
    /**
     * Update the center values based in the new configurations of the clusters
     * @ignore
     * @param {Array<Array<number>>} prevCenters - Centroids from the previous iteration
     * @param {Array <Array <number>>} data - the [x,y,z,...] points to cluster
     * @param {Array <number>} clusterID - the cluster identifier for each data dot
     * @param {number} K - Number of clusters
     * @return {Array} he K centers in format [x,y,z,...]
     */


    function updateCenters(prevCenters, data, clusterID, K) {
      var nDim = data[0].length; // copy previous centers

      var centers = new Array(K);
      var centersLen = new Array(K);

      for (var i = 0; i < K; i++) {
        centers[i] = new Array(nDim);
        centersLen[i] = 0;

        for (var j = 0; j < nDim; j++) {
          centers[i][j] = 0;
        }
      } // add the value for all dimensions of the point


      for (var l = 0; l < data.length; l++) {
        centersLen[clusterID[l]]++;

        for (var dim = 0; dim < nDim; dim++) {
          centers[clusterID[l]][dim] += data[l][dim];
        }
      } // divides by length


      for (var id = 0; id < K; id++) {
        for (var d = 0; d < nDim; d++) {
          if (centersLen[id]) {
            centers[id][d] /= centersLen[id];
          } else {
            centers[id][d] = prevCenters[id][d];
          }
        }
      }

      return centers;
    }
    /**
     * The centers have moved more than the tolerance value?
     * @ignore
     * @param {Array<Array<number>>} centers - the K centers in format [x,y,z,...]
     * @param {Array<Array<number>>} oldCenters - the K old centers in format [x,y,z,...]
     * @param {function} distanceFunction - Distance function to use between the points
     * @param {number} tolerance - Allowed distance for the centroids to move
     * @return {boolean}
     */


    function hasConverged(centers, oldCenters, distanceFunction, tolerance) {
      for (var i = 0; i < centers.length; i++) {
        if (distanceFunction(centers[i], oldCenters[i]) > tolerance) {
          return false;
        }
      }

      return true;
    }
    /**
     * Choose K different random points from the original data
     * @ignore
     * @param {Array<Array<number>>} data - Points in the format to cluster [x,y,z,...]
     * @param {number} K - number of clusters
     * @param {number} seed - seed for random number generation
     * @return {Array<Array<number>>} - Initial random points
     */


    function random(data, K, seed) {
      var random = new Random(seed);
      return random.choice(data, {
        size: K
      });
    }
    /**
     * Chooses the most distant points to a first random pick
     * @ignore
     * @param {Array<Array<number>>} data - Points in the format to cluster [x,y,z,...]
     * @param {number} K - number of clusters
     * @param {Array<Array<number>>} distanceMatrix - matrix with the distance values
     * @param {number} seed - seed for random number generation
     * @return {Array<Array<number>>} - Initial random points
     */


    function mostDistant(data, K, distanceMatrix, seed) {
      var random = new Random(seed);
      var ans = new Array(K); // chooses a random point as initial cluster

      ans[0] = Math.floor(random.random() * data.length);

      if (K > 1) {
        // chooses the more distant point
        var maxDist = {
          dist: -1,
          index: -1
        };

        for (var l = 0; l < data.length; ++l) {
          if (distanceMatrix[ans[0]][l] > maxDist.dist) {
            maxDist.dist = distanceMatrix[ans[0]][l];
            maxDist.index = l;
          }
        }

        ans[1] = maxDist.index;

        if (K > 2) {
          // chooses the set of points that maximises the min distance
          for (var k = 2; k < K; ++k) {
            var center = {
              dist: -1,
              index: -1
            };

            for (var m = 0; m < data.length; ++m) {
              // minimum distance to centers
              var minDistCent = {
                dist: Number.MAX_VALUE,
                index: -1
              };

              for (var n = 0; n < k; ++n) {
                if (distanceMatrix[n][m] < minDistCent.dist && ans.indexOf(m) === -1) {
                  minDistCent = {
                    dist: distanceMatrix[n][m],
                    index: m
                  };
                }
              }

              if (minDistCent.dist !== Number.MAX_VALUE && minDistCent.dist > center.dist) {
                center = Object.assign({}, minDistCent);
              }
            }

            ans[k] = center.index;
          }
        }
      }

      return ans.map(function (index) {
        return data[index];
      });
    } // Implementation inspired from scikit


    function kmeanspp(X, K) {
      var options = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : {};
      X = new mlMatrix.Matrix(X);
      var nSamples = X.rows;
      var random = new Random(options.seed); // Set the number of trials

      var centers = [];
      var localTrials = options.localTrials || 2 + Math.floor(Math.log(K)); // Pick the first center at random from the dataset

      var firstCenterIdx = random.randInt(nSamples);
      centers.push(X.getRow(firstCenterIdx)); // Init closest distances

      var closestDistSquared = new mlMatrix.Matrix(1, X.rows);

      for (var i = 0; i < X.rows; i++) {
        closestDistSquared.set(0, i, mlDistanceEuclidean.squaredEuclidean(X.getRow(i), centers[0]));
      }

      var cumSumClosestDistSquared = [cumSum(closestDistSquared.getRow(0))];
      var factor = 1 / cumSumClosestDistSquared[0][nSamples - 1];
      var probabilities = mlMatrix.Matrix.mul(closestDistSquared, factor); // Iterate over the remaining centers

      for (var _i = 1; _i < K; _i++) {
        var candidateIdx = random.choice(nSamples, {
          replace: true,
          size: localTrials,
          probabilities: probabilities[0]
        });
        var candidates = X.selection(candidateIdx, range(X.columns));
        var distanceToCandidates = euclideanDistances(candidates, X);
        var bestCandidate = void 0;
        var bestPot = void 0;
        var bestDistSquared = void 0;

        for (var j = 0; j < localTrials; j++) {
          var newDistSquared = mlMatrix.Matrix.min(closestDistSquared, [distanceToCandidates.getRow(j)]);
          var newPot = newDistSquared.sum();

          if (bestCandidate === undefined || newPot < bestPot) {
            bestCandidate = candidateIdx[j];
            bestPot = newPot;
            bestDistSquared = newDistSquared;
          }
        }

        centers[_i] = X.getRow(bestCandidate);
        closestDistSquared = bestDistSquared;
        cumSumClosestDistSquared = [cumSum(closestDistSquared.getRow(0))];
        probabilities = mlMatrix.Matrix.mul(closestDistSquared, 1 / cumSumClosestDistSquared[0][nSamples - 1]);
      }

      return centers;
    }

    function euclideanDistances(A, B) {
      var result = new mlMatrix.Matrix(A.rows, B.rows);

      for (var i = 0; i < A.rows; i++) {
        for (var j = 0; j < B.rows; j++) {
          result.set(i, j, mlDistanceEuclidean.squaredEuclidean(A.getRow(i), B.getRow(j)));
        }
      }

      return result;
    }

    function range(l) {
      var r = [];

      for (var i = 0; i < l; i++) {
        r.push(i);
      }

      return r;
    }

    function cumSum(arr) {
      var cumSum = [arr[0]];

      for (var i = 1; i < arr.length; i++) {
        cumSum[i] = cumSum[i - 1] + arr[i];
      }

      return cumSum;
    }

    var distanceSymbol = Symbol('distance');

    var KMeansResult = /*#__PURE__*/function () {
      /**
       * Result of the kmeans algorithm
       * @param {Array<number>} clusters - the cluster identifier for each data dot
       * @param {Array<Array<object>>} centroids - the K centers in format [x,y,z,...], the error and size of the cluster
       * @param {boolean} converged - Converge criteria satisfied
       * @param {number} iterations - Current number of iterations
       * @param {function} distance - (*Private*) Distance function to use between the points
       * @constructor
       */
      function KMeansResult(clusters, centroids, converged, iterations, distance) {
        _classCallCheck(this, KMeansResult);

        this.clusters = clusters;
        this.centroids = centroids;
        this.converged = converged;
        this.iterations = iterations;
        this[distanceSymbol] = distance;
      }
      /**
       * Allows to compute for a new array of points their cluster id
       * @param {Array<Array<number>>} data - the [x,y,z,...] points to cluster
       * @return {Array<number>} - cluster id for each point
       */


      _createClass(KMeansResult, [{
        key: "nearest",
        value: function nearest(data) {
          var clusterID = new Array(data.length);
          var centroids = this.centroids.map(function (centroid) {
            return centroid.centroid;
          });
          return updateClusterID(data, centroids, clusterID, this[distanceSymbol]);
        }
        /**
         * Returns a KMeansResult with the error and size of the cluster
         * @ignore
         * @param {Array<Array<number>>} data - the [x,y,z,...] points to cluster
         * @return {KMeansResult}
         */

      }, {
        key: "computeInformation",
        value: function computeInformation(data) {
          var enrichedCentroids = this.centroids.map(function (centroid) {
            return {
              centroid: centroid,
              error: 0,
              size: 0
            };
          });

          for (var i = 0; i < data.length; i++) {
            enrichedCentroids[this.clusters[i]].error += this[distanceSymbol](data[i], this.centroids[this.clusters[i]]);
            enrichedCentroids[this.clusters[i]].size++;
          }

          for (var j = 0; j < this.centroids.length; j++) {
            if (enrichedCentroids[j].size) {
              enrichedCentroids[j].error /= enrichedCentroids[j].size;
            } else {
              enrichedCentroids[j].error = null;
            }
          }

          return new KMeansResult(this.clusters, enrichedCentroids, this.converged, this.iterations, this[distanceSymbol]);
        }
      }]);

      return KMeansResult;
    }();

    var defaultOptions = {
      maxIterations: 100,
      tolerance: 1e-6,
      withIterations: false,
      initialization: 'kmeans++',
      distanceFunction: mlDistanceEuclidean.squaredEuclidean
    };
    /**
     * Each step operation for kmeans
     * @ignore
     * @param {Array<Array<number>>} centers - K centers in format [x,y,z,...]
     * @param {Array<Array<number>>} data - Points [x,y,z,...] to cluster
     * @param {Array<number>} clusterID - Cluster identifier for each data dot
     * @param {number} K - Number of clusters
     * @param {object} [options] - Option object
     * @param {number} iterations - Current number of iterations
     * @return {KMeansResult}
     */

    function step(centers, data, clusterID, K, options, iterations) {
      clusterID = updateClusterID(data, centers, clusterID, options.distanceFunction);
      var newCenters = updateCenters(centers, data, clusterID, K);
      var converged = hasConverged(newCenters, centers, options.distanceFunction, options.tolerance);
      return new KMeansResult(clusterID, newCenters, converged, iterations, options.distanceFunction);
    }
    /**
     * Generator version for the algorithm
     * @ignore
     * @param {Array<Array<number>>} centers - K centers in format [x,y,z,...]
     * @param {Array<Array<number>>} data - Points [x,y,z,...] to cluster
     * @param {Array<number>} clusterID - Cluster identifier for each data dot
     * @param {number} K - Number of clusters
     * @param {object} [options] - Option object
     */


    function kmeansGenerator(centers, data, clusterID, K, options) {
      var converged, stepNumber, stepResult;
      return regeneratorRuntime.wrap(function kmeansGenerator$(_context) {
        while (1) {
          switch (_context.prev = _context.next) {
            case 0:
              converged = false;
              stepNumber = 0;

            case 2:
              if (!(!converged && stepNumber < options.maxIterations)) {
                _context.next = 10;
                break;
              }

              stepResult = step(centers, data, clusterID, K, options, ++stepNumber);
              _context.next = 6;
              return stepResult.computeInformation(data);

            case 6:
              converged = stepResult.converged;
              centers = stepResult.centroids;
              _context.next = 2;
              break;

            case 10:
            case "end":
              return _context.stop();
          }
        }
      }, _marked);
    }
    /**
     * K-means algorithm
     * @param {Array<Array<number>>} data - Points in the format to cluster [x,y,z,...]
     * @param {number} K - Number of clusters
     * @param {object} [options] - Option object
     * @param {number} [options.maxIterations = 100] - Maximum of iterations allowed
     * @param {number} [options.tolerance = 1e-6] - Error tolerance
     * @param {boolean} [options.withIterations = false] - Store clusters and centroids for each iteration
     * @param {function} [options.distanceFunction = squaredDistance] - Distance function to use between the points
     * @param {number} [options.seed] - Seed for random initialization.
     * @param {string|Array<Array<number>>} [options.initialization = 'kmeans++'] - K centers in format [x,y,z,...] or a method for initialize the data:
     *  * You can either specify your custom start centroids, or select one of the following initialization method:
     *  * `'kmeans++'` will use the kmeans++ method as described by http://ilpubs.stanford.edu:8090/778/1/2006-13.pdf
     *  * `'random'` will choose K random different values.
     *  * `'mostDistant'` will choose the more distant points to a first random pick
     * @return {KMeansResult} - Cluster identifier for each data dot and centroids with the following fields:
     *  * `'clusters'`: Array of indexes for the clusters.
     *  * `'centroids'`: Array with the resulting centroids.
     *  * `'iterations'`: Number of iterations that took to converge
     */


    function kmeans(data, K, options) {
      options = Object.assign({}, defaultOptions, options);

      if (K <= 0 || K > data.length || !Number.isInteger(K)) {
        throw new Error('K should be a positive integer smaller than the number of points');
      }

      var centers;

      if (Array.isArray(options.initialization)) {
        if (options.initialization.length !== K) {
          throw new Error('The initial centers should have the same length as K');
        } else {
          centers = options.initialization;
        }
      } else {
        switch (options.initialization) {
          case 'kmeans++':
            centers = kmeanspp(data, K, options);
            break;

          case 'random':
            centers = random(data, K, options.seed);
            break;

          case 'mostDistant':
            centers = mostDistant(data, K, calculateDistanceMatrix(data, options.distanceFunction), options.seed);
            break;

          default:
            throw new Error("Unknown initialization method: \"".concat(options.initialization, "\""));
        }
      } // infinite loop until convergence


      if (options.maxIterations === 0) {
        options.maxIterations = Number.MAX_VALUE;
      }

      var clusterID = new Array(data.length);

      if (options.withIterations) {
        return kmeansGenerator(centers, data, clusterID, K, options);
      } else {
        var converged = false;
        var stepNumber = 0;
        var stepResult;

        while (!converged && stepNumber < options.maxIterations) {
          stepResult = step(centers, data, clusterID, K, options, ++stepNumber);
          converged = stepResult.converged;
          centers = stepResult.centroids;
        }

        return stepResult.computeInformation(data);
      }
    }

    module.exports = kmeans;
    window.kmeans = kmeans;
  }, {
    "ml-distance-euclidean": 6,
    "ml-matrix": 8,
    "ml-nearest-vector": 9,
    "ml-random": 11
  }],
  8: [function (require, module, exports) {
    'use strict';

    Object.defineProperty(exports, '__esModule', {
      value: true
    });

    function _interopDefault(ex) {
      return ex && _typeof(ex) === 'object' && 'default' in ex ? ex['default'] : ex;
    }

    var rescale = _interopDefault(require('ml-array-rescale'));
    /**
     * @private
     * Check that a row index is not out of bounds
     * @param {Matrix} matrix
     * @param {number} index
     * @param {boolean} [outer]
     */


    function checkRowIndex(matrix, index, outer) {
      var max = outer ? matrix.rows : matrix.rows - 1;

      if (index < 0 || index > max) {
        throw new RangeError('Row index out of range');
      }
    }
    /**
     * @private
     * Check that a column index is not out of bounds
     * @param {Matrix} matrix
     * @param {number} index
     * @param {boolean} [outer]
     */


    function checkColumnIndex(matrix, index, outer) {
      var max = outer ? matrix.columns : matrix.columns - 1;

      if (index < 0 || index > max) {
        throw new RangeError('Column index out of range');
      }
    }
    /**
     * @private
     * Check that the provided vector is an array with the right length
     * @param {Matrix} matrix
     * @param {Array|Matrix} vector
     * @return {Array}
     * @throws {RangeError}
     */


    function checkRowVector(matrix, vector) {
      if (vector.to1DArray) {
        vector = vector.to1DArray();
      }

      if (vector.length !== matrix.columns) {
        throw new RangeError('vector size must be the same as the number of columns');
      }

      return vector;
    }
    /**
     * @private
     * Check that the provided vector is an array with the right length
     * @param {Matrix} matrix
     * @param {Array|Matrix} vector
     * @return {Array}
     * @throws {RangeError}
     */


    function checkColumnVector(matrix, vector) {
      if (vector.to1DArray) {
        vector = vector.to1DArray();
      }

      if (vector.length !== matrix.rows) {
        throw new RangeError('vector size must be the same as the number of rows');
      }

      return vector;
    }

    function checkIndices(matrix, rowIndices, columnIndices) {
      return {
        row: checkRowIndices(matrix, rowIndices),
        column: checkColumnIndices(matrix, columnIndices)
      };
    }

    function checkRowIndices(matrix, rowIndices) {
      if (_typeof(rowIndices) !== 'object') {
        throw new TypeError('unexpected type for row indices');
      }

      var rowOut = rowIndices.some(function (r) {
        return r < 0 || r >= matrix.rows;
      });

      if (rowOut) {
        throw new RangeError('row indices are out of range');
      }

      if (!Array.isArray(rowIndices)) rowIndices = Array.from(rowIndices);
      return rowIndices;
    }

    function checkColumnIndices(matrix, columnIndices) {
      if (_typeof(columnIndices) !== 'object') {
        throw new TypeError('unexpected type for column indices');
      }

      var columnOut = columnIndices.some(function (c) {
        return c < 0 || c >= matrix.columns;
      });

      if (columnOut) {
        throw new RangeError('column indices are out of range');
      }

      if (!Array.isArray(columnIndices)) columnIndices = Array.from(columnIndices);
      return columnIndices;
    }

    function checkRange(matrix, startRow, endRow, startColumn, endColumn) {
      if (arguments.length !== 5) {
        throw new RangeError('expected 4 arguments');
      }

      checkNumber('startRow', startRow);
      checkNumber('endRow', endRow);
      checkNumber('startColumn', startColumn);
      checkNumber('endColumn', endColumn);

      if (startRow > endRow || startColumn > endColumn || startRow < 0 || startRow >= matrix.rows || endRow < 0 || endRow >= matrix.rows || startColumn < 0 || startColumn >= matrix.columns || endColumn < 0 || endColumn >= matrix.columns) {
        throw new RangeError('Submatrix indices are out of range');
      }
    }

    function newArray(length) {
      var value = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : 0;
      var array = [];

      for (var i = 0; i < length; i++) {
        array.push(value);
      }

      return array;
    }

    function checkNumber(name, value) {
      if (typeof value !== 'number') {
        throw new TypeError("".concat(name, " must be a number"));
      }
    }

    function sumByRow(matrix) {
      var sum = newArray(matrix.rows);

      for (var i = 0; i < matrix.rows; ++i) {
        for (var j = 0; j < matrix.columns; ++j) {
          sum[i] += matrix.get(i, j);
        }
      }

      return sum;
    }

    function sumByColumn(matrix) {
      var sum = newArray(matrix.columns);

      for (var i = 0; i < matrix.rows; ++i) {
        for (var j = 0; j < matrix.columns; ++j) {
          sum[j] += matrix.get(i, j);
        }
      }

      return sum;
    }

    function sumAll(matrix) {
      var v = 0;

      for (var i = 0; i < matrix.rows; i++) {
        for (var j = 0; j < matrix.columns; j++) {
          v += matrix.get(i, j);
        }
      }

      return v;
    }

    function productByRow(matrix) {
      var sum = newArray(matrix.rows, 1);

      for (var i = 0; i < matrix.rows; ++i) {
        for (var j = 0; j < matrix.columns; ++j) {
          sum[i] *= matrix.get(i, j);
        }
      }

      return sum;
    }

    function productByColumn(matrix) {
      var sum = newArray(matrix.columns, 1);

      for (var i = 0; i < matrix.rows; ++i) {
        for (var j = 0; j < matrix.columns; ++j) {
          sum[j] *= matrix.get(i, j);
        }
      }

      return sum;
    }

    function productAll(matrix) {
      var v = 1;

      for (var i = 0; i < matrix.rows; i++) {
        for (var j = 0; j < matrix.columns; j++) {
          v *= matrix.get(i, j);
        }
      }

      return v;
    }

    function varianceByRow(matrix, unbiased, mean) {
      var rows = matrix.rows;
      var cols = matrix.columns;
      var variance = [];

      for (var i = 0; i < rows; i++) {
        var sum1 = 0;
        var sum2 = 0;
        var x = 0;

        for (var j = 0; j < cols; j++) {
          x = matrix.get(i, j) - mean[i];
          sum1 += x;
          sum2 += x * x;
        }

        if (unbiased) {
          variance.push((sum2 - sum1 * sum1 / cols) / (cols - 1));
        } else {
          variance.push((sum2 - sum1 * sum1 / cols) / cols);
        }
      }

      return variance;
    }

    function varianceByColumn(matrix, unbiased, mean) {
      var rows = matrix.rows;
      var cols = matrix.columns;
      var variance = [];

      for (var j = 0; j < cols; j++) {
        var sum1 = 0;
        var sum2 = 0;
        var x = 0;

        for (var i = 0; i < rows; i++) {
          x = matrix.get(i, j) - mean[j];
          sum1 += x;
          sum2 += x * x;
        }

        if (unbiased) {
          variance.push((sum2 - sum1 * sum1 / rows) / (rows - 1));
        } else {
          variance.push((sum2 - sum1 * sum1 / rows) / rows);
        }
      }

      return variance;
    }

    function varianceAll(matrix, unbiased, mean) {
      var rows = matrix.rows;
      var cols = matrix.columns;
      var size = rows * cols;
      var sum1 = 0;
      var sum2 = 0;
      var x = 0;

      for (var i = 0; i < rows; i++) {
        for (var j = 0; j < cols; j++) {
          x = matrix.get(i, j) - mean;
          sum1 += x;
          sum2 += x * x;
        }
      }

      if (unbiased) {
        return (sum2 - sum1 * sum1 / size) / (size - 1);
      } else {
        return (sum2 - sum1 * sum1 / size) / size;
      }
    }

    function centerByRow(matrix, mean) {
      for (var i = 0; i < matrix.rows; i++) {
        for (var j = 0; j < matrix.columns; j++) {
          matrix.set(i, j, matrix.get(i, j) - mean[i]);
        }
      }
    }

    function centerByColumn(matrix, mean) {
      for (var i = 0; i < matrix.rows; i++) {
        for (var j = 0; j < matrix.columns; j++) {
          matrix.set(i, j, matrix.get(i, j) - mean[j]);
        }
      }
    }

    function centerAll(matrix, mean) {
      for (var i = 0; i < matrix.rows; i++) {
        for (var j = 0; j < matrix.columns; j++) {
          matrix.set(i, j, matrix.get(i, j) - mean);
        }
      }
    }

    function getScaleByRow(matrix) {
      var scale = [];

      for (var i = 0; i < matrix.rows; i++) {
        var sum = 0;

        for (var j = 0; j < matrix.columns; j++) {
          sum += Math.pow(matrix.get(i, j), 2) / (matrix.columns - 1);
        }

        scale.push(Math.sqrt(sum));
      }

      return scale;
    }

    function scaleByRow(matrix, scale) {
      for (var i = 0; i < matrix.rows; i++) {
        for (var j = 0; j < matrix.columns; j++) {
          matrix.set(i, j, matrix.get(i, j) / scale[i]);
        }
      }
    }

    function getScaleByColumn(matrix) {
      var scale = [];

      for (var j = 0; j < matrix.columns; j++) {
        var sum = 0;

        for (var i = 0; i < matrix.rows; i++) {
          sum += Math.pow(matrix.get(i, j), 2) / (matrix.rows - 1);
        }

        scale.push(Math.sqrt(sum));
      }

      return scale;
    }

    function scaleByColumn(matrix, scale) {
      for (var i = 0; i < matrix.rows; i++) {
        for (var j = 0; j < matrix.columns; j++) {
          matrix.set(i, j, matrix.get(i, j) / scale[j]);
        }
      }
    }

    function getScaleAll(matrix) {
      var divider = matrix.size - 1;
      var sum = 0;

      for (var j = 0; j < matrix.columns; j++) {
        for (var i = 0; i < matrix.rows; i++) {
          sum += Math.pow(matrix.get(i, j), 2) / divider;
        }
      }

      return Math.sqrt(sum);
    }

    function scaleAll(matrix, scale) {
      for (var i = 0; i < matrix.rows; i++) {
        for (var j = 0; j < matrix.columns; j++) {
          matrix.set(i, j, matrix.get(i, j) / scale);
        }
      }
    }

    function inspectMatrix() {
      var indent = ' '.repeat(2);
      var indentData = ' '.repeat(4);
      return "".concat(this.constructor.name, " {\n").concat(indent, "[\n").concat(indentData).concat(inspectData(this, indentData), "\n").concat(indent, "]\n").concat(indent, "rows: ").concat(this.rows, "\n").concat(indent, "columns: ").concat(this.columns, "\n}");
    }

    var maxRows = 15;
    var maxColumns = 10;
    var maxNumSize = 8;

    function inspectData(matrix, indent) {
      var rows = matrix.rows,
          columns = matrix.columns;
      var maxI = Math.min(rows, maxRows);
      var maxJ = Math.min(columns, maxColumns);
      var result = [];

      for (var i = 0; i < maxI; i++) {
        var line = [];

        for (var j = 0; j < maxJ; j++) {
          line.push(formatNumber(matrix.get(i, j)));
        }

        result.push("".concat(line.join(' ')));
      }

      if (maxJ !== columns) {
        result[result.length - 1] += " ... ".concat(columns - maxColumns, " more columns");
      }

      if (maxI !== rows) {
        result.push("... ".concat(rows - maxRows, " more rows"));
      }

      return result.join("\n".concat(indent));
    }

    function formatNumber(num) {
      var numStr = String(num);

      if (numStr.length <= maxNumSize) {
        return numStr.padEnd(maxNumSize, ' ');
      }

      var precise = num.toPrecision(maxNumSize - 2);

      if (precise.length <= maxNumSize) {
        return precise;
      }

      var exponential = num.toExponential(maxNumSize - 2);
      var eIndex = exponential.indexOf('e');
      var e = exponential.substring(eIndex);
      return exponential.substring(0, maxNumSize - e.length) + e;
    }

    function installMathOperations(AbstractMatrix, Matrix) {
      AbstractMatrix.prototype.add = function add(value) {
        if (typeof value === 'number') return this.addS(value);
        return this.addM(value);
      };

      AbstractMatrix.prototype.addS = function addS(value) {
        for (var i = 0; i < this.rows; i++) {
          for (var j = 0; j < this.columns; j++) {
            this.set(i, j, this.get(i, j) + value);
          }
        }

        return this;
      };

      AbstractMatrix.prototype.addM = function addM(matrix) {
        matrix = Matrix.checkMatrix(matrix);

        if (this.rows !== matrix.rows || this.columns !== matrix.columns) {
          throw new RangeError('Matrices dimensions must be equal');
        }

        for (var i = 0; i < this.rows; i++) {
          for (var j = 0; j < this.columns; j++) {
            this.set(i, j, this.get(i, j) + matrix.get(i, j));
          }
        }

        return this;
      };

      AbstractMatrix.add = function add(matrix, value) {
        var newMatrix = new Matrix(matrix);
        return newMatrix.add(value);
      };

      AbstractMatrix.prototype.sub = function sub(value) {
        if (typeof value === 'number') return this.subS(value);
        return this.subM(value);
      };

      AbstractMatrix.prototype.subS = function subS(value) {
        for (var i = 0; i < this.rows; i++) {
          for (var j = 0; j < this.columns; j++) {
            this.set(i, j, this.get(i, j) - value);
          }
        }

        return this;
      };

      AbstractMatrix.prototype.subM = function subM(matrix) {
        matrix = Matrix.checkMatrix(matrix);

        if (this.rows !== matrix.rows || this.columns !== matrix.columns) {
          throw new RangeError('Matrices dimensions must be equal');
        }

        for (var i = 0; i < this.rows; i++) {
          for (var j = 0; j < this.columns; j++) {
            this.set(i, j, this.get(i, j) - matrix.get(i, j));
          }
        }

        return this;
      };

      AbstractMatrix.sub = function sub(matrix, value) {
        var newMatrix = new Matrix(matrix);
        return newMatrix.sub(value);
      };

      AbstractMatrix.prototype.subtract = AbstractMatrix.prototype.sub;
      AbstractMatrix.prototype.subtractS = AbstractMatrix.prototype.subS;
      AbstractMatrix.prototype.subtractM = AbstractMatrix.prototype.subM;
      AbstractMatrix.subtract = AbstractMatrix.sub;

      AbstractMatrix.prototype.mul = function mul(value) {
        if (typeof value === 'number') return this.mulS(value);
        return this.mulM(value);
      };

      AbstractMatrix.prototype.mulS = function mulS(value) {
        for (var i = 0; i < this.rows; i++) {
          for (var j = 0; j < this.columns; j++) {
            this.set(i, j, this.get(i, j) * value);
          }
        }

        return this;
      };

      AbstractMatrix.prototype.mulM = function mulM(matrix) {
        matrix = Matrix.checkMatrix(matrix);

        if (this.rows !== matrix.rows || this.columns !== matrix.columns) {
          throw new RangeError('Matrices dimensions must be equal');
        }

        for (var i = 0; i < this.rows; i++) {
          for (var j = 0; j < this.columns; j++) {
            this.set(i, j, this.get(i, j) * matrix.get(i, j));
          }
        }

        return this;
      };

      AbstractMatrix.mul = function mul(matrix, value) {
        var newMatrix = new Matrix(matrix);
        return newMatrix.mul(value);
      };

      AbstractMatrix.prototype.multiply = AbstractMatrix.prototype.mul;
      AbstractMatrix.prototype.multiplyS = AbstractMatrix.prototype.mulS;
      AbstractMatrix.prototype.multiplyM = AbstractMatrix.prototype.mulM;
      AbstractMatrix.multiply = AbstractMatrix.mul;

      AbstractMatrix.prototype.div = function div(value) {
        if (typeof value === 'number') return this.divS(value);
        return this.divM(value);
      };

      AbstractMatrix.prototype.divS = function divS(value) {
        for (var i = 0; i < this.rows; i++) {
          for (var j = 0; j < this.columns; j++) {
            this.set(i, j, this.get(i, j) / value);
          }
        }

        return this;
      };

      AbstractMatrix.prototype.divM = function divM(matrix) {
        matrix = Matrix.checkMatrix(matrix);

        if (this.rows !== matrix.rows || this.columns !== matrix.columns) {
          throw new RangeError('Matrices dimensions must be equal');
        }

        for (var i = 0; i < this.rows; i++) {
          for (var j = 0; j < this.columns; j++) {
            this.set(i, j, this.get(i, j) / matrix.get(i, j));
          }
        }

        return this;
      };

      AbstractMatrix.div = function div(matrix, value) {
        var newMatrix = new Matrix(matrix);
        return newMatrix.div(value);
      };

      AbstractMatrix.prototype.divide = AbstractMatrix.prototype.div;
      AbstractMatrix.prototype.divideS = AbstractMatrix.prototype.divS;
      AbstractMatrix.prototype.divideM = AbstractMatrix.prototype.divM;
      AbstractMatrix.divide = AbstractMatrix.div;

      AbstractMatrix.prototype.mod = function mod(value) {
        if (typeof value === 'number') return this.modS(value);
        return this.modM(value);
      };

      AbstractMatrix.prototype.modS = function modS(value) {
        for (var i = 0; i < this.rows; i++) {
          for (var j = 0; j < this.columns; j++) {
            this.set(i, j, this.get(i, j) % value);
          }
        }

        return this;
      };

      AbstractMatrix.prototype.modM = function modM(matrix) {
        matrix = Matrix.checkMatrix(matrix);

        if (this.rows !== matrix.rows || this.columns !== matrix.columns) {
          throw new RangeError('Matrices dimensions must be equal');
        }

        for (var i = 0; i < this.rows; i++) {
          for (var j = 0; j < this.columns; j++) {
            this.set(i, j, this.get(i, j) % matrix.get(i, j));
          }
        }

        return this;
      };

      AbstractMatrix.mod = function mod(matrix, value) {
        var newMatrix = new Matrix(matrix);
        return newMatrix.mod(value);
      };

      AbstractMatrix.prototype.modulus = AbstractMatrix.prototype.mod;
      AbstractMatrix.prototype.modulusS = AbstractMatrix.prototype.modS;
      AbstractMatrix.prototype.modulusM = AbstractMatrix.prototype.modM;
      AbstractMatrix.modulus = AbstractMatrix.mod;

      AbstractMatrix.prototype.and = function and(value) {
        if (typeof value === 'number') return this.andS(value);
        return this.andM(value);
      };

      AbstractMatrix.prototype.andS = function andS(value) {
        for (var i = 0; i < this.rows; i++) {
          for (var j = 0; j < this.columns; j++) {
            this.set(i, j, this.get(i, j) & value);
          }
        }

        return this;
      };

      AbstractMatrix.prototype.andM = function andM(matrix) {
        matrix = Matrix.checkMatrix(matrix);

        if (this.rows !== matrix.rows || this.columns !== matrix.columns) {
          throw new RangeError('Matrices dimensions must be equal');
        }

        for (var i = 0; i < this.rows; i++) {
          for (var j = 0; j < this.columns; j++) {
            this.set(i, j, this.get(i, j) & matrix.get(i, j));
          }
        }

        return this;
      };

      AbstractMatrix.and = function and(matrix, value) {
        var newMatrix = new Matrix(matrix);
        return newMatrix.and(value);
      };

      AbstractMatrix.prototype.or = function or(value) {
        if (typeof value === 'number') return this.orS(value);
        return this.orM(value);
      };

      AbstractMatrix.prototype.orS = function orS(value) {
        for (var i = 0; i < this.rows; i++) {
          for (var j = 0; j < this.columns; j++) {
            this.set(i, j, this.get(i, j) | value);
          }
        }

        return this;
      };

      AbstractMatrix.prototype.orM = function orM(matrix) {
        matrix = Matrix.checkMatrix(matrix);

        if (this.rows !== matrix.rows || this.columns !== matrix.columns) {
          throw new RangeError('Matrices dimensions must be equal');
        }

        for (var i = 0; i < this.rows; i++) {
          for (var j = 0; j < this.columns; j++) {
            this.set(i, j, this.get(i, j) | matrix.get(i, j));
          }
        }

        return this;
      };

      AbstractMatrix.or = function or(matrix, value) {
        var newMatrix = new Matrix(matrix);
        return newMatrix.or(value);
      };

      AbstractMatrix.prototype.xor = function xor(value) {
        if (typeof value === 'number') return this.xorS(value);
        return this.xorM(value);
      };

      AbstractMatrix.prototype.xorS = function xorS(value) {
        for (var i = 0; i < this.rows; i++) {
          for (var j = 0; j < this.columns; j++) {
            this.set(i, j, this.get(i, j) ^ value);
          }
        }

        return this;
      };

      AbstractMatrix.prototype.xorM = function xorM(matrix) {
        matrix = Matrix.checkMatrix(matrix);

        if (this.rows !== matrix.rows || this.columns !== matrix.columns) {
          throw new RangeError('Matrices dimensions must be equal');
        }

        for (var i = 0; i < this.rows; i++) {
          for (var j = 0; j < this.columns; j++) {
            this.set(i, j, this.get(i, j) ^ matrix.get(i, j));
          }
        }

        return this;
      };

      AbstractMatrix.xor = function xor(matrix, value) {
        var newMatrix = new Matrix(matrix);
        return newMatrix.xor(value);
      };

      AbstractMatrix.prototype.leftShift = function leftShift(value) {
        if (typeof value === 'number') return this.leftShiftS(value);
        return this.leftShiftM(value);
      };

      AbstractMatrix.prototype.leftShiftS = function leftShiftS(value) {
        for (var i = 0; i < this.rows; i++) {
          for (var j = 0; j < this.columns; j++) {
            this.set(i, j, this.get(i, j) << value);
          }
        }

        return this;
      };

      AbstractMatrix.prototype.leftShiftM = function leftShiftM(matrix) {
        matrix = Matrix.checkMatrix(matrix);

        if (this.rows !== matrix.rows || this.columns !== matrix.columns) {
          throw new RangeError('Matrices dimensions must be equal');
        }

        for (var i = 0; i < this.rows; i++) {
          for (var j = 0; j < this.columns; j++) {
            this.set(i, j, this.get(i, j) << matrix.get(i, j));
          }
        }

        return this;
      };

      AbstractMatrix.leftShift = function leftShift(matrix, value) {
        var newMatrix = new Matrix(matrix);
        return newMatrix.leftShift(value);
      };

      AbstractMatrix.prototype.signPropagatingRightShift = function signPropagatingRightShift(value) {
        if (typeof value === 'number') return this.signPropagatingRightShiftS(value);
        return this.signPropagatingRightShiftM(value);
      };

      AbstractMatrix.prototype.signPropagatingRightShiftS = function signPropagatingRightShiftS(value) {
        for (var i = 0; i < this.rows; i++) {
          for (var j = 0; j < this.columns; j++) {
            this.set(i, j, this.get(i, j) >> value);
          }
        }

        return this;
      };

      AbstractMatrix.prototype.signPropagatingRightShiftM = function signPropagatingRightShiftM(matrix) {
        matrix = Matrix.checkMatrix(matrix);

        if (this.rows !== matrix.rows || this.columns !== matrix.columns) {
          throw new RangeError('Matrices dimensions must be equal');
        }

        for (var i = 0; i < this.rows; i++) {
          for (var j = 0; j < this.columns; j++) {
            this.set(i, j, this.get(i, j) >> matrix.get(i, j));
          }
        }

        return this;
      };

      AbstractMatrix.signPropagatingRightShift = function signPropagatingRightShift(matrix, value) {
        var newMatrix = new Matrix(matrix);
        return newMatrix.signPropagatingRightShift(value);
      };

      AbstractMatrix.prototype.rightShift = function rightShift(value) {
        if (typeof value === 'number') return this.rightShiftS(value);
        return this.rightShiftM(value);
      };

      AbstractMatrix.prototype.rightShiftS = function rightShiftS(value) {
        for (var i = 0; i < this.rows; i++) {
          for (var j = 0; j < this.columns; j++) {
            this.set(i, j, this.get(i, j) >>> value);
          }
        }

        return this;
      };

      AbstractMatrix.prototype.rightShiftM = function rightShiftM(matrix) {
        matrix = Matrix.checkMatrix(matrix);

        if (this.rows !== matrix.rows || this.columns !== matrix.columns) {
          throw new RangeError('Matrices dimensions must be equal');
        }

        for (var i = 0; i < this.rows; i++) {
          for (var j = 0; j < this.columns; j++) {
            this.set(i, j, this.get(i, j) >>> matrix.get(i, j));
          }
        }

        return this;
      };

      AbstractMatrix.rightShift = function rightShift(matrix, value) {
        var newMatrix = new Matrix(matrix);
        return newMatrix.rightShift(value);
      };

      AbstractMatrix.prototype.zeroFillRightShift = AbstractMatrix.prototype.rightShift;
      AbstractMatrix.prototype.zeroFillRightShiftS = AbstractMatrix.prototype.rightShiftS;
      AbstractMatrix.prototype.zeroFillRightShiftM = AbstractMatrix.prototype.rightShiftM;
      AbstractMatrix.zeroFillRightShift = AbstractMatrix.rightShift;

      AbstractMatrix.prototype.not = function not() {
        for (var i = 0; i < this.rows; i++) {
          for (var j = 0; j < this.columns; j++) {
            this.set(i, j, ~this.get(i, j));
          }
        }

        return this;
      };

      AbstractMatrix.not = function not(matrix) {
        var newMatrix = new Matrix(matrix);
        return newMatrix.not();
      };

      AbstractMatrix.prototype.abs = function abs() {
        for (var i = 0; i < this.rows; i++) {
          for (var j = 0; j < this.columns; j++) {
            this.set(i, j, Math.abs(this.get(i, j)));
          }
        }

        return this;
      };

      AbstractMatrix.abs = function abs(matrix) {
        var newMatrix = new Matrix(matrix);
        return newMatrix.abs();
      };

      AbstractMatrix.prototype.acos = function acos() {
        for (var i = 0; i < this.rows; i++) {
          for (var j = 0; j < this.columns; j++) {
            this.set(i, j, Math.acos(this.get(i, j)));
          }
        }

        return this;
      };

      AbstractMatrix.acos = function acos(matrix) {
        var newMatrix = new Matrix(matrix);
        return newMatrix.acos();
      };

      AbstractMatrix.prototype.acosh = function acosh() {
        for (var i = 0; i < this.rows; i++) {
          for (var j = 0; j < this.columns; j++) {
            this.set(i, j, Math.acosh(this.get(i, j)));
          }
        }

        return this;
      };

      AbstractMatrix.acosh = function acosh(matrix) {
        var newMatrix = new Matrix(matrix);
        return newMatrix.acosh();
      };

      AbstractMatrix.prototype.asin = function asin() {
        for (var i = 0; i < this.rows; i++) {
          for (var j = 0; j < this.columns; j++) {
            this.set(i, j, Math.asin(this.get(i, j)));
          }
        }

        return this;
      };

      AbstractMatrix.asin = function asin(matrix) {
        var newMatrix = new Matrix(matrix);
        return newMatrix.asin();
      };

      AbstractMatrix.prototype.asinh = function asinh() {
        for (var i = 0; i < this.rows; i++) {
          for (var j = 0; j < this.columns; j++) {
            this.set(i, j, Math.asinh(this.get(i, j)));
          }
        }

        return this;
      };

      AbstractMatrix.asinh = function asinh(matrix) {
        var newMatrix = new Matrix(matrix);
        return newMatrix.asinh();
      };

      AbstractMatrix.prototype.atan = function atan() {
        for (var i = 0; i < this.rows; i++) {
          for (var j = 0; j < this.columns; j++) {
            this.set(i, j, Math.atan(this.get(i, j)));
          }
        }

        return this;
      };

      AbstractMatrix.atan = function atan(matrix) {
        var newMatrix = new Matrix(matrix);
        return newMatrix.atan();
      };

      AbstractMatrix.prototype.atanh = function atanh() {
        for (var i = 0; i < this.rows; i++) {
          for (var j = 0; j < this.columns; j++) {
            this.set(i, j, Math.atanh(this.get(i, j)));
          }
        }

        return this;
      };

      AbstractMatrix.atanh = function atanh(matrix) {
        var newMatrix = new Matrix(matrix);
        return newMatrix.atanh();
      };

      AbstractMatrix.prototype.cbrt = function cbrt() {
        for (var i = 0; i < this.rows; i++) {
          for (var j = 0; j < this.columns; j++) {
            this.set(i, j, Math.cbrt(this.get(i, j)));
          }
        }

        return this;
      };

      AbstractMatrix.cbrt = function cbrt(matrix) {
        var newMatrix = new Matrix(matrix);
        return newMatrix.cbrt();
      };

      AbstractMatrix.prototype.ceil = function ceil() {
        for (var i = 0; i < this.rows; i++) {
          for (var j = 0; j < this.columns; j++) {
            this.set(i, j, Math.ceil(this.get(i, j)));
          }
        }

        return this;
      };

      AbstractMatrix.ceil = function ceil(matrix) {
        var newMatrix = new Matrix(matrix);
        return newMatrix.ceil();
      };

      AbstractMatrix.prototype.clz32 = function clz32() {
        for (var i = 0; i < this.rows; i++) {
          for (var j = 0; j < this.columns; j++) {
            this.set(i, j, Math.clz32(this.get(i, j)));
          }
        }

        return this;
      };

      AbstractMatrix.clz32 = function clz32(matrix) {
        var newMatrix = new Matrix(matrix);
        return newMatrix.clz32();
      };

      AbstractMatrix.prototype.cos = function cos() {
        for (var i = 0; i < this.rows; i++) {
          for (var j = 0; j < this.columns; j++) {
            this.set(i, j, Math.cos(this.get(i, j)));
          }
        }

        return this;
      };

      AbstractMatrix.cos = function cos(matrix) {
        var newMatrix = new Matrix(matrix);
        return newMatrix.cos();
      };

      AbstractMatrix.prototype.cosh = function cosh() {
        for (var i = 0; i < this.rows; i++) {
          for (var j = 0; j < this.columns; j++) {
            this.set(i, j, Math.cosh(this.get(i, j)));
          }
        }

        return this;
      };

      AbstractMatrix.cosh = function cosh(matrix) {
        var newMatrix = new Matrix(matrix);
        return newMatrix.cosh();
      };

      AbstractMatrix.prototype.exp = function exp() {
        for (var i = 0; i < this.rows; i++) {
          for (var j = 0; j < this.columns; j++) {
            this.set(i, j, Math.exp(this.get(i, j)));
          }
        }

        return this;
      };

      AbstractMatrix.exp = function exp(matrix) {
        var newMatrix = new Matrix(matrix);
        return newMatrix.exp();
      };

      AbstractMatrix.prototype.expm1 = function expm1() {
        for (var i = 0; i < this.rows; i++) {
          for (var j = 0; j < this.columns; j++) {
            this.set(i, j, Math.expm1(this.get(i, j)));
          }
        }

        return this;
      };

      AbstractMatrix.expm1 = function expm1(matrix) {
        var newMatrix = new Matrix(matrix);
        return newMatrix.expm1();
      };

      AbstractMatrix.prototype.floor = function floor() {
        for (var i = 0; i < this.rows; i++) {
          for (var j = 0; j < this.columns; j++) {
            this.set(i, j, Math.floor(this.get(i, j)));
          }
        }

        return this;
      };

      AbstractMatrix.floor = function floor(matrix) {
        var newMatrix = new Matrix(matrix);
        return newMatrix.floor();
      };

      AbstractMatrix.prototype.fround = function fround() {
        for (var i = 0; i < this.rows; i++) {
          for (var j = 0; j < this.columns; j++) {
            this.set(i, j, Math.fround(this.get(i, j)));
          }
        }

        return this;
      };

      AbstractMatrix.fround = function fround(matrix) {
        var newMatrix = new Matrix(matrix);
        return newMatrix.fround();
      };

      AbstractMatrix.prototype.log = function log() {
        for (var i = 0; i < this.rows; i++) {
          for (var j = 0; j < this.columns; j++) {
            this.set(i, j, Math.log(this.get(i, j)));
          }
        }

        return this;
      };

      AbstractMatrix.log = function log(matrix) {
        var newMatrix = new Matrix(matrix);
        return newMatrix.log();
      };

      AbstractMatrix.prototype.log1p = function log1p() {
        for (var i = 0; i < this.rows; i++) {
          for (var j = 0; j < this.columns; j++) {
            this.set(i, j, Math.log1p(this.get(i, j)));
          }
        }

        return this;
      };

      AbstractMatrix.log1p = function log1p(matrix) {
        var newMatrix = new Matrix(matrix);
        return newMatrix.log1p();
      };

      AbstractMatrix.prototype.log10 = function log10() {
        for (var i = 0; i < this.rows; i++) {
          for (var j = 0; j < this.columns; j++) {
            this.set(i, j, Math.log10(this.get(i, j)));
          }
        }

        return this;
      };

      AbstractMatrix.log10 = function log10(matrix) {
        var newMatrix = new Matrix(matrix);
        return newMatrix.log10();
      };

      AbstractMatrix.prototype.log2 = function log2() {
        for (var i = 0; i < this.rows; i++) {
          for (var j = 0; j < this.columns; j++) {
            this.set(i, j, Math.log2(this.get(i, j)));
          }
        }

        return this;
      };

      AbstractMatrix.log2 = function log2(matrix) {
        var newMatrix = new Matrix(matrix);
        return newMatrix.log2();
      };

      AbstractMatrix.prototype.round = function round() {
        for (var i = 0; i < this.rows; i++) {
          for (var j = 0; j < this.columns; j++) {
            this.set(i, j, Math.round(this.get(i, j)));
          }
        }

        return this;
      };

      AbstractMatrix.round = function round(matrix) {
        var newMatrix = new Matrix(matrix);
        return newMatrix.round();
      };

      AbstractMatrix.prototype.sign = function sign() {
        for (var i = 0; i < this.rows; i++) {
          for (var j = 0; j < this.columns; j++) {
            this.set(i, j, Math.sign(this.get(i, j)));
          }
        }

        return this;
      };

      AbstractMatrix.sign = function sign(matrix) {
        var newMatrix = new Matrix(matrix);
        return newMatrix.sign();
      };

      AbstractMatrix.prototype.sin = function sin() {
        for (var i = 0; i < this.rows; i++) {
          for (var j = 0; j < this.columns; j++) {
            this.set(i, j, Math.sin(this.get(i, j)));
          }
        }

        return this;
      };

      AbstractMatrix.sin = function sin(matrix) {
        var newMatrix = new Matrix(matrix);
        return newMatrix.sin();
      };

      AbstractMatrix.prototype.sinh = function sinh() {
        for (var i = 0; i < this.rows; i++) {
          for (var j = 0; j < this.columns; j++) {
            this.set(i, j, Math.sinh(this.get(i, j)));
          }
        }

        return this;
      };

      AbstractMatrix.sinh = function sinh(matrix) {
        var newMatrix = new Matrix(matrix);
        return newMatrix.sinh();
      };

      AbstractMatrix.prototype.sqrt = function sqrt() {
        for (var i = 0; i < this.rows; i++) {
          for (var j = 0; j < this.columns; j++) {
            this.set(i, j, Math.sqrt(this.get(i, j)));
          }
        }

        return this;
      };

      AbstractMatrix.sqrt = function sqrt(matrix) {
        var newMatrix = new Matrix(matrix);
        return newMatrix.sqrt();
      };

      AbstractMatrix.prototype.tan = function tan() {
        for (var i = 0; i < this.rows; i++) {
          for (var j = 0; j < this.columns; j++) {
            this.set(i, j, Math.tan(this.get(i, j)));
          }
        }

        return this;
      };

      AbstractMatrix.tan = function tan(matrix) {
        var newMatrix = new Matrix(matrix);
        return newMatrix.tan();
      };

      AbstractMatrix.prototype.tanh = function tanh() {
        for (var i = 0; i < this.rows; i++) {
          for (var j = 0; j < this.columns; j++) {
            this.set(i, j, Math.tanh(this.get(i, j)));
          }
        }

        return this;
      };

      AbstractMatrix.tanh = function tanh(matrix) {
        var newMatrix = new Matrix(matrix);
        return newMatrix.tanh();
      };

      AbstractMatrix.prototype.trunc = function trunc() {
        for (var i = 0; i < this.rows; i++) {
          for (var j = 0; j < this.columns; j++) {
            this.set(i, j, Math.trunc(this.get(i, j)));
          }
        }

        return this;
      };

      AbstractMatrix.trunc = function trunc(matrix) {
        var newMatrix = new Matrix(matrix);
        return newMatrix.trunc();
      };

      AbstractMatrix.pow = function pow(matrix, arg0) {
        var newMatrix = new Matrix(matrix);
        return newMatrix.pow(arg0);
      };

      AbstractMatrix.prototype.pow = function pow(value) {
        if (typeof value === 'number') return this.powS(value);
        return this.powM(value);
      };

      AbstractMatrix.prototype.powS = function powS(value) {
        for (var i = 0; i < this.rows; i++) {
          for (var j = 0; j < this.columns; j++) {
            this.set(i, j, Math.pow(this.get(i, j), value));
          }
        }

        return this;
      };

      AbstractMatrix.prototype.powM = function powM(matrix) {
        matrix = Matrix.checkMatrix(matrix);

        if (this.rows !== matrix.rows || this.columns !== matrix.columns) {
          throw new RangeError('Matrices dimensions must be equal');
        }

        for (var i = 0; i < this.rows; i++) {
          for (var j = 0; j < this.columns; j++) {
            this.set(i, j, Math.pow(this.get(i, j), matrix.get(i, j)));
          }
        }

        return this;
      };
    }

    var AbstractMatrix = /*#__PURE__*/function () {
      function AbstractMatrix() {
        _classCallCheck(this, AbstractMatrix);
      }

      _createClass(AbstractMatrix, [{
        key: "apply",
        value: function apply(callback) {
          if (typeof callback !== 'function') {
            throw new TypeError('callback must be a function');
          }

          for (var i = 0; i < this.rows; i++) {
            for (var j = 0; j < this.columns; j++) {
              callback.call(this, i, j);
            }
          }

          return this;
        }
      }, {
        key: "to1DArray",
        value: function to1DArray() {
          var array = [];

          for (var i = 0; i < this.rows; i++) {
            for (var j = 0; j < this.columns; j++) {
              array.push(this.get(i, j));
            }
          }

          return array;
        }
      }, {
        key: "to2DArray",
        value: function to2DArray() {
          var copy = [];

          for (var i = 0; i < this.rows; i++) {
            copy.push([]);

            for (var j = 0; j < this.columns; j++) {
              copy[i].push(this.get(i, j));
            }
          }

          return copy;
        }
      }, {
        key: "toJSON",
        value: function toJSON() {
          return this.to2DArray();
        }
      }, {
        key: "isRowVector",
        value: function isRowVector() {
          return this.rows === 1;
        }
      }, {
        key: "isColumnVector",
        value: function isColumnVector() {
          return this.columns === 1;
        }
      }, {
        key: "isVector",
        value: function isVector() {
          return this.rows === 1 || this.columns === 1;
        }
      }, {
        key: "isSquare",
        value: function isSquare() {
          return this.rows === this.columns;
        }
      }, {
        key: "isSymmetric",
        value: function isSymmetric() {
          if (this.isSquare()) {
            for (var i = 0; i < this.rows; i++) {
              for (var j = 0; j <= i; j++) {
                if (this.get(i, j) !== this.get(j, i)) {
                  return false;
                }
              }
            }

            return true;
          }

          return false;
        }
      }, {
        key: "isEchelonForm",
        value: function isEchelonForm() {
          var i = 0;
          var j = 0;
          var previousColumn = -1;
          var isEchelonForm = true;
          var checked = false;

          while (i < this.rows && isEchelonForm) {
            j = 0;
            checked = false;

            while (j < this.columns && checked === false) {
              if (this.get(i, j) === 0) {
                j++;
              } else if (this.get(i, j) === 1 && j > previousColumn) {
                checked = true;
                previousColumn = j;
              } else {
                isEchelonForm = false;
                checked = true;
              }
            }

            i++;
          }

          return isEchelonForm;
        }
      }, {
        key: "isReducedEchelonForm",
        value: function isReducedEchelonForm() {
          var i = 0;
          var j = 0;
          var previousColumn = -1;
          var isReducedEchelonForm = true;
          var checked = false;

          while (i < this.rows && isReducedEchelonForm) {
            j = 0;
            checked = false;

            while (j < this.columns && checked === false) {
              if (this.get(i, j) === 0) {
                j++;
              } else if (this.get(i, j) === 1 && j > previousColumn) {
                checked = true;
                previousColumn = j;
              } else {
                isReducedEchelonForm = false;
                checked = true;
              }
            }

            for (var k = j + 1; k < this.rows; k++) {
              if (this.get(i, k) !== 0) {
                isReducedEchelonForm = false;
              }
            }

            i++;
          }

          return isReducedEchelonForm;
        }
      }, {
        key: "echelonForm",
        value: function echelonForm() {
          var result = this.clone();
          var h = 0;
          var k = 0;

          while (h < result.rows && k < result.columns) {
            var iMax = h;

            for (var i = h; i < result.rows; i++) {
              if (result.get(i, k) > result.get(iMax, k)) {
                iMax = i;
              }
            }

            if (result.get(iMax, k) === 0) {
              k++;
            } else {
              result.swapRows(h, iMax);
              var tmp = result.get(h, k);

              for (var j = k; j < result.columns; j++) {
                result.set(h, j, result.get(h, j) / tmp);
              }

              for (var _i2 = h + 1; _i2 < result.rows; _i2++) {
                var factor = result.get(_i2, k) / result.get(h, k);
                result.set(_i2, k, 0);

                for (var _j = k + 1; _j < result.columns; _j++) {
                  result.set(_i2, _j, result.get(_i2, _j) - result.get(h, _j) * factor);
                }
              }

              h++;
              k++;
            }
          }

          return result;
        }
      }, {
        key: "reducedEchelonForm",
        value: function reducedEchelonForm() {
          var result = this.echelonForm();
          var m = result.columns;
          var n = result.rows;
          var h = n - 1;

          while (h >= 0) {
            if (result.maxRow(h) === 0) {
              h--;
            } else {
              var p = 0;
              var pivot = false;

              while (p < n && pivot === false) {
                if (result.get(h, p) === 1) {
                  pivot = true;
                } else {
                  p++;
                }
              }

              for (var i = 0; i < h; i++) {
                var factor = result.get(i, p);

                for (var j = p; j < m; j++) {
                  var tmp = result.get(i, j) - factor * result.get(h, j);
                  result.set(i, j, tmp);
                }
              }

              h--;
            }
          }

          return result;
        }
      }, {
        key: "set",
        value: function set() {
          throw new Error('set method is unimplemented');
        }
      }, {
        key: "get",
        value: function get() {
          throw new Error('get method is unimplemented');
        }
      }, {
        key: "repeat",
        value: function repeat() {
          var options = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};

          if (_typeof(options) !== 'object') {
            throw new TypeError('options must be an object');
          }

          var _options$rows = options.rows,
              rows = _options$rows === void 0 ? 1 : _options$rows,
              _options$columns = options.columns,
              columns = _options$columns === void 0 ? 1 : _options$columns;

          if (!Number.isInteger(rows) || rows <= 0) {
            throw new TypeError('rows must be a positive integer');
          }

          if (!Number.isInteger(columns) || columns <= 0) {
            throw new TypeError('columns must be a positive integer');
          }

          var matrix = new Matrix(this.rows * rows, this.columns * columns);

          for (var i = 0; i < rows; i++) {
            for (var j = 0; j < columns; j++) {
              matrix.setSubMatrix(this, this.rows * i, this.columns * j);
            }
          }

          return matrix;
        }
      }, {
        key: "fill",
        value: function fill(value) {
          for (var i = 0; i < this.rows; i++) {
            for (var j = 0; j < this.columns; j++) {
              this.set(i, j, value);
            }
          }

          return this;
        }
      }, {
        key: "neg",
        value: function neg() {
          return this.mulS(-1);
        }
      }, {
        key: "getRow",
        value: function getRow(index) {
          checkRowIndex(this, index);
          var row = [];

          for (var i = 0; i < this.columns; i++) {
            row.push(this.get(index, i));
          }

          return row;
        }
      }, {
        key: "getRowVector",
        value: function getRowVector(index) {
          return Matrix.rowVector(this.getRow(index));
        }
      }, {
        key: "setRow",
        value: function setRow(index, array) {
          checkRowIndex(this, index);
          array = checkRowVector(this, array);

          for (var i = 0; i < this.columns; i++) {
            this.set(index, i, array[i]);
          }

          return this;
        }
      }, {
        key: "swapRows",
        value: function swapRows(row1, row2) {
          checkRowIndex(this, row1);
          checkRowIndex(this, row2);

          for (var i = 0; i < this.columns; i++) {
            var temp = this.get(row1, i);
            this.set(row1, i, this.get(row2, i));
            this.set(row2, i, temp);
          }

          return this;
        }
      }, {
        key: "getColumn",
        value: function getColumn(index) {
          checkColumnIndex(this, index);
          var column = [];

          for (var i = 0; i < this.rows; i++) {
            column.push(this.get(i, index));
          }

          return column;
        }
      }, {
        key: "getColumnVector",
        value: function getColumnVector(index) {
          return Matrix.columnVector(this.getColumn(index));
        }
      }, {
        key: "setColumn",
        value: function setColumn(index, array) {
          checkColumnIndex(this, index);
          array = checkColumnVector(this, array);

          for (var i = 0; i < this.rows; i++) {
            this.set(i, index, array[i]);
          }

          return this;
        }
      }, {
        key: "swapColumns",
        value: function swapColumns(column1, column2) {
          checkColumnIndex(this, column1);
          checkColumnIndex(this, column2);

          for (var i = 0; i < this.rows; i++) {
            var temp = this.get(i, column1);
            this.set(i, column1, this.get(i, column2));
            this.set(i, column2, temp);
          }

          return this;
        }
      }, {
        key: "addRowVector",
        value: function addRowVector(vector) {
          vector = checkRowVector(this, vector);

          for (var i = 0; i < this.rows; i++) {
            for (var j = 0; j < this.columns; j++) {
              this.set(i, j, this.get(i, j) + vector[j]);
            }
          }

          return this;
        }
      }, {
        key: "subRowVector",
        value: function subRowVector(vector) {
          vector = checkRowVector(this, vector);

          for (var i = 0; i < this.rows; i++) {
            for (var j = 0; j < this.columns; j++) {
              this.set(i, j, this.get(i, j) - vector[j]);
            }
          }

          return this;
        }
      }, {
        key: "mulRowVector",
        value: function mulRowVector(vector) {
          vector = checkRowVector(this, vector);

          for (var i = 0; i < this.rows; i++) {
            for (var j = 0; j < this.columns; j++) {
              this.set(i, j, this.get(i, j) * vector[j]);
            }
          }

          return this;
        }
      }, {
        key: "divRowVector",
        value: function divRowVector(vector) {
          vector = checkRowVector(this, vector);

          for (var i = 0; i < this.rows; i++) {
            for (var j = 0; j < this.columns; j++) {
              this.set(i, j, this.get(i, j) / vector[j]);
            }
          }

          return this;
        }
      }, {
        key: "addColumnVector",
        value: function addColumnVector(vector) {
          vector = checkColumnVector(this, vector);

          for (var i = 0; i < this.rows; i++) {
            for (var j = 0; j < this.columns; j++) {
              this.set(i, j, this.get(i, j) + vector[i]);
            }
          }

          return this;
        }
      }, {
        key: "subColumnVector",
        value: function subColumnVector(vector) {
          vector = checkColumnVector(this, vector);

          for (var i = 0; i < this.rows; i++) {
            for (var j = 0; j < this.columns; j++) {
              this.set(i, j, this.get(i, j) - vector[i]);
            }
          }

          return this;
        }
      }, {
        key: "mulColumnVector",
        value: function mulColumnVector(vector) {
          vector = checkColumnVector(this, vector);

          for (var i = 0; i < this.rows; i++) {
            for (var j = 0; j < this.columns; j++) {
              this.set(i, j, this.get(i, j) * vector[i]);
            }
          }

          return this;
        }
      }, {
        key: "divColumnVector",
        value: function divColumnVector(vector) {
          vector = checkColumnVector(this, vector);

          for (var i = 0; i < this.rows; i++) {
            for (var j = 0; j < this.columns; j++) {
              this.set(i, j, this.get(i, j) / vector[i]);
            }
          }

          return this;
        }
      }, {
        key: "mulRow",
        value: function mulRow(index, value) {
          checkRowIndex(this, index);

          for (var i = 0; i < this.columns; i++) {
            this.set(index, i, this.get(index, i) * value);
          }

          return this;
        }
      }, {
        key: "mulColumn",
        value: function mulColumn(index, value) {
          checkColumnIndex(this, index);

          for (var i = 0; i < this.rows; i++) {
            this.set(i, index, this.get(i, index) * value);
          }

          return this;
        }
      }, {
        key: "max",
        value: function max() {
          var v = this.get(0, 0);

          for (var i = 0; i < this.rows; i++) {
            for (var j = 0; j < this.columns; j++) {
              if (this.get(i, j) > v) {
                v = this.get(i, j);
              }
            }
          }

          return v;
        }
      }, {
        key: "maxIndex",
        value: function maxIndex() {
          var v = this.get(0, 0);
          var idx = [0, 0];

          for (var i = 0; i < this.rows; i++) {
            for (var j = 0; j < this.columns; j++) {
              if (this.get(i, j) > v) {
                v = this.get(i, j);
                idx[0] = i;
                idx[1] = j;
              }
            }
          }

          return idx;
        }
      }, {
        key: "min",
        value: function min() {
          var v = this.get(0, 0);

          for (var i = 0; i < this.rows; i++) {
            for (var j = 0; j < this.columns; j++) {
              if (this.get(i, j) < v) {
                v = this.get(i, j);
              }
            }
          }

          return v;
        }
      }, {
        key: "minIndex",
        value: function minIndex() {
          var v = this.get(0, 0);
          var idx = [0, 0];

          for (var i = 0; i < this.rows; i++) {
            for (var j = 0; j < this.columns; j++) {
              if (this.get(i, j) < v) {
                v = this.get(i, j);
                idx[0] = i;
                idx[1] = j;
              }
            }
          }

          return idx;
        }
      }, {
        key: "maxRow",
        value: function maxRow(row) {
          checkRowIndex(this, row);
          var v = this.get(row, 0);

          for (var i = 1; i < this.columns; i++) {
            if (this.get(row, i) > v) {
              v = this.get(row, i);
            }
          }

          return v;
        }
      }, {
        key: "maxRowIndex",
        value: function maxRowIndex(row) {
          checkRowIndex(this, row);
          var v = this.get(row, 0);
          var idx = [row, 0];

          for (var i = 1; i < this.columns; i++) {
            if (this.get(row, i) > v) {
              v = this.get(row, i);
              idx[1] = i;
            }
          }

          return idx;
        }
      }, {
        key: "minRow",
        value: function minRow(row) {
          checkRowIndex(this, row);
          var v = this.get(row, 0);

          for (var i = 1; i < this.columns; i++) {
            if (this.get(row, i) < v) {
              v = this.get(row, i);
            }
          }

          return v;
        }
      }, {
        key: "minRowIndex",
        value: function minRowIndex(row) {
          checkRowIndex(this, row);
          var v = this.get(row, 0);
          var idx = [row, 0];

          for (var i = 1; i < this.columns; i++) {
            if (this.get(row, i) < v) {
              v = this.get(row, i);
              idx[1] = i;
            }
          }

          return idx;
        }
      }, {
        key: "maxColumn",
        value: function maxColumn(column) {
          checkColumnIndex(this, column);
          var v = this.get(0, column);

          for (var i = 1; i < this.rows; i++) {
            if (this.get(i, column) > v) {
              v = this.get(i, column);
            }
          }

          return v;
        }
      }, {
        key: "maxColumnIndex",
        value: function maxColumnIndex(column) {
          checkColumnIndex(this, column);
          var v = this.get(0, column);
          var idx = [0, column];

          for (var i = 1; i < this.rows; i++) {
            if (this.get(i, column) > v) {
              v = this.get(i, column);
              idx[0] = i;
            }
          }

          return idx;
        }
      }, {
        key: "minColumn",
        value: function minColumn(column) {
          checkColumnIndex(this, column);
          var v = this.get(0, column);

          for (var i = 1; i < this.rows; i++) {
            if (this.get(i, column) < v) {
              v = this.get(i, column);
            }
          }

          return v;
        }
      }, {
        key: "minColumnIndex",
        value: function minColumnIndex(column) {
          checkColumnIndex(this, column);
          var v = this.get(0, column);
          var idx = [0, column];

          for (var i = 1; i < this.rows; i++) {
            if (this.get(i, column) < v) {
              v = this.get(i, column);
              idx[0] = i;
            }
          }

          return idx;
        }
      }, {
        key: "diag",
        value: function diag() {
          var min = Math.min(this.rows, this.columns);
          var diag = [];

          for (var i = 0; i < min; i++) {
            diag.push(this.get(i, i));
          }

          return diag;
        }
      }, {
        key: "norm",
        value: function norm() {
          var type = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : 'frobenius';
          var result = 0;

          if (type === 'max') {
            return this.max();
          } else if (type === 'frobenius') {
            for (var i = 0; i < this.rows; i++) {
              for (var j = 0; j < this.columns; j++) {
                result = result + this.get(i, j) * this.get(i, j);
              }
            }

            return Math.sqrt(result);
          } else {
            throw new RangeError("unknown norm type: ".concat(type));
          }
        }
      }, {
        key: "cumulativeSum",
        value: function cumulativeSum() {
          var sum = 0;

          for (var i = 0; i < this.rows; i++) {
            for (var j = 0; j < this.columns; j++) {
              sum += this.get(i, j);
              this.set(i, j, sum);
            }
          }

          return this;
        }
      }, {
        key: "dot",
        value: function dot(vector2) {
          if (AbstractMatrix.isMatrix(vector2)) vector2 = vector2.to1DArray();
          var vector1 = this.to1DArray();

          if (vector1.length !== vector2.length) {
            throw new RangeError('vectors do not have the same size');
          }

          var dot = 0;

          for (var i = 0; i < vector1.length; i++) {
            dot += vector1[i] * vector2[i];
          }

          return dot;
        }
      }, {
        key: "mmul",
        value: function mmul(other) {
          other = Matrix.checkMatrix(other);
          var m = this.rows;
          var n = this.columns;
          var p = other.columns;
          var result = new Matrix(m, p);
          var Bcolj = new Float64Array(n);

          for (var j = 0; j < p; j++) {
            for (var k = 0; k < n; k++) {
              Bcolj[k] = other.get(k, j);
            }

            for (var i = 0; i < m; i++) {
              var s = 0;

              for (var _k = 0; _k < n; _k++) {
                s += this.get(i, _k) * Bcolj[_k];
              }

              result.set(i, j, s);
            }
          }

          return result;
        }
      }, {
        key: "strassen2x2",
        value: function strassen2x2(other) {
          other = Matrix.checkMatrix(other);
          var result = new Matrix(2, 2);
          var a11 = this.get(0, 0);
          var b11 = other.get(0, 0);
          var a12 = this.get(0, 1);
          var b12 = other.get(0, 1);
          var a21 = this.get(1, 0);
          var b21 = other.get(1, 0);
          var a22 = this.get(1, 1);
          var b22 = other.get(1, 1); // Compute intermediate values.

          var m1 = (a11 + a22) * (b11 + b22);
          var m2 = (a21 + a22) * b11;
          var m3 = a11 * (b12 - b22);
          var m4 = a22 * (b21 - b11);
          var m5 = (a11 + a12) * b22;
          var m6 = (a21 - a11) * (b11 + b12);
          var m7 = (a12 - a22) * (b21 + b22); // Combine intermediate values into the output.

          var c00 = m1 + m4 - m5 + m7;
          var c01 = m3 + m5;
          var c10 = m2 + m4;
          var c11 = m1 - m2 + m3 + m6;
          result.set(0, 0, c00);
          result.set(0, 1, c01);
          result.set(1, 0, c10);
          result.set(1, 1, c11);
          return result;
        }
      }, {
        key: "strassen3x3",
        value: function strassen3x3(other) {
          other = Matrix.checkMatrix(other);
          var result = new Matrix(3, 3);
          var a00 = this.get(0, 0);
          var a01 = this.get(0, 1);
          var a02 = this.get(0, 2);
          var a10 = this.get(1, 0);
          var a11 = this.get(1, 1);
          var a12 = this.get(1, 2);
          var a20 = this.get(2, 0);
          var a21 = this.get(2, 1);
          var a22 = this.get(2, 2);
          var b00 = other.get(0, 0);
          var b01 = other.get(0, 1);
          var b02 = other.get(0, 2);
          var b10 = other.get(1, 0);
          var b11 = other.get(1, 1);
          var b12 = other.get(1, 2);
          var b20 = other.get(2, 0);
          var b21 = other.get(2, 1);
          var b22 = other.get(2, 2);
          var m1 = (a00 + a01 + a02 - a10 - a11 - a21 - a22) * b11;
          var m2 = (a00 - a10) * (-b01 + b11);
          var m3 = a11 * (-b00 + b01 + b10 - b11 - b12 - b20 + b22);
          var m4 = (-a00 + a10 + a11) * (b00 - b01 + b11);
          var m5 = (a10 + a11) * (-b00 + b01);
          var m6 = a00 * b00;
          var m7 = (-a00 + a20 + a21) * (b00 - b02 + b12);
          var m8 = (-a00 + a20) * (b02 - b12);
          var m9 = (a20 + a21) * (-b00 + b02);
          var m10 = (a00 + a01 + a02 - a11 - a12 - a20 - a21) * b12;
          var m11 = a21 * (-b00 + b02 + b10 - b11 - b12 - b20 + b21);
          var m12 = (-a02 + a21 + a22) * (b11 + b20 - b21);
          var m13 = (a02 - a22) * (b11 - b21);
          var m14 = a02 * b20;
          var m15 = (a21 + a22) * (-b20 + b21);
          var m16 = (-a02 + a11 + a12) * (b12 + b20 - b22);
          var m17 = (a02 - a12) * (b12 - b22);
          var m18 = (a11 + a12) * (-b20 + b22);
          var m19 = a01 * b10;
          var m20 = a12 * b21;
          var m21 = a10 * b02;
          var m22 = a20 * b01;
          var m23 = a22 * b22;
          var c00 = m6 + m14 + m19;
          var c01 = m1 + m4 + m5 + m6 + m12 + m14 + m15;
          var c02 = m6 + m7 + m9 + m10 + m14 + m16 + m18;
          var c10 = m2 + m3 + m4 + m6 + m14 + m16 + m17;
          var c11 = m2 + m4 + m5 + m6 + m20;
          var c12 = m14 + m16 + m17 + m18 + m21;
          var c20 = m6 + m7 + m8 + m11 + m12 + m13 + m14;
          var c21 = m12 + m13 + m14 + m15 + m22;
          var c22 = m6 + m7 + m8 + m9 + m23;
          result.set(0, 0, c00);
          result.set(0, 1, c01);
          result.set(0, 2, c02);
          result.set(1, 0, c10);
          result.set(1, 1, c11);
          result.set(1, 2, c12);
          result.set(2, 0, c20);
          result.set(2, 1, c21);
          result.set(2, 2, c22);
          return result;
        }
      }, {
        key: "mmulStrassen",
        value: function mmulStrassen(y) {
          y = Matrix.checkMatrix(y);
          var x = this.clone();
          var r1 = x.rows;
          var c1 = x.columns;
          var r2 = y.rows;
          var c2 = y.columns;

          if (c1 !== r2) {
            // eslint-disable-next-line no-console
            console.warn("Multiplying ".concat(r1, " x ").concat(c1, " and ").concat(r2, " x ").concat(c2, " matrix: dimensions do not match."));
          } // Put a matrix into the top left of a matrix of zeros.
          // `rows` and `cols` are the dimensions of the output matrix.


          function embed(mat, rows, cols) {
            var r = mat.rows;
            var c = mat.columns;

            if (r === rows && c === cols) {
              return mat;
            } else {
              var resultat = AbstractMatrix.zeros(rows, cols);
              resultat = resultat.setSubMatrix(mat, 0, 0);
              return resultat;
            }
          } // Make sure both matrices are the same size.
          // This is exclusively for simplicity:
          // this algorithm can be implemented with matrices of different sizes.


          var r = Math.max(r1, r2);
          var c = Math.max(c1, c2);
          x = embed(x, r, c);
          y = embed(y, r, c); // Our recursive multiplication function.

          function blockMult(a, b, rows, cols) {
            // For small matrices, resort to naive multiplication.
            if (rows <= 512 || cols <= 512) {
              return a.mmul(b); // a is equivalent to this
            } // Apply dynamic padding.


            if (rows % 2 === 1 && cols % 2 === 1) {
              a = embed(a, rows + 1, cols + 1);
              b = embed(b, rows + 1, cols + 1);
            } else if (rows % 2 === 1) {
              a = embed(a, rows + 1, cols);
              b = embed(b, rows + 1, cols);
            } else if (cols % 2 === 1) {
              a = embed(a, rows, cols + 1);
              b = embed(b, rows, cols + 1);
            }

            var halfRows = parseInt(a.rows / 2, 10);
            var halfCols = parseInt(a.columns / 2, 10); // Subdivide input matrices.

            var a11 = a.subMatrix(0, halfRows - 1, 0, halfCols - 1);
            var b11 = b.subMatrix(0, halfRows - 1, 0, halfCols - 1);
            var a12 = a.subMatrix(0, halfRows - 1, halfCols, a.columns - 1);
            var b12 = b.subMatrix(0, halfRows - 1, halfCols, b.columns - 1);
            var a21 = a.subMatrix(halfRows, a.rows - 1, 0, halfCols - 1);
            var b21 = b.subMatrix(halfRows, b.rows - 1, 0, halfCols - 1);
            var a22 = a.subMatrix(halfRows, a.rows - 1, halfCols, a.columns - 1);
            var b22 = b.subMatrix(halfRows, b.rows - 1, halfCols, b.columns - 1); // Compute intermediate values.

            var m1 = blockMult(AbstractMatrix.add(a11, a22), AbstractMatrix.add(b11, b22), halfRows, halfCols);
            var m2 = blockMult(AbstractMatrix.add(a21, a22), b11, halfRows, halfCols);
            var m3 = blockMult(a11, AbstractMatrix.sub(b12, b22), halfRows, halfCols);
            var m4 = blockMult(a22, AbstractMatrix.sub(b21, b11), halfRows, halfCols);
            var m5 = blockMult(AbstractMatrix.add(a11, a12), b22, halfRows, halfCols);
            var m6 = blockMult(AbstractMatrix.sub(a21, a11), AbstractMatrix.add(b11, b12), halfRows, halfCols);
            var m7 = blockMult(AbstractMatrix.sub(a12, a22), AbstractMatrix.add(b21, b22), halfRows, halfCols); // Combine intermediate values into the output.

            var c11 = AbstractMatrix.add(m1, m4);
            c11.sub(m5);
            c11.add(m7);
            var c12 = AbstractMatrix.add(m3, m5);
            var c21 = AbstractMatrix.add(m2, m4);
            var c22 = AbstractMatrix.sub(m1, m2);
            c22.add(m3);
            c22.add(m6); // Crop output to the desired size (undo dynamic padding).

            var resultat = AbstractMatrix.zeros(2 * c11.rows, 2 * c11.columns);
            resultat = resultat.setSubMatrix(c11, 0, 0);
            resultat = resultat.setSubMatrix(c12, c11.rows, 0);
            resultat = resultat.setSubMatrix(c21, 0, c11.columns);
            resultat = resultat.setSubMatrix(c22, c11.rows, c11.columns);
            return resultat.subMatrix(0, rows - 1, 0, cols - 1);
          }

          return blockMult(x, y, r, c);
        }
      }, {
        key: "scaleRows",
        value: function scaleRows() {
          var options = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};

          if (_typeof(options) !== 'object') {
            throw new TypeError('options must be an object');
          }

          var _options$min2 = options.min,
              min = _options$min2 === void 0 ? 0 : _options$min2,
              _options$max2 = options.max,
              max = _options$max2 === void 0 ? 1 : _options$max2;
          if (!Number.isFinite(min)) throw new TypeError('min must be a number');
          if (!Number.isFinite(max)) throw new TypeError('max must be a number');
          if (min >= max) throw new RangeError('min must be smaller than max');
          var newMatrix = new Matrix(this.rows, this.columns);

          for (var i = 0; i < this.rows; i++) {
            var row = this.getRow(i);
            rescale(row, {
              min: min,
              max: max,
              output: row
            });
            newMatrix.setRow(i, row);
          }

          return newMatrix;
        }
      }, {
        key: "scaleColumns",
        value: function scaleColumns() {
          var options = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : {};

          if (_typeof(options) !== 'object') {
            throw new TypeError('options must be an object');
          }

          var _options$min3 = options.min,
              min = _options$min3 === void 0 ? 0 : _options$min3,
              _options$max3 = options.max,
              max = _options$max3 === void 0 ? 1 : _options$max3;
          if (!Number.isFinite(min)) throw new TypeError('min must be a number');
          if (!Number.isFinite(max)) throw new TypeError('max must be a number');
          if (min >= max) throw new RangeError('min must be smaller than max');
          var newMatrix = new Matrix(this.rows, this.columns);

          for (var i = 0; i < this.columns; i++) {
            var column = this.getColumn(i);
            rescale(column, {
              min: min,
              max: max,
              output: column
            });
            newMatrix.setColumn(i, column);
          }

          return newMatrix;
        }
      }, {
        key: "flipRows",
        value: function flipRows() {
          var middle = Math.ceil(this.columns / 2);

          for (var i = 0; i < this.rows; i++) {
            for (var j = 0; j < middle; j++) {
              var first = this.get(i, j);
              var last = this.get(i, this.columns - 1 - j);
              this.set(i, j, last);
              this.set(i, this.columns - 1 - j, first);
            }
          }

          return this;
        }
      }, {
        key: "flipColumns",
        value: function flipColumns() {
          var middle = Math.ceil(this.rows / 2);

          for (var j = 0; j < this.columns; j++) {
            for (var i = 0; i < middle; i++) {
              var first = this.get(i, j);
              var last = this.get(this.rows - 1 - i, j);
              this.set(i, j, last);
              this.set(this.rows - 1 - i, j, first);
            }
          }

          return this;
        }
      }, {
        key: "kroneckerProduct",
        value: function kroneckerProduct(other) {
          other = Matrix.checkMatrix(other);
          var m = this.rows;
          var n = this.columns;
          var p = other.rows;
          var q = other.columns;
          var result = new Matrix(m * p, n * q);

          for (var i = 0; i < m; i++) {
            for (var j = 0; j < n; j++) {
              for (var k = 0; k < p; k++) {
                for (var l = 0; l < q; l++) {
                  result.set(p * i + k, q * j + l, this.get(i, j) * other.get(k, l));
                }
              }
            }
          }

          return result;
        }
      }, {
        key: "transpose",
        value: function transpose() {
          var result = new Matrix(this.columns, this.rows);

          for (var i = 0; i < this.rows; i++) {
            for (var j = 0; j < this.columns; j++) {
              result.set(j, i, this.get(i, j));
            }
          }

          return result;
        }
      }, {
        key: "sortRows",
        value: function sortRows() {
          var compareFunction = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : compareNumbers;

          for (var i = 0; i < this.rows; i++) {
            this.setRow(i, this.getRow(i).sort(compareFunction));
          }

          return this;
        }
      }, {
        key: "sortColumns",
        value: function sortColumns() {
          var compareFunction = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : compareNumbers;

          for (var i = 0; i < this.columns; i++) {
            this.setColumn(i, this.getColumn(i).sort(compareFunction));
          }

          return this;
        }
      }, {
        key: "subMatrix",
        value: function subMatrix(startRow, endRow, startColumn, endColumn) {
          checkRange(this, startRow, endRow, startColumn, endColumn);
          var newMatrix = new Matrix(endRow - startRow + 1, endColumn - startColumn + 1);

          for (var i = startRow; i <= endRow; i++) {
            for (var j = startColumn; j <= endColumn; j++) {
              newMatrix.set(i - startRow, j - startColumn, this.get(i, j));
            }
          }

          return newMatrix;
        }
      }, {
        key: "subMatrixRow",
        value: function subMatrixRow(indices, startColumn, endColumn) {
          if (startColumn === undefined) startColumn = 0;
          if (endColumn === undefined) endColumn = this.columns - 1;

          if (startColumn > endColumn || startColumn < 0 || startColumn >= this.columns || endColumn < 0 || endColumn >= this.columns) {
            throw new RangeError('Argument out of range');
          }

          var newMatrix = new Matrix(indices.length, endColumn - startColumn + 1);

          for (var i = 0; i < indices.length; i++) {
            for (var j = startColumn; j <= endColumn; j++) {
              if (indices[i] < 0 || indices[i] >= this.rows) {
                throw new RangeError("Row index out of range: ".concat(indices[i]));
              }

              newMatrix.set(i, j - startColumn, this.get(indices[i], j));
            }
          }

          return newMatrix;
        }
      }, {
        key: "subMatrixColumn",
        value: function subMatrixColumn(indices, startRow, endRow) {
          if (startRow === undefined) startRow = 0;
          if (endRow === undefined) endRow = this.rows - 1;

          if (startRow > endRow || startRow < 0 || startRow >= this.rows || endRow < 0 || endRow >= this.rows) {
            throw new RangeError('Argument out of range');
          }

          var newMatrix = new Matrix(endRow - startRow + 1, indices.length);

          for (var i = 0; i < indices.length; i++) {
            for (var j = startRow; j <= endRow; j++) {
              if (indices[i] < 0 || indices[i] >= this.columns) {
                throw new RangeError("Column index out of range: ".concat(indices[i]));
              }

              newMatrix.set(j - startRow, i, this.get(j, indices[i]));
            }
          }

          return newMatrix;
        }
      }, {
        key: "setSubMatrix",
        value: function setSubMatrix(matrix, startRow, startColumn) {
          matrix = Matrix.checkMatrix(matrix);
          var endRow = startRow + matrix.rows - 1;
          var endColumn = startColumn + matrix.columns - 1;
          checkRange(this, startRow, endRow, startColumn, endColumn);

          for (var i = 0; i < matrix.rows; i++) {
            for (var j = 0; j < matrix.columns; j++) {
              this.set(startRow + i, startColumn + j, matrix.get(i, j));
            }
          }

          return this;
        }
      }, {
        key: "selection",
        value: function selection(rowIndices, columnIndices) {
          var indices = checkIndices(this, rowIndices, columnIndices);
          var newMatrix = new Matrix(rowIndices.length, columnIndices.length);

          for (var i = 0; i < indices.row.length; i++) {
            var rowIndex = indices.row[i];

            for (var j = 0; j < indices.column.length; j++) {
              var columnIndex = indices.column[j];
              newMatrix.set(i, j, this.get(rowIndex, columnIndex));
            }
          }

          return newMatrix;
        }
      }, {
        key: "trace",
        value: function trace() {
          var min = Math.min(this.rows, this.columns);
          var trace = 0;

          for (var i = 0; i < min; i++) {
            trace += this.get(i, i);
          }

          return trace;
        }
      }, {
        key: "clone",
        value: function clone() {
          var newMatrix = new Matrix(this.rows, this.columns);

          for (var row = 0; row < this.rows; row++) {
            for (var column = 0; column < this.columns; column++) {
              newMatrix.set(row, column, this.get(row, column));
            }
          }

          return newMatrix;
        }
      }, {
        key: "sum",
        value: function sum(by) {
          switch (by) {
            case 'row':
              return sumByRow(this);

            case 'column':
              return sumByColumn(this);

            case undefined:
              return sumAll(this);

            default:
              throw new Error("invalid option: ".concat(by));
          }
        }
      }, {
        key: "product",
        value: function product(by) {
          switch (by) {
            case 'row':
              return productByRow(this);

            case 'column':
              return productByColumn(this);

            case undefined:
              return productAll(this);

            default:
              throw new Error("invalid option: ".concat(by));
          }
        }
      }, {
        key: "mean",
        value: function mean(by) {
          var sum = this.sum(by);

          switch (by) {
            case 'row':
              {
                for (var i = 0; i < this.rows; i++) {
                  sum[i] /= this.columns;
                }

                return sum;
              }

            case 'column':
              {
                for (var _i3 = 0; _i3 < this.columns; _i3++) {
                  sum[_i3] /= this.rows;
                }

                return sum;
              }

            case undefined:
              return sum / this.size;

            default:
              throw new Error("invalid option: ".concat(by));
          }
        }
      }, {
        key: "variance",
        value: function variance(by) {
          var options = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};

          if (_typeof(by) === 'object') {
            options = by;
            by = undefined;
          }

          if (_typeof(options) !== 'object') {
            throw new TypeError('options must be an object');
          }

          var _options = options,
              _options$unbiased = _options.unbiased,
              unbiased = _options$unbiased === void 0 ? true : _options$unbiased,
              _options$mean = _options.mean,
              mean = _options$mean === void 0 ? this.mean(by) : _options$mean;

          if (typeof unbiased !== 'boolean') {
            throw new TypeError('unbiased must be a boolean');
          }

          switch (by) {
            case 'row':
              {
                if (!Array.isArray(mean)) {
                  throw new TypeError('mean must be an array');
                }

                return varianceByRow(this, unbiased, mean);
              }

            case 'column':
              {
                if (!Array.isArray(mean)) {
                  throw new TypeError('mean must be an array');
                }

                return varianceByColumn(this, unbiased, mean);
              }

            case undefined:
              {
                if (typeof mean !== 'number') {
                  throw new TypeError('mean must be a number');
                }

                return varianceAll(this, unbiased, mean);
              }

            default:
              throw new Error("invalid option: ".concat(by));
          }
        }
      }, {
        key: "standardDeviation",
        value: function standardDeviation(by, options) {
          if (_typeof(by) === 'object') {
            options = by;
            by = undefined;
          }

          var variance = this.variance(by, options);

          if (by === undefined) {
            return Math.sqrt(variance);
          } else {
            for (var i = 0; i < variance.length; i++) {
              variance[i] = Math.sqrt(variance[i]);
            }

            return variance;
          }
        }
      }, {
        key: "center",
        value: function center(by) {
          var options = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};

          if (_typeof(by) === 'object') {
            options = by;
            by = undefined;
          }

          if (_typeof(options) !== 'object') {
            throw new TypeError('options must be an object');
          }

          var _options2 = options,
              _options2$center = _options2.center,
              center = _options2$center === void 0 ? this.mean(by) : _options2$center;

          switch (by) {
            case 'row':
              {
                if (!Array.isArray(center)) {
                  throw new TypeError('center must be an array');
                }

                centerByRow(this, center);
                return this;
              }

            case 'column':
              {
                if (!Array.isArray(center)) {
                  throw new TypeError('center must be an array');
                }

                centerByColumn(this, center);
                return this;
              }

            case undefined:
              {
                if (typeof center !== 'number') {
                  throw new TypeError('center must be a number');
                }

                centerAll(this, center);
                return this;
              }

            default:
              throw new Error("invalid option: ".concat(by));
          }
        }
      }, {
        key: "scale",
        value: function scale(by) {
          var options = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};

          if (_typeof(by) === 'object') {
            options = by;
            by = undefined;
          }

          if (_typeof(options) !== 'object') {
            throw new TypeError('options must be an object');
          }

          var scale = options.scale;

          switch (by) {
            case 'row':
              {
                if (scale === undefined) {
                  scale = getScaleByRow(this);
                } else if (!Array.isArray(scale)) {
                  throw new TypeError('scale must be an array');
                }

                scaleByRow(this, scale);
                return this;
              }

            case 'column':
              {
                if (scale === undefined) {
                  scale = getScaleByColumn(this);
                } else if (!Array.isArray(scale)) {
                  throw new TypeError('scale must be an array');
                }

                scaleByColumn(this, scale);
                return this;
              }

            case undefined:
              {
                if (scale === undefined) {
                  scale = getScaleAll(this);
                } else if (typeof scale !== 'number') {
                  throw new TypeError('scale must be a number');
                }

                scaleAll(this, scale);
                return this;
              }

            default:
              throw new Error("invalid option: ".concat(by));
          }
        }
      }, {
        key: "size",
        get: function get() {
          return this.rows * this.columns;
        }
      }], [{
        key: "from1DArray",
        value: function from1DArray(newRows, newColumns, newData) {
          var length = newRows * newColumns;

          if (length !== newData.length) {
            throw new RangeError('data length does not match given dimensions');
          }

          var newMatrix = new Matrix(newRows, newColumns);

          for (var row = 0; row < newRows; row++) {
            for (var column = 0; column < newColumns; column++) {
              newMatrix.set(row, column, newData[row * newColumns + column]);
            }
          }

          return newMatrix;
        }
      }, {
        key: "rowVector",
        value: function rowVector(newData) {
          var vector = new Matrix(1, newData.length);

          for (var i = 0; i < newData.length; i++) {
            vector.set(0, i, newData[i]);
          }

          return vector;
        }
      }, {
        key: "columnVector",
        value: function columnVector(newData) {
          var vector = new Matrix(newData.length, 1);

          for (var i = 0; i < newData.length; i++) {
            vector.set(i, 0, newData[i]);
          }

          return vector;
        }
      }, {
        key: "zeros",
        value: function zeros(rows, columns) {
          return new Matrix(rows, columns);
        }
      }, {
        key: "ones",
        value: function ones(rows, columns) {
          return new Matrix(rows, columns).fill(1);
        }
      }, {
        key: "rand",
        value: function rand(rows, columns) {
          var options = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : {};

          if (_typeof(options) !== 'object') {
            throw new TypeError('options must be an object');
          }

          var _options$random = options.random,
              random = _options$random === void 0 ? Math.random : _options$random;
          var matrix = new Matrix(rows, columns);

          for (var i = 0; i < rows; i++) {
            for (var j = 0; j < columns; j++) {
              matrix.set(i, j, random());
            }
          }

          return matrix;
        }
      }, {
        key: "randInt",
        value: function randInt(rows, columns) {
          var options = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : {};

          if (_typeof(options) !== 'object') {
            throw new TypeError('options must be an object');
          }

          var _options$min4 = options.min,
              min = _options$min4 === void 0 ? 0 : _options$min4,
              _options$max4 = options.max,
              max = _options$max4 === void 0 ? 1000 : _options$max4,
              _options$random2 = options.random,
              random = _options$random2 === void 0 ? Math.random : _options$random2;
          if (!Number.isInteger(min)) throw new TypeError('min must be an integer');
          if (!Number.isInteger(max)) throw new TypeError('max must be an integer');
          if (min >= max) throw new RangeError('min must be smaller than max');
          var interval = max - min;
          var matrix = new Matrix(rows, columns);

          for (var i = 0; i < rows; i++) {
            for (var j = 0; j < columns; j++) {
              var value = min + Math.round(random() * interval);
              matrix.set(i, j, value);
            }
          }

          return matrix;
        }
      }, {
        key: "eye",
        value: function eye(rows, columns, value) {
          if (columns === undefined) columns = rows;
          if (value === undefined) value = 1;
          var min = Math.min(rows, columns);
          var matrix = this.zeros(rows, columns);

          for (var i = 0; i < min; i++) {
            matrix.set(i, i, value);
          }

          return matrix;
        }
      }, {
        key: "diag",
        value: function diag(data, rows, columns) {
          var l = data.length;
          if (rows === undefined) rows = l;
          if (columns === undefined) columns = rows;
          var min = Math.min(l, rows, columns);
          var matrix = this.zeros(rows, columns);

          for (var i = 0; i < min; i++) {
            matrix.set(i, i, data[i]);
          }

          return matrix;
        }
      }, {
        key: "min",
        value: function min(matrix1, matrix2) {
          matrix1 = this.checkMatrix(matrix1);
          matrix2 = this.checkMatrix(matrix2);
          var rows = matrix1.rows;
          var columns = matrix1.columns;
          var result = new Matrix(rows, columns);

          for (var i = 0; i < rows; i++) {
            for (var j = 0; j < columns; j++) {
              result.set(i, j, Math.min(matrix1.get(i, j), matrix2.get(i, j)));
            }
          }

          return result;
        }
      }, {
        key: "max",
        value: function max(matrix1, matrix2) {
          matrix1 = this.checkMatrix(matrix1);
          matrix2 = this.checkMatrix(matrix2);
          var rows = matrix1.rows;
          var columns = matrix1.columns;
          var result = new this(rows, columns);

          for (var i = 0; i < rows; i++) {
            for (var j = 0; j < columns; j++) {
              result.set(i, j, Math.max(matrix1.get(i, j), matrix2.get(i, j)));
            }
          }

          return result;
        }
      }, {
        key: "checkMatrix",
        value: function checkMatrix(value) {
          return AbstractMatrix.isMatrix(value) ? value : new Matrix(value);
        }
      }, {
        key: "isMatrix",
        value: function isMatrix(value) {
          return value != null && value.klass === 'Matrix';
        }
      }]);

      return AbstractMatrix;
    }();

    AbstractMatrix.prototype.klass = 'Matrix';

    if (typeof Symbol !== 'undefined') {
      AbstractMatrix.prototype[Symbol.for('nodejs.util.inspect.custom')] = inspectMatrix;
    }

    function compareNumbers(a, b) {
      return a - b;
    } // Synonyms


    AbstractMatrix.random = AbstractMatrix.rand;
    AbstractMatrix.randomInt = AbstractMatrix.randInt;
    AbstractMatrix.diagonal = AbstractMatrix.diag;
    AbstractMatrix.prototype.diagonal = AbstractMatrix.prototype.diag;
    AbstractMatrix.identity = AbstractMatrix.eye;
    AbstractMatrix.prototype.negate = AbstractMatrix.prototype.neg;
    AbstractMatrix.prototype.tensorProduct = AbstractMatrix.prototype.kroneckerProduct;

    var Matrix = /*#__PURE__*/function (_AbstractMatrix) {
      _inherits(Matrix, _AbstractMatrix);

      var _super = _createSuper(Matrix);

      function Matrix(nRows, nColumns) {
        var _this;

        _classCallCheck(this, Matrix);

        _this = _super.call(this);

        if (Matrix.isMatrix(nRows)) {
          return _possibleConstructorReturn(_this, nRows.clone());
        } else if (Number.isInteger(nRows) && nRows > 0) {
          // Create an empty matrix
          _this.data = [];

          if (Number.isInteger(nColumns) && nColumns > 0) {
            for (var i = 0; i < nRows; i++) {
              _this.data.push(new Float64Array(nColumns));
            }
          } else {
            throw new TypeError('nColumns must be a positive integer');
          }
        } else if (Array.isArray(nRows)) {
          // Copy the values from the 2D array
          var arrayData = nRows;
          nRows = arrayData.length;
          nColumns = arrayData[0].length;

          if (typeof nColumns !== 'number' || nColumns === 0) {
            throw new TypeError('Data must be a 2D array with at least one element');
          }

          _this.data = [];

          for (var _i4 = 0; _i4 < nRows; _i4++) {
            if (arrayData[_i4].length !== nColumns) {
              throw new RangeError('Inconsistent array dimensions');
            }

            _this.data.push(Float64Array.from(arrayData[_i4]));
          }
        } else {
          throw new TypeError('First argument must be a positive number or an array');
        }

        _this.rows = nRows;
        _this.columns = nColumns;
        return _possibleConstructorReturn(_this, _assertThisInitialized(_this));
      }

      _createClass(Matrix, [{
        key: "set",
        value: function set(rowIndex, columnIndex, value) {
          this.data[rowIndex][columnIndex] = value;
          return this;
        }
      }, {
        key: "get",
        value: function get(rowIndex, columnIndex) {
          return this.data[rowIndex][columnIndex];
        }
      }, {
        key: "removeRow",
        value: function removeRow(index) {
          checkRowIndex(this, index);

          if (this.rows === 1) {
            throw new RangeError('A matrix cannot have less than one row');
          }

          this.data.splice(index, 1);
          this.rows -= 1;
          return this;
        }
      }, {
        key: "addRow",
        value: function addRow(index, array) {
          if (array === undefined) {
            array = index;
            index = this.rows;
          }

          checkRowIndex(this, index, true);
          array = Float64Array.from(checkRowVector(this, array));
          this.data.splice(index, 0, array);
          this.rows += 1;
          return this;
        }
      }, {
        key: "removeColumn",
        value: function removeColumn(index) {
          checkColumnIndex(this, index);

          if (this.columns === 1) {
            throw new RangeError('A matrix cannot have less than one column');
          }

          for (var i = 0; i < this.rows; i++) {
            var newRow = new Float64Array(this.columns - 1);

            for (var j = 0; j < index; j++) {
              newRow[j] = this.data[i][j];
            }

            for (var _j2 = index + 1; _j2 < this.columns; _j2++) {
              newRow[_j2 - 1] = this.data[i][_j2];
            }

            this.data[i] = newRow;
          }

          this.columns -= 1;
          return this;
        }
      }, {
        key: "addColumn",
        value: function addColumn(index, array) {
          if (typeof array === 'undefined') {
            array = index;
            index = this.columns;
          }

          checkColumnIndex(this, index, true);
          array = checkColumnVector(this, array);

          for (var i = 0; i < this.rows; i++) {
            var newRow = new Float64Array(this.columns + 1);
            var j = 0;

            for (; j < index; j++) {
              newRow[j] = this.data[i][j];
            }

            newRow[j++] = array[i];

            for (; j < this.columns + 1; j++) {
              newRow[j] = this.data[i][j - 1];
            }

            this.data[i] = newRow;
          }

          this.columns += 1;
          return this;
        }
      }]);

      return Matrix;
    }(AbstractMatrix);

    installMathOperations(AbstractMatrix, Matrix);

    var BaseView = /*#__PURE__*/function (_AbstractMatrix2) {
      _inherits(BaseView, _AbstractMatrix2);

      var _super2 = _createSuper(BaseView);

      function BaseView(matrix, rows, columns) {
        var _this2;

        _classCallCheck(this, BaseView);

        _this2 = _super2.call(this);
        _this2.matrix = matrix;
        _this2.rows = rows;
        _this2.columns = columns;
        return _this2;
      }

      return BaseView;
    }(AbstractMatrix);

    var MatrixColumnView = /*#__PURE__*/function (_BaseView) {
      _inherits(MatrixColumnView, _BaseView);

      var _super3 = _createSuper(MatrixColumnView);

      function MatrixColumnView(matrix, column) {
        var _this3;

        _classCallCheck(this, MatrixColumnView);

        checkColumnIndex(matrix, column);
        _this3 = _super3.call(this, matrix, matrix.rows, 1);
        _this3.column = column;
        return _this3;
      }

      _createClass(MatrixColumnView, [{
        key: "set",
        value: function set(rowIndex, columnIndex, value) {
          this.matrix.set(rowIndex, this.column, value);
          return this;
        }
      }, {
        key: "get",
        value: function get(rowIndex) {
          return this.matrix.get(rowIndex, this.column);
        }
      }]);

      return MatrixColumnView;
    }(BaseView);

    var MatrixColumnSelectionView = /*#__PURE__*/function (_BaseView2) {
      _inherits(MatrixColumnSelectionView, _BaseView2);

      var _super4 = _createSuper(MatrixColumnSelectionView);

      function MatrixColumnSelectionView(matrix, columnIndices) {
        var _this4;

        _classCallCheck(this, MatrixColumnSelectionView);

        columnIndices = checkColumnIndices(matrix, columnIndices);
        _this4 = _super4.call(this, matrix, matrix.rows, columnIndices.length);
        _this4.columnIndices = columnIndices;
        return _this4;
      }

      _createClass(MatrixColumnSelectionView, [{
        key: "set",
        value: function set(rowIndex, columnIndex, value) {
          this.matrix.set(rowIndex, this.columnIndices[columnIndex], value);
          return this;
        }
      }, {
        key: "get",
        value: function get(rowIndex, columnIndex) {
          return this.matrix.get(rowIndex, this.columnIndices[columnIndex]);
        }
      }]);

      return MatrixColumnSelectionView;
    }(BaseView);

    var MatrixFlipColumnView = /*#__PURE__*/function (_BaseView3) {
      _inherits(MatrixFlipColumnView, _BaseView3);

      var _super5 = _createSuper(MatrixFlipColumnView);

      function MatrixFlipColumnView(matrix) {
        _classCallCheck(this, MatrixFlipColumnView);

        return _super5.call(this, matrix, matrix.rows, matrix.columns);
      }

      _createClass(MatrixFlipColumnView, [{
        key: "set",
        value: function set(rowIndex, columnIndex, value) {
          this.matrix.set(rowIndex, this.columns - columnIndex - 1, value);
          return this;
        }
      }, {
        key: "get",
        value: function get(rowIndex, columnIndex) {
          return this.matrix.get(rowIndex, this.columns - columnIndex - 1);
        }
      }]);

      return MatrixFlipColumnView;
    }(BaseView);

    var MatrixFlipRowView = /*#__PURE__*/function (_BaseView4) {
      _inherits(MatrixFlipRowView, _BaseView4);

      var _super6 = _createSuper(MatrixFlipRowView);

      function MatrixFlipRowView(matrix) {
        _classCallCheck(this, MatrixFlipRowView);

        return _super6.call(this, matrix, matrix.rows, matrix.columns);
      }

      _createClass(MatrixFlipRowView, [{
        key: "set",
        value: function set(rowIndex, columnIndex, value) {
          this.matrix.set(this.rows - rowIndex - 1, columnIndex, value);
          return this;
        }
      }, {
        key: "get",
        value: function get(rowIndex, columnIndex) {
          return this.matrix.get(this.rows - rowIndex - 1, columnIndex);
        }
      }]);

      return MatrixFlipRowView;
    }(BaseView);

    var MatrixRowView = /*#__PURE__*/function (_BaseView5) {
      _inherits(MatrixRowView, _BaseView5);

      var _super7 = _createSuper(MatrixRowView);

      function MatrixRowView(matrix, row) {
        var _this5;

        _classCallCheck(this, MatrixRowView);

        checkRowIndex(matrix, row);
        _this5 = _super7.call(this, matrix, 1, matrix.columns);
        _this5.row = row;
        return _this5;
      }

      _createClass(MatrixRowView, [{
        key: "set",
        value: function set(rowIndex, columnIndex, value) {
          this.matrix.set(this.row, columnIndex, value);
          return this;
        }
      }, {
        key: "get",
        value: function get(rowIndex, columnIndex) {
          return this.matrix.get(this.row, columnIndex);
        }
      }]);

      return MatrixRowView;
    }(BaseView);

    var MatrixRowSelectionView = /*#__PURE__*/function (_BaseView6) {
      _inherits(MatrixRowSelectionView, _BaseView6);

      var _super8 = _createSuper(MatrixRowSelectionView);

      function MatrixRowSelectionView(matrix, rowIndices) {
        var _this6;

        _classCallCheck(this, MatrixRowSelectionView);

        rowIndices = checkRowIndices(matrix, rowIndices);
        _this6 = _super8.call(this, matrix, rowIndices.length, matrix.columns);
        _this6.rowIndices = rowIndices;
        return _this6;
      }

      _createClass(MatrixRowSelectionView, [{
        key: "set",
        value: function set(rowIndex, columnIndex, value) {
          this.matrix.set(this.rowIndices[rowIndex], columnIndex, value);
          return this;
        }
      }, {
        key: "get",
        value: function get(rowIndex, columnIndex) {
          return this.matrix.get(this.rowIndices[rowIndex], columnIndex);
        }
      }]);

      return MatrixRowSelectionView;
    }(BaseView);

    var MatrixSelectionView = /*#__PURE__*/function (_BaseView7) {
      _inherits(MatrixSelectionView, _BaseView7);

      var _super9 = _createSuper(MatrixSelectionView);

      function MatrixSelectionView(matrix, rowIndices, columnIndices) {
        var _this7;

        _classCallCheck(this, MatrixSelectionView);

        var indices = checkIndices(matrix, rowIndices, columnIndices);
        _this7 = _super9.call(this, matrix, indices.row.length, indices.column.length);
        _this7.rowIndices = indices.row;
        _this7.columnIndices = indices.column;
        return _this7;
      }

      _createClass(MatrixSelectionView, [{
        key: "set",
        value: function set(rowIndex, columnIndex, value) {
          this.matrix.set(this.rowIndices[rowIndex], this.columnIndices[columnIndex], value);
          return this;
        }
      }, {
        key: "get",
        value: function get(rowIndex, columnIndex) {
          return this.matrix.get(this.rowIndices[rowIndex], this.columnIndices[columnIndex]);
        }
      }]);

      return MatrixSelectionView;
    }(BaseView);

    var MatrixSubView = /*#__PURE__*/function (_BaseView8) {
      _inherits(MatrixSubView, _BaseView8);

      var _super10 = _createSuper(MatrixSubView);

      function MatrixSubView(matrix, startRow, endRow, startColumn, endColumn) {
        var _this8;

        _classCallCheck(this, MatrixSubView);

        checkRange(matrix, startRow, endRow, startColumn, endColumn);
        _this8 = _super10.call(this, matrix, endRow - startRow + 1, endColumn - startColumn + 1);
        _this8.startRow = startRow;
        _this8.startColumn = startColumn;
        return _this8;
      }

      _createClass(MatrixSubView, [{
        key: "set",
        value: function set(rowIndex, columnIndex, value) {
          this.matrix.set(this.startRow + rowIndex, this.startColumn + columnIndex, value);
          return this;
        }
      }, {
        key: "get",
        value: function get(rowIndex, columnIndex) {
          return this.matrix.get(this.startRow + rowIndex, this.startColumn + columnIndex);
        }
      }]);

      return MatrixSubView;
    }(BaseView);

    var MatrixTransposeView = /*#__PURE__*/function (_BaseView9) {
      _inherits(MatrixTransposeView, _BaseView9);

      var _super11 = _createSuper(MatrixTransposeView);

      function MatrixTransposeView(matrix) {
        _classCallCheck(this, MatrixTransposeView);

        return _super11.call(this, matrix, matrix.columns, matrix.rows);
      }

      _createClass(MatrixTransposeView, [{
        key: "set",
        value: function set(rowIndex, columnIndex, value) {
          this.matrix.set(columnIndex, rowIndex, value);
          return this;
        }
      }, {
        key: "get",
        value: function get(rowIndex, columnIndex) {
          return this.matrix.get(columnIndex, rowIndex);
        }
      }]);

      return MatrixTransposeView;
    }(BaseView);

    var WrapperMatrix1D = /*#__PURE__*/function (_AbstractMatrix3) {
      _inherits(WrapperMatrix1D, _AbstractMatrix3);

      var _super12 = _createSuper(WrapperMatrix1D);

      function WrapperMatrix1D(data) {
        var _this9;

        var options = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};

        _classCallCheck(this, WrapperMatrix1D);

        var _options$rows2 = options.rows,
            rows = _options$rows2 === void 0 ? 1 : _options$rows2;

        if (data.length % rows !== 0) {
          throw new Error('the data length is not divisible by the number of rows');
        }

        _this9 = _super12.call(this);
        _this9.rows = rows;
        _this9.columns = data.length / rows;
        _this9.data = data;
        return _this9;
      }

      _createClass(WrapperMatrix1D, [{
        key: "set",
        value: function set(rowIndex, columnIndex, value) {
          var index = this._calculateIndex(rowIndex, columnIndex);

          this.data[index] = value;
          return this;
        }
      }, {
        key: "get",
        value: function get(rowIndex, columnIndex) {
          var index = this._calculateIndex(rowIndex, columnIndex);

          return this.data[index];
        }
      }, {
        key: "_calculateIndex",
        value: function _calculateIndex(row, column) {
          return row * this.columns + column;
        }
      }]);

      return WrapperMatrix1D;
    }(AbstractMatrix);

    var WrapperMatrix2D = /*#__PURE__*/function (_AbstractMatrix4) {
      _inherits(WrapperMatrix2D, _AbstractMatrix4);

      var _super13 = _createSuper(WrapperMatrix2D);

      function WrapperMatrix2D(data) {
        var _this10;

        _classCallCheck(this, WrapperMatrix2D);

        _this10 = _super13.call(this);
        _this10.data = data;
        _this10.rows = data.length;
        _this10.columns = data[0].length;
        return _this10;
      }

      _createClass(WrapperMatrix2D, [{
        key: "set",
        value: function set(rowIndex, columnIndex, value) {
          this.data[rowIndex][columnIndex] = value;
          return this;
        }
      }, {
        key: "get",
        value: function get(rowIndex, columnIndex) {
          return this.data[rowIndex][columnIndex];
        }
      }]);

      return WrapperMatrix2D;
    }(AbstractMatrix);

    function wrap(array, options) {
      if (Array.isArray(array)) {
        if (array[0] && Array.isArray(array[0])) {
          return new WrapperMatrix2D(array);
        } else {
          return new WrapperMatrix1D(array, options);
        }
      } else {
        throw new Error('the argument is not an array');
      }
    }

    var LuDecomposition = /*#__PURE__*/function () {
      function LuDecomposition(matrix) {
        _classCallCheck(this, LuDecomposition);

        matrix = WrapperMatrix2D.checkMatrix(matrix);
        var lu = matrix.clone();
        var rows = lu.rows;
        var columns = lu.columns;
        var pivotVector = new Float64Array(rows);
        var pivotSign = 1;
        var i, j, k, p, s, t, v;
        var LUcolj, kmax;

        for (i = 0; i < rows; i++) {
          pivotVector[i] = i;
        }

        LUcolj = new Float64Array(rows);

        for (j = 0; j < columns; j++) {
          for (i = 0; i < rows; i++) {
            LUcolj[i] = lu.get(i, j);
          }

          for (i = 0; i < rows; i++) {
            kmax = Math.min(i, j);
            s = 0;

            for (k = 0; k < kmax; k++) {
              s += lu.get(i, k) * LUcolj[k];
            }

            LUcolj[i] -= s;
            lu.set(i, j, LUcolj[i]);
          }

          p = j;

          for (i = j + 1; i < rows; i++) {
            if (Math.abs(LUcolj[i]) > Math.abs(LUcolj[p])) {
              p = i;
            }
          }

          if (p !== j) {
            for (k = 0; k < columns; k++) {
              t = lu.get(p, k);
              lu.set(p, k, lu.get(j, k));
              lu.set(j, k, t);
            }

            v = pivotVector[p];
            pivotVector[p] = pivotVector[j];
            pivotVector[j] = v;
            pivotSign = -pivotSign;
          }

          if (j < rows && lu.get(j, j) !== 0) {
            for (i = j + 1; i < rows; i++) {
              lu.set(i, j, lu.get(i, j) / lu.get(j, j));
            }
          }
        }

        this.LU = lu;
        this.pivotVector = pivotVector;
        this.pivotSign = pivotSign;
      }

      _createClass(LuDecomposition, [{
        key: "isSingular",
        value: function isSingular() {
          var data = this.LU;
          var col = data.columns;

          for (var _j3 = 0; _j3 < col; _j3++) {
            if (data.get(_j3, _j3) === 0) {
              return true;
            }
          }

          return false;
        }
      }, {
        key: "solve",
        value: function solve(value) {
          value = Matrix.checkMatrix(value);
          var lu = this.LU;
          var rows = lu.rows;

          if (rows !== value.rows) {
            throw new Error('Invalid matrix dimensions');
          }

          if (this.isSingular()) {
            throw new Error('LU matrix is singular');
          }

          var count = value.columns;
          var X = value.subMatrixRow(this.pivotVector, 0, count - 1);
          var columns = lu.columns;
          var i, j, k;

          for (k = 0; k < columns; k++) {
            for (i = k + 1; i < columns; i++) {
              for (j = 0; j < count; j++) {
                X.set(i, j, X.get(i, j) - X.get(k, j) * lu.get(i, k));
              }
            }
          }

          for (k = columns - 1; k >= 0; k--) {
            for (j = 0; j < count; j++) {
              X.set(k, j, X.get(k, j) / lu.get(k, k));
            }

            for (i = 0; i < k; i++) {
              for (j = 0; j < count; j++) {
                X.set(i, j, X.get(i, j) - X.get(k, j) * lu.get(i, k));
              }
            }
          }

          return X;
        }
      }, {
        key: "determinant",
        get: function get() {
          var data = this.LU;

          if (!data.isSquare()) {
            throw new Error('Matrix must be square');
          }

          var determinant = this.pivotSign;
          var col = data.columns;

          for (var _j4 = 0; _j4 < col; _j4++) {
            determinant *= data.get(_j4, _j4);
          }

          return determinant;
        }
      }, {
        key: "lowerTriangularMatrix",
        get: function get() {
          var data = this.LU;
          var rows = data.rows;
          var columns = data.columns;
          var X = new Matrix(rows, columns);

          for (var _i5 = 0; _i5 < rows; _i5++) {
            for (var _j5 = 0; _j5 < columns; _j5++) {
              if (_i5 > _j5) {
                X.set(_i5, _j5, data.get(_i5, _j5));
              } else if (_i5 === _j5) {
                X.set(_i5, _j5, 1);
              } else {
                X.set(_i5, _j5, 0);
              }
            }
          }

          return X;
        }
      }, {
        key: "upperTriangularMatrix",
        get: function get() {
          var data = this.LU;
          var rows = data.rows;
          var columns = data.columns;
          var X = new Matrix(rows, columns);

          for (var _i6 = 0; _i6 < rows; _i6++) {
            for (var _j6 = 0; _j6 < columns; _j6++) {
              if (_i6 <= _j6) {
                X.set(_i6, _j6, data.get(_i6, _j6));
              } else {
                X.set(_i6, _j6, 0);
              }
            }
          }

          return X;
        }
      }, {
        key: "pivotPermutationVector",
        get: function get() {
          return Array.from(this.pivotVector);
        }
      }]);

      return LuDecomposition;
    }();

    function hypotenuse(a, b) {
      var r = 0;

      if (Math.abs(a) > Math.abs(b)) {
        r = b / a;
        return Math.abs(a) * Math.sqrt(1 + r * r);
      }

      if (b !== 0) {
        r = a / b;
        return Math.abs(b) * Math.sqrt(1 + r * r);
      }

      return 0;
    }

    var QrDecomposition = /*#__PURE__*/function () {
      function QrDecomposition(value) {
        _classCallCheck(this, QrDecomposition);

        value = WrapperMatrix2D.checkMatrix(value);
        var qr = value.clone();
        var m = value.rows;
        var n = value.columns;
        var rdiag = new Float64Array(n);
        var i, j, k, s;

        for (k = 0; k < n; k++) {
          var nrm = 0;

          for (i = k; i < m; i++) {
            nrm = hypotenuse(nrm, qr.get(i, k));
          }

          if (nrm !== 0) {
            if (qr.get(k, k) < 0) {
              nrm = -nrm;
            }

            for (i = k; i < m; i++) {
              qr.set(i, k, qr.get(i, k) / nrm);
            }

            qr.set(k, k, qr.get(k, k) + 1);

            for (j = k + 1; j < n; j++) {
              s = 0;

              for (i = k; i < m; i++) {
                s += qr.get(i, k) * qr.get(i, j);
              }

              s = -s / qr.get(k, k);

              for (i = k; i < m; i++) {
                qr.set(i, j, qr.get(i, j) + s * qr.get(i, k));
              }
            }
          }

          rdiag[k] = -nrm;
        }

        this.QR = qr;
        this.Rdiag = rdiag;
      }

      _createClass(QrDecomposition, [{
        key: "solve",
        value: function solve(value) {
          value = Matrix.checkMatrix(value);
          var qr = this.QR;
          var m = qr.rows;

          if (value.rows !== m) {
            throw new Error('Matrix row dimensions must agree');
          }

          if (!this.isFullRank()) {
            throw new Error('Matrix is rank deficient');
          }

          var count = value.columns;
          var X = value.clone();
          var n = qr.columns;
          var i, j, k, s;

          for (k = 0; k < n; k++) {
            for (j = 0; j < count; j++) {
              s = 0;

              for (i = k; i < m; i++) {
                s += qr.get(i, k) * X.get(i, j);
              }

              s = -s / qr.get(k, k);

              for (i = k; i < m; i++) {
                X.set(i, j, X.get(i, j) + s * qr.get(i, k));
              }
            }
          }

          for (k = n - 1; k >= 0; k--) {
            for (j = 0; j < count; j++) {
              X.set(k, j, X.get(k, j) / this.Rdiag[k]);
            }

            for (i = 0; i < k; i++) {
              for (j = 0; j < count; j++) {
                X.set(i, j, X.get(i, j) - X.get(k, j) * qr.get(i, k));
              }
            }
          }

          return X.subMatrix(0, n - 1, 0, count - 1);
        }
      }, {
        key: "isFullRank",
        value: function isFullRank() {
          var columns = this.QR.columns;

          for (var _i7 = 0; _i7 < columns; _i7++) {
            if (this.Rdiag[_i7] === 0) {
              return false;
            }
          }

          return true;
        }
      }, {
        key: "upperTriangularMatrix",
        get: function get() {
          var qr = this.QR;
          var n = qr.columns;
          var X = new Matrix(n, n);
          var i, j;

          for (i = 0; i < n; i++) {
            for (j = 0; j < n; j++) {
              if (i < j) {
                X.set(i, j, qr.get(i, j));
              } else if (i === j) {
                X.set(i, j, this.Rdiag[i]);
              } else {
                X.set(i, j, 0);
              }
            }
          }

          return X;
        }
      }, {
        key: "orthogonalMatrix",
        get: function get() {
          var qr = this.QR;
          var rows = qr.rows;
          var columns = qr.columns;
          var X = new Matrix(rows, columns);
          var i, j, k, s;

          for (k = columns - 1; k >= 0; k--) {
            for (i = 0; i < rows; i++) {
              X.set(i, k, 0);
            }

            X.set(k, k, 1);

            for (j = k; j < columns; j++) {
              if (qr.get(k, k) !== 0) {
                s = 0;

                for (i = k; i < rows; i++) {
                  s += qr.get(i, k) * X.get(i, j);
                }

                s = -s / qr.get(k, k);

                for (i = k; i < rows; i++) {
                  X.set(i, j, X.get(i, j) + s * qr.get(i, k));
                }
              }
            }
          }

          return X;
        }
      }]);

      return QrDecomposition;
    }();

    var SingularValueDecomposition = /*#__PURE__*/function () {
      function SingularValueDecomposition(value) {
        var options = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};

        _classCallCheck(this, SingularValueDecomposition);

        value = WrapperMatrix2D.checkMatrix(value);
        var m = value.rows;
        var n = value.columns;
        var _options$computeLeftS = options.computeLeftSingularVectors,
            computeLeftSingularVectors = _options$computeLeftS === void 0 ? true : _options$computeLeftS,
            _options$computeRight = options.computeRightSingularVectors,
            computeRightSingularVectors = _options$computeRight === void 0 ? true : _options$computeRight,
            _options$autoTranspos = options.autoTranspose,
            autoTranspose = _options$autoTranspos === void 0 ? false : _options$autoTranspos;
        var wantu = Boolean(computeLeftSingularVectors);
        var wantv = Boolean(computeRightSingularVectors);
        var swapped = false;
        var a;

        if (m < n) {
          if (!autoTranspose) {
            a = value.clone(); // eslint-disable-next-line no-console

            console.warn('Computing SVD on a matrix with more columns than rows. Consider enabling autoTranspose');
          } else {
            a = value.transpose();
            m = a.rows;
            n = a.columns;
            swapped = true;
            var aux = wantu;
            wantu = wantv;
            wantv = aux;
          }
        } else {
          a = value.clone();
        }

        var nu = Math.min(m, n);
        var ni = Math.min(m + 1, n);
        var s = new Float64Array(ni);
        var U = new Matrix(m, nu);
        var V = new Matrix(n, n);
        var e = new Float64Array(n);
        var work = new Float64Array(m);
        var si = new Float64Array(ni);

        for (var _i8 = 0; _i8 < ni; _i8++) {
          si[_i8] = _i8;
        }

        var nct = Math.min(m - 1, n);
        var nrt = Math.max(0, Math.min(n - 2, m));
        var mrc = Math.max(nct, nrt);

        for (var _k2 = 0; _k2 < mrc; _k2++) {
          if (_k2 < nct) {
            s[_k2] = 0;

            for (var _i9 = _k2; _i9 < m; _i9++) {
              s[_k2] = hypotenuse(s[_k2], a.get(_i9, _k2));
            }

            if (s[_k2] !== 0) {
              if (a.get(_k2, _k2) < 0) {
                s[_k2] = -s[_k2];
              }

              for (var _i10 = _k2; _i10 < m; _i10++) {
                a.set(_i10, _k2, a.get(_i10, _k2) / s[_k2]);
              }

              a.set(_k2, _k2, a.get(_k2, _k2) + 1);
            }

            s[_k2] = -s[_k2];
          }

          for (var _j7 = _k2 + 1; _j7 < n; _j7++) {
            if (_k2 < nct && s[_k2] !== 0) {
              var _t = 0;

              for (var _i11 = _k2; _i11 < m; _i11++) {
                _t += a.get(_i11, _k2) * a.get(_i11, _j7);
              }

              _t = -_t / a.get(_k2, _k2);

              for (var _i12 = _k2; _i12 < m; _i12++) {
                a.set(_i12, _j7, a.get(_i12, _j7) + _t * a.get(_i12, _k2));
              }
            }

            e[_j7] = a.get(_k2, _j7);
          }

          if (wantu && _k2 < nct) {
            for (var _i13 = _k2; _i13 < m; _i13++) {
              U.set(_i13, _k2, a.get(_i13, _k2));
            }
          }

          if (_k2 < nrt) {
            e[_k2] = 0;

            for (var _i14 = _k2 + 1; _i14 < n; _i14++) {
              e[_k2] = hypotenuse(e[_k2], e[_i14]);
            }

            if (e[_k2] !== 0) {
              if (e[_k2 + 1] < 0) {
                e[_k2] = 0 - e[_k2];
              }

              for (var _i15 = _k2 + 1; _i15 < n; _i15++) {
                e[_i15] /= e[_k2];
              }

              e[_k2 + 1] += 1;
            }

            e[_k2] = -e[_k2];

            if (_k2 + 1 < m && e[_k2] !== 0) {
              for (var _i16 = _k2 + 1; _i16 < m; _i16++) {
                work[_i16] = 0;
              }

              for (var _i17 = _k2 + 1; _i17 < m; _i17++) {
                for (var _j8 = _k2 + 1; _j8 < n; _j8++) {
                  work[_i17] += e[_j8] * a.get(_i17, _j8);
                }
              }

              for (var _j9 = _k2 + 1; _j9 < n; _j9++) {
                var _t2 = -e[_j9] / e[_k2 + 1];

                for (var _i18 = _k2 + 1; _i18 < m; _i18++) {
                  a.set(_i18, _j9, a.get(_i18, _j9) + _t2 * work[_i18]);
                }
              }
            }

            if (wantv) {
              for (var _i19 = _k2 + 1; _i19 < n; _i19++) {
                V.set(_i19, _k2, e[_i19]);
              }
            }
          }
        }

        var p = Math.min(n, m + 1);

        if (nct < n) {
          s[nct] = a.get(nct, nct);
        }

        if (m < p) {
          s[p - 1] = 0;
        }

        if (nrt + 1 < p) {
          e[nrt] = a.get(nrt, p - 1);
        }

        e[p - 1] = 0;

        if (wantu) {
          for (var _j10 = nct; _j10 < nu; _j10++) {
            for (var _i20 = 0; _i20 < m; _i20++) {
              U.set(_i20, _j10, 0);
            }

            U.set(_j10, _j10, 1);
          }

          for (var _k3 = nct - 1; _k3 >= 0; _k3--) {
            if (s[_k3] !== 0) {
              for (var _j11 = _k3 + 1; _j11 < nu; _j11++) {
                var _t3 = 0;

                for (var _i21 = _k3; _i21 < m; _i21++) {
                  _t3 += U.get(_i21, _k3) * U.get(_i21, _j11);
                }

                _t3 = -_t3 / U.get(_k3, _k3);

                for (var _i22 = _k3; _i22 < m; _i22++) {
                  U.set(_i22, _j11, U.get(_i22, _j11) + _t3 * U.get(_i22, _k3));
                }
              }

              for (var _i23 = _k3; _i23 < m; _i23++) {
                U.set(_i23, _k3, -U.get(_i23, _k3));
              }

              U.set(_k3, _k3, 1 + U.get(_k3, _k3));

              for (var _i24 = 0; _i24 < _k3 - 1; _i24++) {
                U.set(_i24, _k3, 0);
              }
            } else {
              for (var _i25 = 0; _i25 < m; _i25++) {
                U.set(_i25, _k3, 0);
              }

              U.set(_k3, _k3, 1);
            }
          }
        }

        if (wantv) {
          for (var _k4 = n - 1; _k4 >= 0; _k4--) {
            if (_k4 < nrt && e[_k4] !== 0) {
              for (var _j12 = _k4 + 1; _j12 < n; _j12++) {
                var _t4 = 0;

                for (var _i26 = _k4 + 1; _i26 < n; _i26++) {
                  _t4 += V.get(_i26, _k4) * V.get(_i26, _j12);
                }

                _t4 = -_t4 / V.get(_k4 + 1, _k4);

                for (var _i27 = _k4 + 1; _i27 < n; _i27++) {
                  V.set(_i27, _j12, V.get(_i27, _j12) + _t4 * V.get(_i27, _k4));
                }
              }
            }

            for (var _i28 = 0; _i28 < n; _i28++) {
              V.set(_i28, _k4, 0);
            }

            V.set(_k4, _k4, 1);
          }
        }

        var pp = p - 1;
        var eps = Number.EPSILON;

        while (p > 0) {
          var _k5 = void 0,
              kase = void 0;

          for (_k5 = p - 2; _k5 >= -1; _k5--) {
            if (_k5 === -1) {
              break;
            }

            var alpha = Number.MIN_VALUE + eps * Math.abs(s[_k5] + Math.abs(s[_k5 + 1]));

            if (Math.abs(e[_k5]) <= alpha || Number.isNaN(e[_k5])) {
              e[_k5] = 0;
              break;
            }
          }

          if (_k5 === p - 2) {
            kase = 4;
          } else {
            var ks = void 0;

            for (ks = p - 1; ks >= _k5; ks--) {
              if (ks === _k5) {
                break;
              }

              var _t5 = (ks !== p ? Math.abs(e[ks]) : 0) + (ks !== _k5 + 1 ? Math.abs(e[ks - 1]) : 0);

              if (Math.abs(s[ks]) <= eps * _t5) {
                s[ks] = 0;
                break;
              }
            }

            if (ks === _k5) {
              kase = 3;
            } else if (ks === p - 1) {
              kase = 1;
            } else {
              kase = 2;
              _k5 = ks;
            }
          }

          _k5++;

          switch (kase) {
            case 1:
              {
                var f = e[p - 2];
                e[p - 2] = 0;

                for (var _j13 = p - 2; _j13 >= _k5; _j13--) {
                  var _t6 = hypotenuse(s[_j13], f);

                  var cs = s[_j13] / _t6;
                  var sn = f / _t6;
                  s[_j13] = _t6;

                  if (_j13 !== _k5) {
                    f = -sn * e[_j13 - 1];
                    e[_j13 - 1] = cs * e[_j13 - 1];
                  }

                  if (wantv) {
                    for (var _i29 = 0; _i29 < n; _i29++) {
                      _t6 = cs * V.get(_i29, _j13) + sn * V.get(_i29, p - 1);
                      V.set(_i29, p - 1, -sn * V.get(_i29, _j13) + cs * V.get(_i29, p - 1));
                      V.set(_i29, _j13, _t6);
                    }
                  }
                }

                break;
              }

            case 2:
              {
                var _f = e[_k5 - 1];
                e[_k5 - 1] = 0;

                for (var _j14 = _k5; _j14 < p; _j14++) {
                  var _t7 = hypotenuse(s[_j14], _f);

                  var _cs = s[_j14] / _t7;

                  var _sn = _f / _t7;

                  s[_j14] = _t7;
                  _f = -_sn * e[_j14];
                  e[_j14] = _cs * e[_j14];

                  if (wantu) {
                    for (var _i30 = 0; _i30 < m; _i30++) {
                      _t7 = _cs * U.get(_i30, _j14) + _sn * U.get(_i30, _k5 - 1);
                      U.set(_i30, _k5 - 1, -_sn * U.get(_i30, _j14) + _cs * U.get(_i30, _k5 - 1));
                      U.set(_i30, _j14, _t7);
                    }
                  }
                }

                break;
              }

            case 3:
              {
                var scale = Math.max(Math.abs(s[p - 1]), Math.abs(s[p - 2]), Math.abs(e[p - 2]), Math.abs(s[_k5]), Math.abs(e[_k5]));
                var sp = s[p - 1] / scale;
                var spm1 = s[p - 2] / scale;
                var epm1 = e[p - 2] / scale;
                var sk = s[_k5] / scale;
                var ek = e[_k5] / scale;
                var b = ((spm1 + sp) * (spm1 - sp) + epm1 * epm1) / 2;
                var c = sp * epm1 * (sp * epm1);
                var shift = 0;

                if (b !== 0 || c !== 0) {
                  if (b < 0) {
                    shift = 0 - Math.sqrt(b * b + c);
                  } else {
                    shift = Math.sqrt(b * b + c);
                  }

                  shift = c / (b + shift);
                }

                var _f2 = (sk + sp) * (sk - sp) + shift;

                var g = sk * ek;

                for (var _j15 = _k5; _j15 < p - 1; _j15++) {
                  var _t8 = hypotenuse(_f2, g);

                  if (_t8 === 0) _t8 = Number.MIN_VALUE;

                  var _cs2 = _f2 / _t8;

                  var _sn2 = g / _t8;

                  if (_j15 !== _k5) {
                    e[_j15 - 1] = _t8;
                  }

                  _f2 = _cs2 * s[_j15] + _sn2 * e[_j15];
                  e[_j15] = _cs2 * e[_j15] - _sn2 * s[_j15];
                  g = _sn2 * s[_j15 + 1];
                  s[_j15 + 1] = _cs2 * s[_j15 + 1];

                  if (wantv) {
                    for (var _i31 = 0; _i31 < n; _i31++) {
                      _t8 = _cs2 * V.get(_i31, _j15) + _sn2 * V.get(_i31, _j15 + 1);
                      V.set(_i31, _j15 + 1, -_sn2 * V.get(_i31, _j15) + _cs2 * V.get(_i31, _j15 + 1));
                      V.set(_i31, _j15, _t8);
                    }
                  }

                  _t8 = hypotenuse(_f2, g);
                  if (_t8 === 0) _t8 = Number.MIN_VALUE;
                  _cs2 = _f2 / _t8;
                  _sn2 = g / _t8;
                  s[_j15] = _t8;
                  _f2 = _cs2 * e[_j15] + _sn2 * s[_j15 + 1];
                  s[_j15 + 1] = -_sn2 * e[_j15] + _cs2 * s[_j15 + 1];
                  g = _sn2 * e[_j15 + 1];
                  e[_j15 + 1] = _cs2 * e[_j15 + 1];

                  if (wantu && _j15 < m - 1) {
                    for (var _i32 = 0; _i32 < m; _i32++) {
                      _t8 = _cs2 * U.get(_i32, _j15) + _sn2 * U.get(_i32, _j15 + 1);
                      U.set(_i32, _j15 + 1, -_sn2 * U.get(_i32, _j15) + _cs2 * U.get(_i32, _j15 + 1));
                      U.set(_i32, _j15, _t8);
                    }
                  }
                }

                e[p - 2] = _f2;
                break;
              }

            case 4:
              {
                if (s[_k5] <= 0) {
                  s[_k5] = s[_k5] < 0 ? -s[_k5] : 0;

                  if (wantv) {
                    for (var _i33 = 0; _i33 <= pp; _i33++) {
                      V.set(_i33, _k5, -V.get(_i33, _k5));
                    }
                  }
                }

                while (_k5 < pp) {
                  if (s[_k5] >= s[_k5 + 1]) {
                    break;
                  }

                  var _t9 = s[_k5];
                  s[_k5] = s[_k5 + 1];
                  s[_k5 + 1] = _t9;

                  if (wantv && _k5 < n - 1) {
                    for (var _i34 = 0; _i34 < n; _i34++) {
                      _t9 = V.get(_i34, _k5 + 1);
                      V.set(_i34, _k5 + 1, V.get(_i34, _k5));
                      V.set(_i34, _k5, _t9);
                    }
                  }

                  if (wantu && _k5 < m - 1) {
                    for (var _i35 = 0; _i35 < m; _i35++) {
                      _t9 = U.get(_i35, _k5 + 1);
                      U.set(_i35, _k5 + 1, U.get(_i35, _k5));
                      U.set(_i35, _k5, _t9);
                    }
                  }

                  _k5++;
                }

                p--;
                break;
              }
            // no default
          }
        }

        if (swapped) {
          var tmp = V;
          V = U;
          U = tmp;
        }

        this.m = m;
        this.n = n;
        this.s = s;
        this.U = U;
        this.V = V;
      }

      _createClass(SingularValueDecomposition, [{
        key: "solve",
        value: function solve(value) {
          var Y = value;
          var e = this.threshold;
          var scols = this.s.length;
          var Ls = Matrix.zeros(scols, scols);

          for (var _i36 = 0; _i36 < scols; _i36++) {
            if (Math.abs(this.s[_i36]) <= e) {
              Ls.set(_i36, _i36, 0);
            } else {
              Ls.set(_i36, _i36, 1 / this.s[_i36]);
            }
          }

          var U = this.U;
          var V = this.rightSingularVectors;
          var VL = V.mmul(Ls);
          var vrows = V.rows;
          var urows = U.rows;
          var VLU = Matrix.zeros(vrows, urows);

          for (var _i37 = 0; _i37 < vrows; _i37++) {
            for (var _j16 = 0; _j16 < urows; _j16++) {
              var sum = 0;

              for (var _k6 = 0; _k6 < scols; _k6++) {
                sum += VL.get(_i37, _k6) * U.get(_j16, _k6);
              }

              VLU.set(_i37, _j16, sum);
            }
          }

          return VLU.mmul(Y);
        }
      }, {
        key: "solveForDiagonal",
        value: function solveForDiagonal(value) {
          return this.solve(Matrix.diag(value));
        }
      }, {
        key: "inverse",
        value: function inverse() {
          var V = this.V;
          var e = this.threshold;
          var vrows = V.rows;
          var vcols = V.columns;
          var X = new Matrix(vrows, this.s.length);

          for (var _i38 = 0; _i38 < vrows; _i38++) {
            for (var _j17 = 0; _j17 < vcols; _j17++) {
              if (Math.abs(this.s[_j17]) > e) {
                X.set(_i38, _j17, V.get(_i38, _j17) / this.s[_j17]);
              }
            }
          }

          var U = this.U;
          var urows = U.rows;
          var ucols = U.columns;
          var Y = new Matrix(vrows, urows);

          for (var _i39 = 0; _i39 < vrows; _i39++) {
            for (var _j18 = 0; _j18 < urows; _j18++) {
              var sum = 0;

              for (var _k7 = 0; _k7 < ucols; _k7++) {
                sum += X.get(_i39, _k7) * U.get(_j18, _k7);
              }

              Y.set(_i39, _j18, sum);
            }
          }

          return Y;
        }
      }, {
        key: "condition",
        get: function get() {
          return this.s[0] / this.s[Math.min(this.m, this.n) - 1];
        }
      }, {
        key: "norm2",
        get: function get() {
          return this.s[0];
        }
      }, {
        key: "rank",
        get: function get() {
          var tol = Math.max(this.m, this.n) * this.s[0] * Number.EPSILON;
          var r = 0;
          var s = this.s;

          for (var _i40 = 0, ii = s.length; _i40 < ii; _i40++) {
            if (s[_i40] > tol) {
              r++;
            }
          }

          return r;
        }
      }, {
        key: "diagonal",
        get: function get() {
          return Array.from(this.s);
        }
      }, {
        key: "threshold",
        get: function get() {
          return Number.EPSILON / 2 * Math.max(this.m, this.n) * this.s[0];
        }
      }, {
        key: "leftSingularVectors",
        get: function get() {
          return this.U;
        }
      }, {
        key: "rightSingularVectors",
        get: function get() {
          return this.V;
        }
      }, {
        key: "diagonalMatrix",
        get: function get() {
          return Matrix.diag(this.s);
        }
      }]);

      return SingularValueDecomposition;
    }();

    function inverse(matrix) {
      var useSVD = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : false;
      matrix = WrapperMatrix2D.checkMatrix(matrix);

      if (useSVD) {
        return new SingularValueDecomposition(matrix).inverse();
      } else {
        return solve(matrix, Matrix.eye(matrix.rows));
      }
    }

    function solve(leftHandSide, rightHandSide) {
      var useSVD = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : false;
      leftHandSide = WrapperMatrix2D.checkMatrix(leftHandSide);
      rightHandSide = WrapperMatrix2D.checkMatrix(rightHandSide);

      if (useSVD) {
        return new SingularValueDecomposition(leftHandSide).solve(rightHandSide);
      } else {
        return leftHandSide.isSquare() ? new LuDecomposition(leftHandSide).solve(rightHandSide) : new QrDecomposition(leftHandSide).solve(rightHandSide);
      }
    }

    function determinant(matrix) {
      matrix = Matrix.checkMatrix(matrix);

      if (matrix.isSquare()) {
        var _a, b, c, d;

        if (matrix.columns === 2) {
          // 2 x 2 matrix
          _a = matrix.get(0, 0);
          b = matrix.get(0, 1);
          c = matrix.get(1, 0);
          d = matrix.get(1, 1);
          return _a * d - b * c;
        } else if (matrix.columns === 3) {
          // 3 x 3 matrix
          var subMatrix0, subMatrix1, subMatrix2;
          subMatrix0 = new MatrixSelectionView(matrix, [1, 2], [1, 2]);
          subMatrix1 = new MatrixSelectionView(matrix, [1, 2], [0, 2]);
          subMatrix2 = new MatrixSelectionView(matrix, [1, 2], [0, 1]);
          _a = matrix.get(0, 0);
          b = matrix.get(0, 1);
          c = matrix.get(0, 2);
          return _a * determinant(subMatrix0) - b * determinant(subMatrix1) + c * determinant(subMatrix2);
        } else {
          // general purpose determinant using the LU decomposition
          return new LuDecomposition(matrix).determinant;
        }
      } else {
        throw Error('determinant can only be calculated for a square matrix');
      }
    }

    function xrange(n, exception) {
      var range = [];

      for (var _i41 = 0; _i41 < n; _i41++) {
        if (_i41 !== exception) {
          range.push(_i41);
        }
      }

      return range;
    }

    function dependenciesOneRow(error, matrix, index) {
      var thresholdValue = arguments.length > 3 && arguments[3] !== undefined ? arguments[3] : 10e-10;
      var thresholdError = arguments.length > 4 && arguments[4] !== undefined ? arguments[4] : 10e-10;

      if (error > thresholdError) {
        return new Array(matrix.rows + 1).fill(0);
      } else {
        var returnArray = matrix.addRow(index, [0]);

        for (var _i42 = 0; _i42 < returnArray.rows; _i42++) {
          if (Math.abs(returnArray.get(_i42, 0)) < thresholdValue) {
            returnArray.set(_i42, 0, 0);
          }
        }

        return returnArray.to1DArray();
      }
    }

    function linearDependencies(matrix) {
      var options = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};
      var _options$thresholdVal = options.thresholdValue,
          thresholdValue = _options$thresholdVal === void 0 ? 10e-10 : _options$thresholdVal,
          _options$thresholdErr = options.thresholdError,
          thresholdError = _options$thresholdErr === void 0 ? 10e-10 : _options$thresholdErr;
      matrix = Matrix.checkMatrix(matrix);
      var n = matrix.rows;
      var results = new Matrix(n, n);

      for (var _i43 = 0; _i43 < n; _i43++) {
        var b = Matrix.columnVector(matrix.getRow(_i43));
        var Abis = matrix.subMatrixRow(xrange(n, _i43)).transpose();
        var svd = new SingularValueDecomposition(Abis);
        var x = svd.solve(b);
        var error = Matrix.sub(b, Abis.mmul(x)).abs().max();
        results.setRow(_i43, dependenciesOneRow(error, x, _i43, thresholdValue, thresholdError));
      }

      return results;
    }

    function pseudoInverse(matrix) {
      var threshold = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : Number.EPSILON;
      matrix = Matrix.checkMatrix(matrix);
      var svdSolution = new SingularValueDecomposition(matrix, {
        autoTranspose: true
      });
      var U = svdSolution.leftSingularVectors;
      var V = svdSolution.rightSingularVectors;
      var s = svdSolution.diagonal;

      for (var _i44 = 0; _i44 < s.length; _i44++) {
        if (Math.abs(s[_i44]) > threshold) {
          s[_i44] = 1.0 / s[_i44];
        } else {
          s[_i44] = 0.0;
        }
      }

      return V.mmul(Matrix.diag(s).mmul(U.transpose()));
    }

    function covariance(xMatrix) {
      var yMatrix = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : xMatrix;
      var options = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : {};
      xMatrix = Matrix.checkMatrix(xMatrix);
      var yIsSame = false;

      if (_typeof(yMatrix) === 'object' && !Matrix.isMatrix(yMatrix) && !Array.isArray(yMatrix)) {
        options = yMatrix;
        yMatrix = xMatrix;
        yIsSame = true;
      } else {
        yMatrix = Matrix.checkMatrix(yMatrix);
      }

      if (xMatrix.rows !== yMatrix.rows) {
        throw new TypeError('Both matrices must have the same number of rows');
      }

      var _options3 = options,
          _options3$center = _options3.center,
          center = _options3$center === void 0 ? true : _options3$center;

      if (center) {
        xMatrix = xMatrix.center('column');

        if (!yIsSame) {
          yMatrix = yMatrix.center('column');
        }
      }

      var cov = xMatrix.transpose().mmul(yMatrix);

      for (var _i45 = 0; _i45 < cov.rows; _i45++) {
        for (var _j19 = 0; _j19 < cov.columns; _j19++) {
          cov.set(_i45, _j19, cov.get(_i45, _j19) * (1 / (xMatrix.rows - 1)));
        }
      }

      return cov;
    }

    function correlation(xMatrix) {
      var yMatrix = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : xMatrix;
      var options = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : {};
      xMatrix = Matrix.checkMatrix(xMatrix);
      var yIsSame = false;

      if (_typeof(yMatrix) === 'object' && !Matrix.isMatrix(yMatrix) && !Array.isArray(yMatrix)) {
        options = yMatrix;
        yMatrix = xMatrix;
        yIsSame = true;
      } else {
        yMatrix = Matrix.checkMatrix(yMatrix);
      }

      if (xMatrix.rows !== yMatrix.rows) {
        throw new TypeError('Both matrices must have the same number of rows');
      }

      var _options4 = options,
          _options4$center = _options4.center,
          center = _options4$center === void 0 ? true : _options4$center,
          _options4$scale = _options4.scale,
          scale = _options4$scale === void 0 ? true : _options4$scale;

      if (center) {
        xMatrix.center('column');

        if (!yIsSame) {
          yMatrix.center('column');
        }
      }

      if (scale) {
        xMatrix.scale('column');

        if (!yIsSame) {
          yMatrix.scale('column');
        }
      }

      var sdx = xMatrix.standardDeviation('column', {
        unbiased: true
      });
      var sdy = yIsSame ? sdx : yMatrix.standardDeviation('column', {
        unbiased: true
      });
      var corr = xMatrix.transpose().mmul(yMatrix);

      for (var _i46 = 0; _i46 < corr.rows; _i46++) {
        for (var _j20 = 0; _j20 < corr.columns; _j20++) {
          corr.set(_i46, _j20, corr.get(_i46, _j20) * (1 / (sdx[_i46] * sdy[_j20])) * (1 / (xMatrix.rows - 1)));
        }
      }

      return corr;
    }

    var EigenvalueDecomposition = /*#__PURE__*/function () {
      function EigenvalueDecomposition(matrix) {
        var options = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};

        _classCallCheck(this, EigenvalueDecomposition);

        var _options$assumeSymmet = options.assumeSymmetric,
            assumeSymmetric = _options$assumeSymmet === void 0 ? false : _options$assumeSymmet;
        matrix = WrapperMatrix2D.checkMatrix(matrix);

        if (!matrix.isSquare()) {
          throw new Error('Matrix is not a square matrix');
        }

        var n = matrix.columns;
        var V = new Matrix(n, n);
        var d = new Float64Array(n);
        var e = new Float64Array(n);
        var value = matrix;
        var i, j;
        var isSymmetric = false;

        if (assumeSymmetric) {
          isSymmetric = true;
        } else {
          isSymmetric = matrix.isSymmetric();
        }

        if (isSymmetric) {
          for (i = 0; i < n; i++) {
            for (j = 0; j < n; j++) {
              V.set(i, j, value.get(i, j));
            }
          }

          tred2(n, e, d, V);
          tql2(n, e, d, V);
        } else {
          var H = new Matrix(n, n);
          var ort = new Float64Array(n);

          for (j = 0; j < n; j++) {
            for (i = 0; i < n; i++) {
              H.set(i, j, value.get(i, j));
            }
          }

          orthes(n, H, ort, V);
          hqr2(n, e, d, V, H);
        }

        this.n = n;
        this.e = e;
        this.d = d;
        this.V = V;
      }

      _createClass(EigenvalueDecomposition, [{
        key: "realEigenvalues",
        get: function get() {
          return Array.from(this.d);
        }
      }, {
        key: "imaginaryEigenvalues",
        get: function get() {
          return Array.from(this.e);
        }
      }, {
        key: "eigenvectorMatrix",
        get: function get() {
          return this.V;
        }
      }, {
        key: "diagonalMatrix",
        get: function get() {
          var n = this.n;
          var e = this.e;
          var d = this.d;
          var X = new Matrix(n, n);
          var i, j;

          for (i = 0; i < n; i++) {
            for (j = 0; j < n; j++) {
              X.set(i, j, 0);
            }

            X.set(i, i, d[i]);

            if (e[i] > 0) {
              X.set(i, i + 1, e[i]);
            } else if (e[i] < 0) {
              X.set(i, i - 1, e[i]);
            }
          }

          return X;
        }
      }]);

      return EigenvalueDecomposition;
    }();

    function tred2(n, e, d, V) {
      var f, g, h, i, j, k, hh, scale;

      for (j = 0; j < n; j++) {
        d[j] = V.get(n - 1, j);
      }

      for (i = n - 1; i > 0; i--) {
        scale = 0;
        h = 0;

        for (k = 0; k < i; k++) {
          scale = scale + Math.abs(d[k]);
        }

        if (scale === 0) {
          e[i] = d[i - 1];

          for (j = 0; j < i; j++) {
            d[j] = V.get(i - 1, j);
            V.set(i, j, 0);
            V.set(j, i, 0);
          }
        } else {
          for (k = 0; k < i; k++) {
            d[k] /= scale;
            h += d[k] * d[k];
          }

          f = d[i - 1];
          g = Math.sqrt(h);

          if (f > 0) {
            g = -g;
          }

          e[i] = scale * g;
          h = h - f * g;
          d[i - 1] = f - g;

          for (j = 0; j < i; j++) {
            e[j] = 0;
          }

          for (j = 0; j < i; j++) {
            f = d[j];
            V.set(j, i, f);
            g = e[j] + V.get(j, j) * f;

            for (k = j + 1; k <= i - 1; k++) {
              g += V.get(k, j) * d[k];
              e[k] += V.get(k, j) * f;
            }

            e[j] = g;
          }

          f = 0;

          for (j = 0; j < i; j++) {
            e[j] /= h;
            f += e[j] * d[j];
          }

          hh = f / (h + h);

          for (j = 0; j < i; j++) {
            e[j] -= hh * d[j];
          }

          for (j = 0; j < i; j++) {
            f = d[j];
            g = e[j];

            for (k = j; k <= i - 1; k++) {
              V.set(k, j, V.get(k, j) - (f * e[k] + g * d[k]));
            }

            d[j] = V.get(i - 1, j);
            V.set(i, j, 0);
          }
        }

        d[i] = h;
      }

      for (i = 0; i < n - 1; i++) {
        V.set(n - 1, i, V.get(i, i));
        V.set(i, i, 1);
        h = d[i + 1];

        if (h !== 0) {
          for (k = 0; k <= i; k++) {
            d[k] = V.get(k, i + 1) / h;
          }

          for (j = 0; j <= i; j++) {
            g = 0;

            for (k = 0; k <= i; k++) {
              g += V.get(k, i + 1) * V.get(k, j);
            }

            for (k = 0; k <= i; k++) {
              V.set(k, j, V.get(k, j) - g * d[k]);
            }
          }
        }

        for (k = 0; k <= i; k++) {
          V.set(k, i + 1, 0);
        }
      }

      for (j = 0; j < n; j++) {
        d[j] = V.get(n - 1, j);
        V.set(n - 1, j, 0);
      }

      V.set(n - 1, n - 1, 1);
      e[0] = 0;
    }

    function tql2(n, e, d, V) {
      var g, h, i, j, k, l, m, p, r, dl1, c, c2, c3, el1, s, s2;

      for (i = 1; i < n; i++) {
        e[i - 1] = e[i];
      }

      e[n - 1] = 0;
      var f = 0;
      var tst1 = 0;
      var eps = Number.EPSILON;

      for (l = 0; l < n; l++) {
        tst1 = Math.max(tst1, Math.abs(d[l]) + Math.abs(e[l]));
        m = l;

        while (m < n) {
          if (Math.abs(e[m]) <= eps * tst1) {
            break;
          }

          m++;
        }

        if (m > l) {
          do {
            g = d[l];
            p = (d[l + 1] - g) / (2 * e[l]);
            r = hypotenuse(p, 1);

            if (p < 0) {
              r = -r;
            }

            d[l] = e[l] / (p + r);
            d[l + 1] = e[l] * (p + r);
            dl1 = d[l + 1];
            h = g - d[l];

            for (i = l + 2; i < n; i++) {
              d[i] -= h;
            }

            f = f + h;
            p = d[m];
            c = 1;
            c2 = c;
            c3 = c;
            el1 = e[l + 1];
            s = 0;
            s2 = 0;

            for (i = m - 1; i >= l; i--) {
              c3 = c2;
              c2 = c;
              s2 = s;
              g = c * e[i];
              h = c * p;
              r = hypotenuse(p, e[i]);
              e[i + 1] = s * r;
              s = e[i] / r;
              c = p / r;
              p = c * d[i] - s * g;
              d[i + 1] = h + s * (c * g + s * d[i]);

              for (k = 0; k < n; k++) {
                h = V.get(k, i + 1);
                V.set(k, i + 1, s * V.get(k, i) + c * h);
                V.set(k, i, c * V.get(k, i) - s * h);
              }
            }

            p = -s * s2 * c3 * el1 * e[l] / dl1;
            e[l] = s * p;
            d[l] = c * p;
          } while (Math.abs(e[l]) > eps * tst1);
        }

        d[l] = d[l] + f;
        e[l] = 0;
      }

      for (i = 0; i < n - 1; i++) {
        k = i;
        p = d[i];

        for (j = i + 1; j < n; j++) {
          if (d[j] < p) {
            k = j;
            p = d[j];
          }
        }

        if (k !== i) {
          d[k] = d[i];
          d[i] = p;

          for (j = 0; j < n; j++) {
            p = V.get(j, i);
            V.set(j, i, V.get(j, k));
            V.set(j, k, p);
          }
        }
      }
    }

    function orthes(n, H, ort, V) {
      var low = 0;
      var high = n - 1;
      var f, g, h, i, j, m;
      var scale;

      for (m = low + 1; m <= high - 1; m++) {
        scale = 0;

        for (i = m; i <= high; i++) {
          scale = scale + Math.abs(H.get(i, m - 1));
        }

        if (scale !== 0) {
          h = 0;

          for (i = high; i >= m; i--) {
            ort[i] = H.get(i, m - 1) / scale;
            h += ort[i] * ort[i];
          }

          g = Math.sqrt(h);

          if (ort[m] > 0) {
            g = -g;
          }

          h = h - ort[m] * g;
          ort[m] = ort[m] - g;

          for (j = m; j < n; j++) {
            f = 0;

            for (i = high; i >= m; i--) {
              f += ort[i] * H.get(i, j);
            }

            f = f / h;

            for (i = m; i <= high; i++) {
              H.set(i, j, H.get(i, j) - f * ort[i]);
            }
          }

          for (i = 0; i <= high; i++) {
            f = 0;

            for (j = high; j >= m; j--) {
              f += ort[j] * H.get(i, j);
            }

            f = f / h;

            for (j = m; j <= high; j++) {
              H.set(i, j, H.get(i, j) - f * ort[j]);
            }
          }

          ort[m] = scale * ort[m];
          H.set(m, m - 1, scale * g);
        }
      }

      for (i = 0; i < n; i++) {
        for (j = 0; j < n; j++) {
          V.set(i, j, i === j ? 1 : 0);
        }
      }

      for (m = high - 1; m >= low + 1; m--) {
        if (H.get(m, m - 1) !== 0) {
          for (i = m + 1; i <= high; i++) {
            ort[i] = H.get(i, m - 1);
          }

          for (j = m; j <= high; j++) {
            g = 0;

            for (i = m; i <= high; i++) {
              g += ort[i] * V.get(i, j);
            }

            g = g / ort[m] / H.get(m, m - 1);

            for (i = m; i <= high; i++) {
              V.set(i, j, V.get(i, j) + g * ort[i]);
            }
          }
        }
      }
    }

    function hqr2(nn, e, d, V, H) {
      var n = nn - 1;
      var low = 0;
      var high = nn - 1;
      var eps = Number.EPSILON;
      var exshift = 0;
      var norm = 0;
      var p = 0;
      var q = 0;
      var r = 0;
      var s = 0;
      var z = 0;
      var iter = 0;
      var i, j, k, l, m, t, w, x, y;
      var ra, sa, vr, vi;
      var notlast, cdivres;

      for (i = 0; i < nn; i++) {
        if (i < low || i > high) {
          d[i] = H.get(i, i);
          e[i] = 0;
        }

        for (j = Math.max(i - 1, 0); j < nn; j++) {
          norm = norm + Math.abs(H.get(i, j));
        }
      }

      while (n >= low) {
        l = n;

        while (l > low) {
          s = Math.abs(H.get(l - 1, l - 1)) + Math.abs(H.get(l, l));

          if (s === 0) {
            s = norm;
          }

          if (Math.abs(H.get(l, l - 1)) < eps * s) {
            break;
          }

          l--;
        }

        if (l === n) {
          H.set(n, n, H.get(n, n) + exshift);
          d[n] = H.get(n, n);
          e[n] = 0;
          n--;
          iter = 0;
        } else if (l === n - 1) {
          w = H.get(n, n - 1) * H.get(n - 1, n);
          p = (H.get(n - 1, n - 1) - H.get(n, n)) / 2;
          q = p * p + w;
          z = Math.sqrt(Math.abs(q));
          H.set(n, n, H.get(n, n) + exshift);
          H.set(n - 1, n - 1, H.get(n - 1, n - 1) + exshift);
          x = H.get(n, n);

          if (q >= 0) {
            z = p >= 0 ? p + z : p - z;
            d[n - 1] = x + z;
            d[n] = d[n - 1];

            if (z !== 0) {
              d[n] = x - w / z;
            }

            e[n - 1] = 0;
            e[n] = 0;
            x = H.get(n, n - 1);
            s = Math.abs(x) + Math.abs(z);
            p = x / s;
            q = z / s;
            r = Math.sqrt(p * p + q * q);
            p = p / r;
            q = q / r;

            for (j = n - 1; j < nn; j++) {
              z = H.get(n - 1, j);
              H.set(n - 1, j, q * z + p * H.get(n, j));
              H.set(n, j, q * H.get(n, j) - p * z);
            }

            for (i = 0; i <= n; i++) {
              z = H.get(i, n - 1);
              H.set(i, n - 1, q * z + p * H.get(i, n));
              H.set(i, n, q * H.get(i, n) - p * z);
            }

            for (i = low; i <= high; i++) {
              z = V.get(i, n - 1);
              V.set(i, n - 1, q * z + p * V.get(i, n));
              V.set(i, n, q * V.get(i, n) - p * z);
            }
          } else {
            d[n - 1] = x + p;
            d[n] = x + p;
            e[n - 1] = z;
            e[n] = -z;
          }

          n = n - 2;
          iter = 0;
        } else {
          x = H.get(n, n);
          y = 0;
          w = 0;

          if (l < n) {
            y = H.get(n - 1, n - 1);
            w = H.get(n, n - 1) * H.get(n - 1, n);
          }

          if (iter === 10) {
            exshift += x;

            for (i = low; i <= n; i++) {
              H.set(i, i, H.get(i, i) - x);
            }

            s = Math.abs(H.get(n, n - 1)) + Math.abs(H.get(n - 1, n - 2));
            x = y = 0.75 * s;
            w = -0.4375 * s * s;
          }

          if (iter === 30) {
            s = (y - x) / 2;
            s = s * s + w;

            if (s > 0) {
              s = Math.sqrt(s);

              if (y < x) {
                s = -s;
              }

              s = x - w / ((y - x) / 2 + s);

              for (i = low; i <= n; i++) {
                H.set(i, i, H.get(i, i) - s);
              }

              exshift += s;
              x = y = w = 0.964;
            }
          }

          iter = iter + 1;
          m = n - 2;

          while (m >= l) {
            z = H.get(m, m);
            r = x - z;
            s = y - z;
            p = (r * s - w) / H.get(m + 1, m) + H.get(m, m + 1);
            q = H.get(m + 1, m + 1) - z - r - s;
            r = H.get(m + 2, m + 1);
            s = Math.abs(p) + Math.abs(q) + Math.abs(r);
            p = p / s;
            q = q / s;
            r = r / s;

            if (m === l) {
              break;
            }

            if (Math.abs(H.get(m, m - 1)) * (Math.abs(q) + Math.abs(r)) < eps * (Math.abs(p) * (Math.abs(H.get(m - 1, m - 1)) + Math.abs(z) + Math.abs(H.get(m + 1, m + 1))))) {
              break;
            }

            m--;
          }

          for (i = m + 2; i <= n; i++) {
            H.set(i, i - 2, 0);

            if (i > m + 2) {
              H.set(i, i - 3, 0);
            }
          }

          for (k = m; k <= n - 1; k++) {
            notlast = k !== n - 1;

            if (k !== m) {
              p = H.get(k, k - 1);
              q = H.get(k + 1, k - 1);
              r = notlast ? H.get(k + 2, k - 1) : 0;
              x = Math.abs(p) + Math.abs(q) + Math.abs(r);

              if (x !== 0) {
                p = p / x;
                q = q / x;
                r = r / x;
              }
            }

            if (x === 0) {
              break;
            }

            s = Math.sqrt(p * p + q * q + r * r);

            if (p < 0) {
              s = -s;
            }

            if (s !== 0) {
              if (k !== m) {
                H.set(k, k - 1, -s * x);
              } else if (l !== m) {
                H.set(k, k - 1, -H.get(k, k - 1));
              }

              p = p + s;
              x = p / s;
              y = q / s;
              z = r / s;
              q = q / p;
              r = r / p;

              for (j = k; j < nn; j++) {
                p = H.get(k, j) + q * H.get(k + 1, j);

                if (notlast) {
                  p = p + r * H.get(k + 2, j);
                  H.set(k + 2, j, H.get(k + 2, j) - p * z);
                }

                H.set(k, j, H.get(k, j) - p * x);
                H.set(k + 1, j, H.get(k + 1, j) - p * y);
              }

              for (i = 0; i <= Math.min(n, k + 3); i++) {
                p = x * H.get(i, k) + y * H.get(i, k + 1);

                if (notlast) {
                  p = p + z * H.get(i, k + 2);
                  H.set(i, k + 2, H.get(i, k + 2) - p * r);
                }

                H.set(i, k, H.get(i, k) - p);
                H.set(i, k + 1, H.get(i, k + 1) - p * q);
              }

              for (i = low; i <= high; i++) {
                p = x * V.get(i, k) + y * V.get(i, k + 1);

                if (notlast) {
                  p = p + z * V.get(i, k + 2);
                  V.set(i, k + 2, V.get(i, k + 2) - p * r);
                }

                V.set(i, k, V.get(i, k) - p);
                V.set(i, k + 1, V.get(i, k + 1) - p * q);
              }
            }
          }
        }
      }

      if (norm === 0) {
        return;
      }

      for (n = nn - 1; n >= 0; n--) {
        p = d[n];
        q = e[n];

        if (q === 0) {
          l = n;
          H.set(n, n, 1);

          for (i = n - 1; i >= 0; i--) {
            w = H.get(i, i) - p;
            r = 0;

            for (j = l; j <= n; j++) {
              r = r + H.get(i, j) * H.get(j, n);
            }

            if (e[i] < 0) {
              z = w;
              s = r;
            } else {
              l = i;

              if (e[i] === 0) {
                H.set(i, n, w !== 0 ? -r / w : -r / (eps * norm));
              } else {
                x = H.get(i, i + 1);
                y = H.get(i + 1, i);
                q = (d[i] - p) * (d[i] - p) + e[i] * e[i];
                t = (x * s - z * r) / q;
                H.set(i, n, t);
                H.set(i + 1, n, Math.abs(x) > Math.abs(z) ? (-r - w * t) / x : (-s - y * t) / z);
              }

              t = Math.abs(H.get(i, n));

              if (eps * t * t > 1) {
                for (j = i; j <= n; j++) {
                  H.set(j, n, H.get(j, n) / t);
                }
              }
            }
          }
        } else if (q < 0) {
          l = n - 1;

          if (Math.abs(H.get(n, n - 1)) > Math.abs(H.get(n - 1, n))) {
            H.set(n - 1, n - 1, q / H.get(n, n - 1));
            H.set(n - 1, n, -(H.get(n, n) - p) / H.get(n, n - 1));
          } else {
            cdivres = cdiv(0, -H.get(n - 1, n), H.get(n - 1, n - 1) - p, q);
            H.set(n - 1, n - 1, cdivres[0]);
            H.set(n - 1, n, cdivres[1]);
          }

          H.set(n, n - 1, 0);
          H.set(n, n, 1);

          for (i = n - 2; i >= 0; i--) {
            ra = 0;
            sa = 0;

            for (j = l; j <= n; j++) {
              ra = ra + H.get(i, j) * H.get(j, n - 1);
              sa = sa + H.get(i, j) * H.get(j, n);
            }

            w = H.get(i, i) - p;

            if (e[i] < 0) {
              z = w;
              r = ra;
              s = sa;
            } else {
              l = i;

              if (e[i] === 0) {
                cdivres = cdiv(-ra, -sa, w, q);
                H.set(i, n - 1, cdivres[0]);
                H.set(i, n, cdivres[1]);
              } else {
                x = H.get(i, i + 1);
                y = H.get(i + 1, i);
                vr = (d[i] - p) * (d[i] - p) + e[i] * e[i] - q * q;
                vi = (d[i] - p) * 2 * q;

                if (vr === 0 && vi === 0) {
                  vr = eps * norm * (Math.abs(w) + Math.abs(q) + Math.abs(x) + Math.abs(y) + Math.abs(z));
                }

                cdivres = cdiv(x * r - z * ra + q * sa, x * s - z * sa - q * ra, vr, vi);
                H.set(i, n - 1, cdivres[0]);
                H.set(i, n, cdivres[1]);

                if (Math.abs(x) > Math.abs(z) + Math.abs(q)) {
                  H.set(i + 1, n - 1, (-ra - w * H.get(i, n - 1) + q * H.get(i, n)) / x);
                  H.set(i + 1, n, (-sa - w * H.get(i, n) - q * H.get(i, n - 1)) / x);
                } else {
                  cdivres = cdiv(-r - y * H.get(i, n - 1), -s - y * H.get(i, n), z, q);
                  H.set(i + 1, n - 1, cdivres[0]);
                  H.set(i + 1, n, cdivres[1]);
                }
              }

              t = Math.max(Math.abs(H.get(i, n - 1)), Math.abs(H.get(i, n)));

              if (eps * t * t > 1) {
                for (j = i; j <= n; j++) {
                  H.set(j, n - 1, H.get(j, n - 1) / t);
                  H.set(j, n, H.get(j, n) / t);
                }
              }
            }
          }
        }
      }

      for (i = 0; i < nn; i++) {
        if (i < low || i > high) {
          for (j = i; j < nn; j++) {
            V.set(i, j, H.get(i, j));
          }
        }
      }

      for (j = nn - 1; j >= low; j--) {
        for (i = low; i <= high; i++) {
          z = 0;

          for (k = low; k <= Math.min(j, high); k++) {
            z = z + V.get(i, k) * H.get(k, j);
          }

          V.set(i, j, z);
        }
      }
    }

    function cdiv(xr, xi, yr, yi) {
      var r, d;

      if (Math.abs(yr) > Math.abs(yi)) {
        r = yi / yr;
        d = yr + r * yi;
        return [(xr + r * xi) / d, (xi - r * xr) / d];
      } else {
        r = yr / yi;
        d = yi + r * yr;
        return [(r * xr + xi) / d, (r * xi - xr) / d];
      }
    }

    var CholeskyDecomposition = /*#__PURE__*/function () {
      function CholeskyDecomposition(value) {
        _classCallCheck(this, CholeskyDecomposition);

        value = WrapperMatrix2D.checkMatrix(value);

        if (!value.isSymmetric()) {
          throw new Error('Matrix is not symmetric');
        }

        var a = value;
        var dimension = a.rows;
        var l = new Matrix(dimension, dimension);
        var positiveDefinite = true;
        var i, j, k;

        for (j = 0; j < dimension; j++) {
          var d = 0;

          for (k = 0; k < j; k++) {
            var _s = 0;

            for (i = 0; i < k; i++) {
              _s += l.get(k, i) * l.get(j, i);
            }

            _s = (a.get(j, k) - _s) / l.get(k, k);
            l.set(j, k, _s);
            d = d + _s * _s;
          }

          d = a.get(j, j) - d;
          positiveDefinite &= d > 0;
          l.set(j, j, Math.sqrt(Math.max(d, 0)));

          for (k = j + 1; k < dimension; k++) {
            l.set(j, k, 0);
          }
        }

        this.L = l;
        this.positiveDefinite = Boolean(positiveDefinite);
      }

      _createClass(CholeskyDecomposition, [{
        key: "isPositiveDefinite",
        value: function isPositiveDefinite() {
          return this.positiveDefinite;
        }
      }, {
        key: "solve",
        value: function solve(value) {
          value = WrapperMatrix2D.checkMatrix(value);
          var l = this.L;
          var dimension = l.rows;

          if (value.rows !== dimension) {
            throw new Error('Matrix dimensions do not match');
          }

          if (this.isPositiveDefinite() === false) {
            throw new Error('Matrix is not positive definite');
          }

          var count = value.columns;
          var B = value.clone();
          var i, j, k;

          for (k = 0; k < dimension; k++) {
            for (j = 0; j < count; j++) {
              for (i = 0; i < k; i++) {
                B.set(k, j, B.get(k, j) - B.get(i, j) * l.get(k, i));
              }

              B.set(k, j, B.get(k, j) / l.get(k, k));
            }
          }

          for (k = dimension - 1; k >= 0; k--) {
            for (j = 0; j < count; j++) {
              for (i = k + 1; i < dimension; i++) {
                B.set(k, j, B.get(k, j) - B.get(i, j) * l.get(i, k));
              }

              B.set(k, j, B.get(k, j) / l.get(k, k));
            }
          }

          return B;
        }
      }, {
        key: "lowerTriangularMatrix",
        get: function get() {
          return this.L;
        }
      }]);

      return CholeskyDecomposition;
    }();

    var nipals = function nipals(X) {
      var options = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};

      _classCallCheck(this, nipals);

      X = WrapperMatrix2D.checkMatrix(X);
      var Y = options.Y;
      var _options$scaleScores = options.scaleScores,
          scaleScores = _options$scaleScores === void 0 ? false : _options$scaleScores,
          _options$maxIteration = options.maxIterations,
          maxIterations = _options$maxIteration === void 0 ? 1000 : _options$maxIteration,
          _options$terminationC = options.terminationCriteria,
          terminationCriteria = _options$terminationC === void 0 ? 1e-10 : _options$terminationC;
      var u;

      if (Y) {
        if (Array.isArray(Y) && typeof Y[0] === 'number') {
          Y = Matrix.columnVector(Y);
        } else {
          Y = WrapperMatrix2D.checkMatrix(Y);
        }

        if (!Y.isColumnVector() || Y.rows !== X.rows) {
          throw new Error('Y must be a column vector of length X.rows');
        }

        u = Y;
      } else {
        u = X.getColumnVector(0);
      }

      var diff = 1;
      var t, q, w, tOld;

      for (var counter = 0; counter < maxIterations && diff > terminationCriteria; counter++) {
        w = X.transpose().mmul(u).div(u.transpose().mmul(u).get(0, 0));
        w = w.div(w.norm());
        t = X.mmul(w).div(w.transpose().mmul(w).get(0, 0));

        if (counter > 0) {
          diff = t.clone().sub(tOld).pow(2).sum();
        }

        tOld = t.clone();

        if (Y) {
          q = Y.transpose().mmul(t).div(t.transpose().mmul(t).get(0, 0));
          q = q.div(q.norm());
          u = Y.mmul(q).div(q.transpose().mmul(q).get(0, 0));
        } else {
          u = t;
        }
      }

      if (Y) {
        var _p = X.transpose().mmul(t).div(t.transpose().mmul(t).get(0, 0));

        _p = _p.div(_p.norm());
        var xResidual = X.clone().sub(t.clone().mmul(_p.transpose()));
        var residual = u.transpose().mmul(t).div(t.transpose().mmul(t).get(0, 0));
        var yResidual = Y.clone().sub(t.clone().mulS(residual.get(0, 0)).mmul(q.transpose()));
        this.t = t;
        this.p = _p.transpose();
        this.w = w.transpose();
        this.q = q;
        this.u = u;
        this.s = t.transpose().mmul(t);
        this.xResidual = xResidual;
        this.yResidual = yResidual;
        this.betas = residual;
      } else {
        this.w = w.transpose();
        this.s = t.transpose().mmul(t).sqrt();

        if (scaleScores) {
          this.t = t.clone().div(this.s.get(0, 0));
        } else {
          this.t = t;
        }

        this.xResidual = X.sub(t.mmul(w.transpose()));
      }
    };

    exports.AbstractMatrix = AbstractMatrix;
    exports.CHO = CholeskyDecomposition;
    exports.CholeskyDecomposition = CholeskyDecomposition;
    exports.EVD = EigenvalueDecomposition;
    exports.EigenvalueDecomposition = EigenvalueDecomposition;
    exports.LU = LuDecomposition;
    exports.LuDecomposition = LuDecomposition;
    exports.Matrix = Matrix;
    exports.MatrixColumnSelectionView = MatrixColumnSelectionView;
    exports.MatrixColumnView = MatrixColumnView;
    exports.MatrixFlipColumnView = MatrixFlipColumnView;
    exports.MatrixFlipRowView = MatrixFlipRowView;
    exports.MatrixRowSelectionView = MatrixRowSelectionView;
    exports.MatrixRowView = MatrixRowView;
    exports.MatrixSelectionView = MatrixSelectionView;
    exports.MatrixSubView = MatrixSubView;
    exports.MatrixTransposeView = MatrixTransposeView;
    exports.NIPALS = nipals;
    exports.Nipals = nipals;
    exports.QR = QrDecomposition;
    exports.QrDecomposition = QrDecomposition;
    exports.SVD = SingularValueDecomposition;
    exports.SingularValueDecomposition = SingularValueDecomposition;
    exports.WrapperMatrix1D = WrapperMatrix1D;
    exports.WrapperMatrix2D = WrapperMatrix2D;
    exports.correlation = correlation;
    exports.covariance = covariance;
    exports.default = Matrix;
    exports.determinant = determinant;
    exports.inverse = inverse;
    exports.linearDependencies = linearDependencies;
    exports.pseudoInverse = pseudoInverse;
    exports.solve = solve;
    exports.wrap = wrap;
  }, {
    "ml-array-rescale": 5
  }],
  9: [function (require, module, exports) {
    'use strict';

    Object.defineProperty(exports, "__esModule", {
      value: true
    });

    var ml_distance_euclidean_1 = require("ml-distance-euclidean");

    var defaultOptions = {
      distanceFunction: ml_distance_euclidean_1.squaredEuclidean
    };

    function nearestVector(listVectors, vector) {
      var options = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : defaultOptions;
      var distanceFunction = options.distanceFunction || defaultOptions.distanceFunction;
      var similarityFunction = options.similarityFunction || defaultOptions.similarityFunction;
      var vectorIndex = -1;

      if (typeof similarityFunction === 'function') {
        // maximum similarity
        var maxSim = Number.MIN_VALUE;

        for (var _j21 = 0; _j21 < listVectors.length; _j21++) {
          var sim = similarityFunction(vector, listVectors[_j21]);

          if (sim > maxSim) {
            maxSim = sim;
            vectorIndex = _j21;
          }
        }
      } else if (typeof distanceFunction === 'function') {
        // minimum distance
        var minDist = Number.MAX_VALUE;

        for (var _i47 = 0; _i47 < listVectors.length; _i47++) {
          var dist = distanceFunction(vector, listVectors[_i47]);

          if (dist < minDist) {
            minDist = dist;
            vectorIndex = _i47;
          }
        }
      } else {
        throw new Error("A similarity or distance function it's required");
      }

      return vectorIndex;
    }

    exports.default = nearestVector;

    function findNearestVector(vectorList, vector) {
      var options = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : defaultOptions;
      var index = nearestVector(vectorList, vector, options);
      return vectorList[index];
    }

    exports.findNearestVector = findNearestVector;
  }, {
    "ml-distance-euclidean": 6
  }],
  10: [function (require, module, exports) {
    "use strict";

    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    var PROB_TOLERANCE = 0.00000001;

    function randomChoice(values) {
      var options = arguments.length > 1 && arguments[1] !== undefined ? arguments[1] : {};
      var random = arguments.length > 2 && arguments[2] !== undefined ? arguments[2] : Math.random;
      var _options$size = options.size,
          size = _options$size === void 0 ? 1 : _options$size,
          _options$replace = options.replace,
          replace = _options$replace === void 0 ? false : _options$replace,
          probabilities = options.probabilities;
      var valuesArr;
      var cumSum;

      if (typeof values === 'number') {
        valuesArr = getArray(values);
      } else {
        valuesArr = values.slice();
      }

      if (probabilities) {
        if (!replace) {
          throw new Error('choice with probabilities and no replacement is not implemented');
        } // check input is sane


        if (probabilities.length !== valuesArr.length) {
          throw new Error('the length of probabilities option should be equal to the number of choices');
        }

        cumSum = [probabilities[0]];

        for (var _i48 = 1; _i48 < probabilities.length; _i48++) {
          cumSum[_i48] = cumSum[_i48 - 1] + probabilities[_i48];
        }

        if (Math.abs(1 - cumSum[cumSum.length - 1]) > PROB_TOLERANCE) {
          throw new Error("probabilities should sum to 1, but instead sums to ".concat(cumSum[cumSum.length - 1]));
        }
      }

      if (replace === false && size > valuesArr.length) {
        throw new Error('size option is too large');
      }

      var result = [];

      for (var _i49 = 0; _i49 < size; _i49++) {
        var index = randomIndex(valuesArr.length, random, cumSum);
        result.push(valuesArr[index]);

        if (!replace) {
          valuesArr.splice(index, 1);
        }
      }

      return result;
    }

    function getArray(n) {
      var arr = [];

      for (var _i50 = 0; _i50 < n; _i50++) {
        arr.push(_i50);
      }

      return arr;
    }

    function randomIndex(n, random, cumSum) {
      var rand = random();

      if (!cumSum) {
        return Math.floor(rand * n);
      } else {
        var idx = 0;

        while (rand > cumSum[idx]) {
          idx++;
        }

        return idx;
      }
    }

    exports.default = randomChoice;
  }, {}],
  11: [function (require, module, exports) {
    "use strict";

    Object.defineProperty(exports, "__esModule", {
      value: true
    }); // tslint:disable-next-line

    var ml_xsadd_1 = require("ml-xsadd");

    var choice_1 = require("./choice");
    /**
     * @classdesc Random class
     */


    var Random = /*#__PURE__*/function () {
      /**
       * @param [seedOrRandom=Math.random] - Control the random number generator used by the Random class instance. Pass a random number generator function with a uniform distribution over the half-open interval [0, 1[. If seed will pass it to ml-xsadd to create a seeded random number generator. If undefined will use Math.random.
       */
      function Random() {
        var seedOrRandom = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : Math.random;

        _classCallCheck(this, Random);

        if (typeof seedOrRandom === 'number') {
          var xsadd = new ml_xsadd_1.default(seedOrRandom);
          this.randomGenerator = xsadd.random;
        } else {
          this.randomGenerator = seedOrRandom;
        }
      }

      _createClass(Random, [{
        key: "choice",
        value: function choice(values, options) {
          if (typeof values === 'number') {
            return choice_1.default(values, options, this.randomGenerator);
          }

          return choice_1.default(values, options, this.randomGenerator);
        }
        /**
         * Draw a random number from a uniform distribution on [0,1)
         * @return The random number
         */

      }, {
        key: "random",
        value: function random() {
          return this.randomGenerator();
        }
        /**
         * Draw a random integer from a uniform distribution on [low, high). If only low is specified, the number is drawn on [0, low)
         * @param low - The lower bound of the uniform distribution interval.
         * @param high - The higher bound of the uniform distribution interval.
         */

      }, {
        key: "randInt",
        value: function randInt(low, high) {
          if (high === undefined) {
            high = low;
            low = 0;
          }

          return low + Math.floor(this.randomGenerator() * (high - low));
        }
        /**
         * Draw several random number from a uniform distribution on [0, 1)
         * @param size - The number of number to draw
         * @return - The list of drawn numbers.
         */

      }, {
        key: "randomSample",
        value: function randomSample(size) {
          var result = [];

          for (var _i51 = 0; _i51 < size; _i51++) {
            result.push(this.random());
          }

          return result;
        }
      }]);

      return Random;
    }();

    exports.default = Random;
  }, {
    "./choice": 10,
    "ml-xsadd": 12
  }],
  12: [function (require, module, exports) {
    "use strict";

    Object.defineProperty(exports, "__esModule", {
      value: true
    });
    var LOOP = 8;
    var FLOAT_MUL = 1 / 16777216;
    var sh1 = 15;
    var sh2 = 18;
    var sh3 = 11;

    function multiply_uint32(n, m) {
      n >>>= 0;
      m >>>= 0;
      var nlo = n & 0xffff;
      var nhi = n - nlo;
      return (nhi * m >>> 0) + nlo * m >>> 0;
    }

    var XSadd = /*#__PURE__*/function () {
      function XSadd() {
        var seed = arguments.length > 0 && arguments[0] !== undefined ? arguments[0] : Date.now();

        _classCallCheck(this, XSadd);

        this.state = new Uint32Array(4);
        this.init(seed);
        this.random = this.getFloat.bind(this);
      }
      /**
       * Returns a 32-bit integer r (0 <= r < 2^32)
       */


      _createClass(XSadd, [{
        key: "getUint32",
        value: function getUint32() {
          this.nextState();
          return this.state[3] + this.state[2] >>> 0;
        }
        /**
         * Returns a floating point number r (0.0 <= r < 1.0)
         */

      }, {
        key: "getFloat",
        value: function getFloat() {
          return (this.getUint32() >>> 8) * FLOAT_MUL;
        }
      }, {
        key: "init",
        value: function init(seed) {
          if (!Number.isInteger(seed)) {
            throw new TypeError('seed must be an integer');
          }

          this.state[0] = seed;
          this.state[1] = 0;
          this.state[2] = 0;
          this.state[3] = 0;

          for (var _i52 = 1; _i52 < LOOP; _i52++) {
            this.state[_i52 & 3] ^= _i52 + multiply_uint32(1812433253, this.state[_i52 - 1 & 3] ^ this.state[_i52 - 1 & 3] >>> 30 >>> 0) >>> 0;
          }

          this.periodCertification();

          for (var _i53 = 0; _i53 < LOOP; _i53++) {
            this.nextState();
          }
        }
      }, {
        key: "periodCertification",
        value: function periodCertification() {
          if (this.state[0] === 0 && this.state[1] === 0 && this.state[2] === 0 && this.state[3] === 0) {
            this.state[0] = 88; // X

            this.state[1] = 83; // S

            this.state[2] = 65; // A

            this.state[3] = 68; // D
          }
        }
      }, {
        key: "nextState",
        value: function nextState() {
          var t = this.state[0];
          t ^= t << sh1;
          t ^= t >>> sh2;
          t ^= this.state[3] << sh3;
          this.state[0] = this.state[1];
          this.state[1] = this.state[2];
          this.state[2] = this.state[3];
          this.state[3] = t;
        }
      }]);

      return XSadd;
    }();

    exports.default = XSadd;
  }, {}]
}, {}, [1]);
},{}],"../../../../../../usr/local/lib/node_modules/parcel/src/builtins/hmr-runtime.js":[function(require,module,exports) {
var global = arguments[3];
var OVERLAY_ID = '__parcel__error__overlay__';
var OldModule = module.bundle.Module;

function Module(moduleName) {
  OldModule.call(this, moduleName);
  this.hot = {
    data: module.bundle.hotData,
    _acceptCallbacks: [],
    _disposeCallbacks: [],
    accept: function (fn) {
      this._acceptCallbacks.push(fn || function () {});
    },
    dispose: function (fn) {
      this._disposeCallbacks.push(fn);
    }
  };
  module.bundle.hotData = null;
}

module.bundle.Module = Module;
var checkedAssets, assetsToAccept;
var parent = module.bundle.parent;

if ((!parent || !parent.isParcelRequire) && typeof WebSocket !== 'undefined') {
  var hostname = "" || location.hostname;
  var protocol = location.protocol === 'https:' ? 'wss' : 'ws';
  var ws = new WebSocket(protocol + '://' + hostname + ':' + "61976" + '/');

  ws.onmessage = function (event) {
    checkedAssets = {};
    assetsToAccept = [];
    var data = JSON.parse(event.data);

    if (data.type === 'update') {
      var handled = false;
      data.assets.forEach(function (asset) {
        if (!asset.isNew) {
          var didAccept = hmrAcceptCheck(global.parcelRequire, asset.id);

          if (didAccept) {
            handled = true;
          }
        }
      }); // Enable HMR for CSS by default.

      handled = handled || data.assets.every(function (asset) {
        return asset.type === 'css' && asset.generated.js;
      });

      if (handled) {
        console.clear();
        data.assets.forEach(function (asset) {
          hmrApply(global.parcelRequire, asset);
        });
        assetsToAccept.forEach(function (v) {
          hmrAcceptRun(v[0], v[1]);
        });
      } else if (location.reload) {
        // `location` global exists in a web worker context but lacks `.reload()` function.
        location.reload();
      }
    }

    if (data.type === 'reload') {
      ws.close();

      ws.onclose = function () {
        location.reload();
      };
    }

    if (data.type === 'error-resolved') {
      console.log('[parcel] ✨ Error resolved');
      removeErrorOverlay();
    }

    if (data.type === 'error') {
      console.error('[parcel] 🚨  ' + data.error.message + '\n' + data.error.stack);
      removeErrorOverlay();
      var overlay = createErrorOverlay(data);
      document.body.appendChild(overlay);
    }
  };
}

function removeErrorOverlay() {
  var overlay = document.getElementById(OVERLAY_ID);

  if (overlay) {
    overlay.remove();
  }
}

function createErrorOverlay(data) {
  var overlay = document.createElement('div');
  overlay.id = OVERLAY_ID; // html encode message and stack trace

  var message = document.createElement('div');
  var stackTrace = document.createElement('pre');
  message.innerText = data.error.message;
  stackTrace.innerText = data.error.stack;
  overlay.innerHTML = '<div style="background: black; font-size: 16px; color: white; position: fixed; height: 100%; width: 100%; top: 0px; left: 0px; padding: 30px; opacity: 0.85; font-family: Menlo, Consolas, monospace; z-index: 9999;">' + '<span style="background: red; padding: 2px 4px; border-radius: 2px;">ERROR</span>' + '<span style="top: 2px; margin-left: 5px; position: relative;">🚨</span>' + '<div style="font-size: 18px; font-weight: bold; margin-top: 20px;">' + message.innerHTML + '</div>' + '<pre>' + stackTrace.innerHTML + '</pre>' + '</div>';
  return overlay;
}

function getParents(bundle, id) {
  var modules = bundle.modules;

  if (!modules) {
    return [];
  }

  var parents = [];
  var k, d, dep;

  for (k in modules) {
    for (d in modules[k][1]) {
      dep = modules[k][1][d];

      if (dep === id || Array.isArray(dep) && dep[dep.length - 1] === id) {
        parents.push(k);
      }
    }
  }

  if (bundle.parent) {
    parents = parents.concat(getParents(bundle.parent, id));
  }

  return parents;
}

function hmrApply(bundle, asset) {
  var modules = bundle.modules;

  if (!modules) {
    return;
  }

  if (modules[asset.id] || !bundle.parent) {
    var fn = new Function('require', 'module', 'exports', asset.generated.js);
    asset.isNew = !modules[asset.id];
    modules[asset.id] = [fn, asset.deps];
  } else if (bundle.parent) {
    hmrApply(bundle.parent, asset);
  }
}

function hmrAcceptCheck(bundle, id) {
  var modules = bundle.modules;

  if (!modules) {
    return;
  }

  if (!modules[id] && bundle.parent) {
    return hmrAcceptCheck(bundle.parent, id);
  }

  if (checkedAssets[id]) {
    return;
  }

  checkedAssets[id] = true;
  var cached = bundle.cache[id];
  assetsToAccept.push([bundle, id]);

  if (cached && cached.hot && cached.hot._acceptCallbacks.length) {
    return true;
  }

  return getParents(global.parcelRequire, id).some(function (id) {
    return hmrAcceptCheck(global.parcelRequire, id);
  });
}

function hmrAcceptRun(bundle, id) {
  var cached = bundle.cache[id];
  bundle.hotData = {};

  if (cached) {
    cached.hot.data = bundle.hotData;
  }

  if (cached && cached.hot && cached.hot._disposeCallbacks.length) {
    cached.hot._disposeCallbacks.forEach(function (cb) {
      cb(bundle.hotData);
    });
  }

  delete bundle.cache[id];
  bundle(id);
  cached = bundle.cache[id];

  if (cached && cached.hot && cached.hot._acceptCallbacks.length) {
    cached.hot._acceptCallbacks.forEach(function (cb) {
      cb();
    });

    return true;
  }
}
},{}]},{},["../../../../../../usr/local/lib/node_modules/parcel/src/builtins/hmr-runtime.js","scripts/ml-kmeans-bundle.js"], null)
//# sourceMappingURL=/ml-kmeans-bundle.90d6e94d.js.map
// modules are defined as an array
// [ module function, map of requires ]
//
// map of requires is short require name -> numeric require
//
// anything defined in a previous bundle is accessed via the
// orig method which is the require for previous bundles
parcelRequire = (function (modules, cache, entry, globalName) {
  // Save the require from previous bundle to this closure if any
  var previousRequire = typeof parcelRequire === 'function' && parcelRequire;
  var nodeRequire = typeof require === 'function' && require;

  function newRequire(name, jumped) {
    if (!cache[name]) {
      if (!modules[name]) {
        // if we cannot find the module within our internal map or
        // cache jump to the current global require ie. the last bundle
        // that was added to the page.
        var currentRequire = typeof parcelRequire === 'function' && parcelRequire;
        if (!jumped && currentRequire) {
          return currentRequire(name, true);
        }

        // If there are other bundles on this page the require from the
        // previous one is saved to 'previousRequire'. Repeat this as
        // many times as there are bundles until the module is found or
        // we exhaust the require chain.
        if (previousRequire) {
          return previousRequire(name, true);
        }

        // Try the node require function if it exists.
        if (nodeRequire && typeof name === 'string') {
          return nodeRequire(name);
        }

        var err = new Error('Cannot find module \'' + name + '\'');
        err.code = 'MODULE_NOT_FOUND';
        throw err;
      }

      localRequire.resolve = resolve;
      localRequire.cache = {};

      var module = cache[name] = new newRequire.Module(name);

      modules[name][0].call(module.exports, localRequire, module, module.exports, this);
    }

    return cache[name].exports;

    function localRequire(x){
      return newRequire(localRequire.resolve(x));
    }

    function resolve(x){
      return modules[name][1][x] || x;
    }
  }

  function Module(moduleName) {
    this.id = moduleName;
    this.bundle = newRequire;
    this.exports = {};
  }

  newRequire.isParcelRequire = true;
  newRequire.Module = Module;
  newRequire.modules = modules;
  newRequire.cache = cache;
  newRequire.parent = previousRequire;
  newRequire.register = function (id, exports) {
    modules[id] = [function (require, module) {
      module.exports = exports;
    }, {}];
  };

  var error;
  for (var i = 0; i < entry.length; i++) {
    try {
      newRequire(entry[i]);
    } catch (e) {
      // Save first error but execute all entries
      if (!error) {
        error = e;
      }
    }
  }

  if (entry.length) {
    // Expose entry point to Node, AMD or browser globals
    // Based on https://github.com/ForbesLindesay/umd/blob/master/template.js
    var mainExports = newRequire(entry[entry.length - 1]);

    // CommonJS
    if (typeof exports === "object" && typeof module !== "undefined") {
      module.exports = mainExports;

    // RequireJS
    } else if (typeof define === "function" && define.amd) {
     define(function () {
       return mainExports;
     });

    // <script>
    } else if (globalName) {
      this[globalName] = mainExports;
    }
  }

  // Override the current require with this new one
  parcelRequire = newRequire;

  if (error) {
    // throw error from earlier, _after updating parcelRequire_
    throw error;
  }

  return newRequire;
})({"scripts/controllers/main.js":[function(require,module,exports) {
/**
 * @license
 * Copyright Dan Munro
 * Released under Expat license <https://directory.fsf.org/wiki/License:Expat>
 */
"use strict";
/**
 * @ngdoc function
 * @name pbnApp.controller:MainCtrl
 * @description
 * # MainCtrl
 * Controller of the pbnApp
 */

function _createForOfIteratorHelper(o) { if (typeof Symbol === "undefined" || o[Symbol.iterator] == null) { if (Array.isArray(o) || (o = _unsupportedIterableToArray(o))) { var i = 0; var F = function F() {}; return { s: F, n: function n() { if (i >= o.length) return { done: true }; return { done: false, value: o[i++] }; }, e: function e(_e) { throw _e; }, f: F }; } throw new TypeError("Invalid attempt to iterate non-iterable instance.\nIn order to be iterable, non-array objects must have a [Symbol.iterator]() method."); } var it, normalCompletion = true, didErr = false, err; return { s: function s() { it = o[Symbol.iterator](); }, n: function n() { var step = it.next(); normalCompletion = step.done; return step; }, e: function e(_e2) { didErr = true; err = _e2; }, f: function f() { try { if (!normalCompletion && it.return != null) it.return(); } finally { if (didErr) throw err; } } }; }

function _unsupportedIterableToArray(o, minLen) { if (!o) return; if (typeof o === "string") return _arrayLikeToArray(o, minLen); var n = Object.prototype.toString.call(o).slice(8, -1); if (n === "Object" && o.constructor) n = o.constructor.name; if (n === "Map" || n === "Set") return Array.from(o); if (n === "Arguments" || /^(?:Ui|I)nt(?:8|16|32)(?:Clamped)?Array$/.test(n)) return _arrayLikeToArray(o, minLen); }

function _arrayLikeToArray(arr, len) { if (len == null || len > arr.length) len = arr.length; for (var i = 0, arr2 = new Array(len); i < len; i++) { arr2[i] = arr[i]; } return arr2; }

angular.module("pbnApp").controller("MainCtrl", function ($scope) {
  $scope.step = "load";
  $scope.view = "";
  $scope.status = "";
  $scope.loaderStyle = {
    border: "4px dashed #777777"
  };
  $scope.palette = [];
  $scope.colorInfoVisible = false;
  $scope.nColors = 6;

  $scope.imageLoaded = function (imgSrc) {
    var img = new Image();
    img.src = imgSrc;

    img.onload = function () {
      var c = document.getElementById("img-canvas"); // c.width = 800;

      c.width = document.getElementById("widthSlider").value;
      $scope.nColors = document.getElementById("levelSlider").value;
      var scale = c.width / img.naturalWidth;
      c.height = img.naturalHeight * scale;
      document.getElementById("canvases").style.height = c.height + 20 + "px";
      $scope.c = c;
      $scope.ctx = c.getContext("2d");
      $scope.ctx.drawImage(img, 0, 0, c.width, c.height);
      $scope.step = "select";
      $scope.$apply();
    };
  };

  $scope.addColor = function (color) {
    $scope.palette.push(color);
  };

  $scope.removeColor = function (color) {
    _.pull($scope.palette, color);
  };

  var getNearest = function getNearest(palette, col) {
    var nearest;
    var nearestDistsq = 1000000;

    for (var i = 0; i < palette.length; i++) {
      var pcol = palette[i];
      var distsq = Math.pow(pcol.r - col.r, 2) + Math.pow(pcol.g - col.g, 2) + Math.pow(pcol.b - col.b, 2);

      if (distsq < nearestDistsq) {
        nearest = i;
        nearestDistsq = distsq;
      }
    }

    return nearest;
  };

  var imageDataToSimpMat = function imageDataToSimpMat(imgData, palette) {
    var mat = [];

    for (var i = 0; i < imgData.height; i++) {
      mat[i] = new Array(imgData.width);
    }

    for (var i = 0; i < imgData.data.length; i += 4) {
      var nearestI = getNearest(palette, {
        r: imgData.data[i],
        g: imgData.data[i + 1],
        b: imgData.data[i + 2]
      });
      var x = i / 4 % imgData.width;
      var y = Math.floor(i / 4 / imgData.width);
      mat[y][x] = nearestI;
    }

    return mat;
  };

  var matToImageData = function matToImageData(mat, palette, context) {
    var imgData = context.createImageData(mat[0].length, mat.length);

    for (var y = 0; y < mat.length; y++) {
      for (var x = 0; x < mat[0].length; x++) {
        var i = (y * mat[0].length + x) * 4;
        var col = palette[mat[y][x]];
        imgData.data[i] = col.r;
        imgData.data[i + 1] = col.g;
        imgData.data[i + 2] = col.b;
        imgData.data[i + 3] = 255;
      }
    }

    return imgData;
  };

  var displayResults = function displayResults(matSmooth, matLine, labelLocs) {
    var c2 = document.getElementById("filled-canvas");
    c2.width = $scope.c.width;
    c2.height = $scope.c.height; // draw filled

    var ctx2 = c2.getContext("2d");
    var imgData = matToImageData(matSmooth, $scope.palette, ctx2);
    ctx2.putImageData(imgData, 0, 0);
    var c3 = document.getElementById("outline-canvas");
    c3.width = c2.width;
    c3.height = c2.height; // draw outlines
    // gray value was 191, changed to 150.

    var bw = [{
      r: 255,
      g: 255,
      b: 255
    }, {
      r: 150,
      g: 150,
      b: 150
    }];
    var ctx3 = c3.getContext("2d");
    var imgData = matToImageData(matLine, bw, ctx3);
    ctx3.putImageData(imgData, 0, 0); // draw numbers

    ctx3.font = "12px Georgia";
    ctx3.fillStyle = "rgb(150, 150, 150)";

    for (var i = 0; i < labelLocs.length; i++) {
      ctx3.fillText(labelLocs[i].value + 1, labelLocs[i].x - 3, labelLocs[i].y + 4);
    }

    $scope.c2 = c2;
    $scope.c3 = c3;
  };

  var rgbToHex = function rgbToHex(r, g, b) {
    return "#" + ((1 << 24) + (r << 16) + (g << 8) + b).toString(16).slice(1);
  };

  var rgbToCmyk = function rgbToCmyk(r, g, b) {
    var k = 1 - Math.max(r / 255, g / 255, b / 255);

    if (k == 1) {
      var c = 0;
      var m = 0;
      var y = 0;
    } else {
      var c = (1 - r / 255 - k) / (1 - k);
      var m = (1 - g / 255 - k) / (1 - k);
      var y = (1 - b / 255 - k) / (1 - k);
    }

    return {
      c: Math.round(c * 100),
      m: Math.round(m * 100),
      y: Math.round(y * 100),
      k: Math.round(k * 100)
    };
  };

  var rgbToHsl = function rgbToHsl(r, g, b) {
    r = r / 255;
    g = g / 255;
    b = b / 255;
    var M = Math.max(r, g, b);
    var m = Math.min(r, g, b);

    if (M == m) {
      var h = 0;
    } else if (M == r) {
      var h = 60 * (g - b) / (M - m);
    } else if (M == g) {
      var h = 60 * (b - r) / (M - m) + 120;
    } else {
      var h = 60 * (r - g) / (M - m) + 240;
    }

    var l = (M + m) / 2;

    if (l == 0 || l == 1) {
      var s = 0; // So it isn't NaN for black or white.
    } else {
      var s = (M - m) / (1 - Math.abs(2 * l - 1));
    }

    return {
      h: (Math.round(h) % 360 + 360) % 360,
      // js modulo isn't always positive
      s: Math.round(s * 100),
      l: Math.round(l * 100)
    };
  };

  var rgbToHsv = function rgbToHsv(r, g, b) {
    r = r / 255;
    g = g / 255;
    b = b / 255;
    var M = Math.max(r, g, b);
    var m = Math.min(r, g, b);

    if (M == m) {
      var h = 0;
    } else if (M == r) {
      var h = 60 * (g - b) / (M - m);
    } else if (M == g) {
      var h = 60 * (b - r) / (M - m) + 120;
    } else {
      var h = 60 * (r - g) / (M - m) + 240;
    }

    if (M == 0) {
      var s = 0; // So it isn't NaN for black.
    } else {
      var s = (M - m) / M;
    }

    return {
      h: (Math.round(h) % 360 + 360) % 360,
      s: Math.round(s * 100),
      v: Math.round(M * 100)
    };
  };

  var getColorInfo = function getColorInfo(palette) {
    for (var i = 0; i < palette.length; i++) {
      var col = palette[i];
      col.hex = rgbToHex(col.r, col.g, col.b);
      col.cmyk = rgbToCmyk(col.r, col.g, col.b);
      col.hsl = rgbToHsl(col.r, col.g, col.b);
      col.hsv = rgbToHsv(col.r, col.g, col.b);
    }
  };

  $scope.pbnify = function () {
    $scope.step = "process";
    var width = $scope.c.width;
    var height = $scope.c.height;
    var imgData = $scope.ctx.getImageData(0, 0, width, height);
    kMeansPalette(imgData, $scope.nColors);
    var mat = imageDataToSimpMat(imgData, $scope.palette);
    var worker = new Worker("/processImage.7a7b5f05.js");
    worker.addEventListener("message", function (e) {
      if (e.data.cmd == "status") {
        $scope.status = e.data.status;
        $scope.$apply();
      } else {
        var matSmooth = e.data.matSmooth;
        var labelLocs = e.data.labelLocs;
        var matLine = e.data.matLine;
        worker.terminate();
        displayResults(matSmooth, matLine, labelLocs);
        getColorInfo($scope.palette); // adds hex and CMYK values for display

        $scope.step = "result";
        $scope.view = "filled";
        $scope.$apply();
      }
    }, false);
    worker.postMessage({
      mat: mat
    });
  };

  $scope.newImage = function () {
    $scope.palette = [];
    document.getElementById("canvases").style.height = "0px";
    $scope.step = "load";
  };

  $scope.recolor = function () {
    $scope.step = "select";
  };

  $scope.clearPalette = function () {
    $scope.palette = [];
  };

  $scope.showColorInfo = function () {
    $scope.colorInfoVisible = true;
  };

  $scope.hideColorInfo = function () {
    $scope.colorInfoVisible = false;
  };

  $scope.viewFilled = function () {
    $scope.view = "filled";
  };

  $scope.viewOutline = function () {
    $scope.view = "outline";
  };

  $scope.saveFilled = function () {
    var win = window.open();
    win.document.write('<html><head><title>PBN filled</title></head><body><img src="' + $scope.c2.toDataURL() + '"></body></html>'); // win.print();
  };

  $scope.saveOutline = function () {
    var win = window.open();
    win.document.write('<html><head><title>PBN outline</title></head><body><img src="' + $scope.c3.toDataURL() + '"></body></html>'); // win.print();
  };

  $scope.savePalette = function () {
    var canvas = document.createElement("canvas");
    canvas.width = 80 * Math.min($scope.palette.length, 10);
    canvas.height = 80 * (Math.floor(($scope.palette.length - 1) / 10) + 1);
    var ctx = canvas.getContext("2d");
    ctx.fillStyle = "#ffffff";
    ctx.fillRect(0, 0, canvas.width, canvas.height);
    ctx.strokeStyle = "#000000";

    for (var i = 0; i < $scope.palette.length; i++) {
      var col = $scope.palette[i];
      ctx.fillStyle = "rgba(" + col.r + ", " + col.g + ", " + col.b + ", 255)";
      var x = 80 * (i % 10);
      var y = 80 * Math.floor(i / 10);
      ctx.fillRect(x + 10, y + 10, 60, 60);
      ctx.fillStyle = "#ffffff";
      ctx.fillRect(x + 10, y + 10, 20, 20);
      ctx.font = "16px sans-serif";
      ctx.fillStyle = "#000000";
      ctx.textAlign = "center";
      ctx.fillText(i + 1, x + 20, y + 26);
      ctx.strokeRect(x + 10, y + 10, 60, 60);
    }

    var win = window.open();
    win.document.write('<html><head><title>PBN palette</title></head><body><img src="' + canvas.toDataURL() + '"></body></html>'); // win.print();
  };
  /* kmeansPalette - produziert eine Palette automatisch mit K-Means Clustering
   */


  var kMeansPalette = function kMeansPalette(imgData, nColors) {
    var colors = []; // Produziert eine Feld, mit jeder Farbe als (R, G, B).

    for (var i = 0; i < imgData.data.length; i += 4) {
      colors.push([imgData.data[i], imgData.data[i + 1], imgData.data[i + 2]]);
    }

    var km = kmeans(colors, Math.round(nColors));
    var point;
    $scope.clearPalette(); // F�r jeder Massenmittenpnukt produzieren eine RGB-Farbe.

    var _iterator = _createForOfIteratorHelper(km.centroids),
        _step;

    try {
      for (_iterator.s(); !(_step = _iterator.n()).done;) {
        point = _step.value;
        var pcol = {
          r: Math.round(point.centroid[0]),
          g: Math.round(point.centroid[1]),
          b: Math.round(point.centroid[2])
        };
        $scope.palette.push(pcol);
      }
    } catch (err) {
      _iterator.e(err);
    } finally {
      _iterator.f();
    }
  };
});
},{"./../processImage.js":[["processImage.7a7b5f05.js","scripts/processImage.js"],"processImage.7a7b5f05.js.map","scripts/processImage.js"]}],"../../../../../../usr/local/lib/node_modules/parcel/src/builtins/hmr-runtime.js":[function(require,module,exports) {
var global = arguments[3];
var OVERLAY_ID = '__parcel__error__overlay__';
var OldModule = module.bundle.Module;

function Module(moduleName) {
  OldModule.call(this, moduleName);
  this.hot = {
    data: module.bundle.hotData,
    _acceptCallbacks: [],
    _disposeCallbacks: [],
    accept: function (fn) {
      this._acceptCallbacks.push(fn || function () {});
    },
    dispose: function (fn) {
      this._disposeCallbacks.push(fn);
    }
  };
  module.bundle.hotData = null;
}

module.bundle.Module = Module;
var checkedAssets, assetsToAccept;
var parent = module.bundle.parent;

if ((!parent || !parent.isParcelRequire) && typeof WebSocket !== 'undefined') {
  var hostname = "" || location.hostname;
  var protocol = location.protocol === 'https:' ? 'wss' : 'ws';
  var ws = new WebSocket(protocol + '://' + hostname + ':' + "61976" + '/');

  ws.onmessage = function (event) {
    checkedAssets = {};
    assetsToAccept = [];
    var data = JSON.parse(event.data);

    if (data.type === 'update') {
      var handled = false;
      data.assets.forEach(function (asset) {
        if (!asset.isNew) {
          var didAccept = hmrAcceptCheck(global.parcelRequire, asset.id);

          if (didAccept) {
            handled = true;
          }
        }
      }); // Enable HMR for CSS by default.

      handled = handled || data.assets.every(function (asset) {
        return asset.type === 'css' && asset.generated.js;
      });

      if (handled) {
        console.clear();
        data.assets.forEach(function (asset) {
          hmrApply(global.parcelRequire, asset);
        });
        assetsToAccept.forEach(function (v) {
          hmrAcceptRun(v[0], v[1]);
        });
      } else if (location.reload) {
        // `location` global exists in a web worker context but lacks `.reload()` function.
        location.reload();
      }
    }

    if (data.type === 'reload') {
      ws.close();

      ws.onclose = function () {
        location.reload();
      };
    }

    if (data.type === 'error-resolved') {
      console.log('[parcel] ✨ Error resolved');
      removeErrorOverlay();
    }

    if (data.type === 'error') {
      console.error('[parcel] 🚨  ' + data.error.message + '\n' + data.error.stack);
      removeErrorOverlay();
      var overlay = createErrorOverlay(data);
      document.body.appendChild(overlay);
    }
  };
}

function removeErrorOverlay() {
  var overlay = document.getElementById(OVERLAY_ID);

  if (overlay) {
    overlay.remove();
  }
}

function createErrorOverlay(data) {
  var overlay = document.createElement('div');
  overlay.id = OVERLAY_ID; // html encode message and stack trace

  var message = document.createElement('div');
  var stackTrace = document.createElement('pre');
  message.innerText = data.error.message;
  stackTrace.innerText = data.error.stack;
  overlay.innerHTML = '<div style="background: black; font-size: 16px; color: white; position: fixed; height: 100%; width: 100%; top: 0px; left: 0px; padding: 30px; opacity: 0.85; font-family: Menlo, Consolas, monospace; z-index: 9999;">' + '<span style="background: red; padding: 2px 4px; border-radius: 2px;">ERROR</span>' + '<span style="top: 2px; margin-left: 5px; position: relative;">🚨</span>' + '<div style="font-size: 18px; font-weight: bold; margin-top: 20px;">' + message.innerHTML + '</div>' + '<pre>' + stackTrace.innerHTML + '</pre>' + '</div>';
  return overlay;
}

function getParents(bundle, id) {
  var modules = bundle.modules;

  if (!modules) {
    return [];
  }

  var parents = [];
  var k, d, dep;

  for (k in modules) {
    for (d in modules[k][1]) {
      dep = modules[k][1][d];

      if (dep === id || Array.isArray(dep) && dep[dep.length - 1] === id) {
        parents.push(k);
      }
    }
  }

  if (bundle.parent) {
    parents = parents.concat(getParents(bundle.parent, id));
  }

  return parents;
}

function hmrApply(bundle, asset) {
  var modules = bundle.modules;

  if (!modules) {
    return;
  }

  if (modules[asset.id] || !bundle.parent) {
    var fn = new Function('require', 'module', 'exports', asset.generated.js);
    asset.isNew = !modules[asset.id];
    modules[asset.id] = [fn, asset.deps];
  } else if (bundle.parent) {
    hmrApply(bundle.parent, asset);
  }
}

function hmrAcceptCheck(bundle, id) {
  var modules = bundle.modules;

  if (!modules) {
    return;
  }

  if (!modules[id] && bundle.parent) {
    return hmrAcceptCheck(bundle.parent, id);
  }

  if (checkedAssets[id]) {
    return;
  }

  checkedAssets[id] = true;
  var cached = bundle.cache[id];
  assetsToAccept.push([bundle, id]);

  if (cached && cached.hot && cached.hot._acceptCallbacks.length) {
    return true;
  }

  return getParents(global.parcelRequire, id).some(function (id) {
    return hmrAcceptCheck(global.parcelRequire, id);
  });
}

function hmrAcceptRun(bundle, id) {
  var cached = bundle.cache[id];
  bundle.hotData = {};

  if (cached) {
    cached.hot.data = bundle.hotData;
  }

  if (cached && cached.hot && cached.hot._disposeCallbacks.length) {
    cached.hot._disposeCallbacks.forEach(function (cb) {
      cb(bundle.hotData);
    });
  }

  delete bundle.cache[id];
  bundle(id);
  cached = bundle.cache[id];

  if (cached && cached.hot && cached.hot._acceptCallbacks.length) {
    cached.hot._acceptCallbacks.forEach(function (cb) {
      cb();
    });

    return true;
  }
}
},{}]},{},["../../../../../../usr/local/lib/node_modules/parcel/src/builtins/hmr-runtime.js","scripts/controllers/main.js"], null)
//# sourceMappingURL=/main.b68b6df7.js.map